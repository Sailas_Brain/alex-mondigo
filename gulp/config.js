var templateDir = 'project/production/local/templates/mondigo'

var config = {
    markup: {
        src: {
            index: "project/src/index.php",
            header: "project/src/header.php",
            footer: "project/src/footer.php",
            pages: "project/src/pages/**/*.php",
        },
        dest: {
            index: "project/production",
            header: templateDir,
            footer: templateDir,
            pages: "project/production"
        }
    },
    sass: {
        src: 'project/src/styles/**/*.scss',
        dest: templateDir + '/styles',
        options: {
            
        }
    },
    js: {
        src: "project/src/js/",
        dest: templateDir + "/js"
    },
    images: {
        src: "project/src/images/**/*.*",
        dest: templateDir + "/images"
    },
    fonts: {
        src: "project/src/fonts/**/*.*",
        dest: templateDir + "/fonts"
    },
    watch: {
        markup: {
            index: "project/src/index.php",
            header: "project/src/header.php",
            footer: "project/src/footer.php",
            pages: "project/src/pages/**/*.php",
            blocks: "project/src/blocks/**/*.php"
        },
        styles: {
            sass: [
                'project/src/styles/**/*.scss',
                'project/src/blocks/**/*.scss',
                'project/src/pages/**/*.scss'
            ],
        },
        images: "project/src/images/**/*.*",
        fonts: "project/src/fonts/**/*.*",
        js: "project/src/js/**/*.js",
    }
}

module.exports = config;