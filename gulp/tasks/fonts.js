var gulp = require('gulp'),
    plumber = require('gulp-plumber');
var config = require('../config.js');

gulp.task('fonts', function(){
    return gulp.src(config.fonts.src)
    .pipe(plumber())
    .pipe(gulp.dest(config.fonts.dest))
});