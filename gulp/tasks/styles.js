var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass');

var config = require('../config.js');

gulp.task('styles:sass', function(){
    return gulp.src(config.sass.src)
    .pipe(plumber())
    .pipe(sass(config.sass.options))
    .pipe(gulp.dest(config.sass.dest))
});