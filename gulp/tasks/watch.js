var gulp = require('gulp');
var config = require('../config.js');

gulp.task('watch', ['build', 'browser-sync'], function () {
    gulp.watch(config.watch.markup.index, ['markup:index']);
    gulp.watch(config.watch.markup.header, ['markup:header']);
    gulp.watch(config.watch.markup.footer, ['markup:footer']);
    gulp.watch(config.watch.markup.pages, ['markup:pages']);

    gulp.watch(config.watch.markup.blocks, ['markup:index', 'markup:header', 'markup:footer','markup:pages']);
    
    gulp.watch(config.watch.js, ['scripts:js']);

    gulp.watch(config.watch.styles.sass, ['styles:sass']);

    gulp.watch(config.watch.images, ['images']);
    gulp.watch(config.watch.fonts, ['fonts']);
});