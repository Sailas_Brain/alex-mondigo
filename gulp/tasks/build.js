var gulp = require('gulp'),
    gulpSequence = require('gulp-sequence');

gulp.task('build', gulpSequence(
    'markup:index',
    'markup:header',
    'markup:footer',
    'markup:pages',
    'styles:sass',
    'scripts:js',
    'images',
    'fonts',
));