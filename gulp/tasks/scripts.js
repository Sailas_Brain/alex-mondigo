var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat');
    // uglify = require('gulp-uglify');
var config = require('../config.js');

gulp.task('scripts:js', function(){
    return gulp.src([config.js.src + 'pages/**/*.js', config.js.src + '*.js'])
    .pipe(plumber())
    .pipe(concat('main.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(config.js.dest))
});