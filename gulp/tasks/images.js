var gulp = require('gulp'),
    plumber = require('gulp-plumber');
var config = require('../config.js');

gulp.task('images', function(){
    return gulp.src(config.images.src)
    .pipe(plumber())
    .pipe(gulp.dest(config.images.dest))
});