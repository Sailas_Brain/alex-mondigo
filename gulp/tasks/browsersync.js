var gulp = require('gulp'),
    browserSync = require('browser-sync');
var config = require('../config.js');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "mondigo.local",
        files: ['project/production']
    });
});