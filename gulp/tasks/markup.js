var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rigger = require('gulp-rigger');
var config = require('../config.js');

gulp.task('markup:index', function(){
    return gulp.src(config.markup.src.index)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(config.markup.dest.index))
});

gulp.task('markup:header', function(){
    return gulp.src(config.markup.src.header)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(config.markup.dest.header))
});

gulp.task('markup:footer', function(){
    return gulp.src(config.markup.src.footer)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(config.markup.dest.footer))
});

gulp.task('markup:pages', function(){
    return gulp.src(config.markup.src.pages)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(config.markup.dest.pages))
});