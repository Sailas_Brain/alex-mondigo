<?define('SITE_TEMPLATE_PATH', '/local/templates/mondigo')?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- vendor -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick-theme.css" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main.css">
    <title>Document</title>
</head>
<body>
<div class="header">
    <div class="header__inner">
        <div class="header__wrapper-for-col internal-blocks">
            <div class="internal-blocks-col-1">
                <a href="/link/index.php" class="internal-blocks__logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                </a>
            </div>
            <div class="internal-blocks-col-2">
                <div class="internal-blocks__burger-menu"><button class="btn button-burger-menu"></button></div>
                <div class="internal-blocks__background-burger-menu">
                    <div class="internal-blocks__block-manu">
                        <div class="internal-blocks__novelties">
                            <div class="">
                                <span>Новинки</span>
                            </div> 
                            <div class="internal-blocks__block-menu novelties">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="elements">
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-1.jpg" alt="">
                                        <span>Новинки женские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-2.jpg" alt="">
                                        <span>Новинки мужские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-3.jpg" alt="">
                                        <span>Аксессуары</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="internal-blocks__female">
                            <div>
                                <span>Женская</span>
                            </div>
                            <div class="internal-blocks__block-menu female">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="internal-blocks__male">
                            <div>
                                <span>Мужская</span>
                            </div>
                            <div class="internal-blocks__block-menu male">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <a href="#" class="iinternal-blocks__InstaShop">InstaShop</a>
                    </div>
                </div>
            </div>
            <div class="internal-blocks-col-3">
                <div class="internal-blocks__messege">
                    <div class="internal-blocks__block-images messege"></div>
                    <div>

                    </div>
                </div>
                <div class="internal-blocks__phone">
                    <div class="internal-blocks__block-images phone"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__lock">
                    <div class="internal-blocks__block-images lock"></div>
                    <div class="internal-blocks__internal-block-lock">
                        <div class="links">
                            <a class="uppercase" href="#">Товары</a>
                            <a class="active" href="#">Отложенные товары</a>
                            <a class="bottom-text" href="#">Лист Ожидания</a>
                            <a class="uppercase" href="#">Работа с заказом</a>
                            <a href="#">Мои заказы</a>
                            <a href="#">Мои доставки</a>
                            <a href="#">Мой баланс</a>
                            <a href="#">Личные предложения</a>
                            <a href="#">Гардероб</a>
                            <a class="bottom-text" href="#">Оформление возврата</a>
                            <a class="uppercase" href="#">Профиль</a>
                            <a href="#">Мои данные</a>
                            <a href="#">Моя скидка 5%</a>
                            <a href="#">Мои рассылки</a>
                            <a class="bottom-text" href="#">Новости</a>
                            <a class="uppercase" href="#">Обратная связь</a>
                            <a href="#">Мои обращения</a>
                            <a class="bottom-text" href="#">Проверка товара</a>
                            <a href="#">Выйти</a>
                        </div>                   
                    </div>
                </div>
                <div class="internal-blocks__heart">
                    <div class="internal-blocks__block-images heart"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__serch">
                    <div class="internal-blocks__block-images serch"></div>
                    
                </div>
                <div class="internal-blocks__basket">
                    <div class="nember-basket"><span>2</span></div>

                    <div class="internal-blocks__basket-dropdawn internal-basket">
                    
                        <span class="internal-basket-title">Корзина</span>
                        <div class="internal-basket-product-basket">
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                        </div>

                        <div class="internal-basket__price">
                            <span>Итого:</span>
                            <span>руб 6.500</span>
                        </div>



                        <div class="internal-basket__lenk-basket">
                            <a href="#">перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="internal-blocks__input-search">
                <span>Что ищем? |</span>
                <span class="close-in-search"></span>
                <input type="search" placeholder="">
            </div>
        </div>
    </div>
</div>
<div class="page page-personal-offers">
    <div class="page__inner page-personal-offers__inner">
        <div class="page-personal-offers__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Новинки</span>
        </div>
        <div class="page-personal-offers__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <h1 class="page-personal-offers__title">Личные предложения</h1>
                <div class="page-personal-offers__element-block">
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">500р</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 1 500 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">5%</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-green" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 5000 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">1000р</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-blue" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 1 500 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">5%</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-DeepPink" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 5000 руб.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="footer__inner">
        <div class="footer__internal internal-blocks"> 
            <div class="col-1">
                <div class="internal-blocks__information">
                <span class="footer__title">контаткы</span>
                    <div class="internal-blocks__address">
                    <i class="fas fa-map-marker-alt"></i>
                        <span> г. Москва, ул. Нижняя Красносельская, д. 40/12, корп. 6, офис 8 </span>
                    </div>
                    <div class="internal-blocks__massege">
                    <i class="fas fa-envelope"></i>
                        <span>info@mondigo.ru</span>
                    </div>
                    <div class="internal-blocks__number-phone">
                        <i class="fas fa-phone"></i>
                        <span>8 800 775 88 38 (звонок бесплатный) 8 495 775 88 38</span>
                    </div>
                </div>
                <div class="internal-blocks__means-of-payment"> 
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/visa.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mastercard.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mir-logo-h14px.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/qiwi.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/maestro.svg" alt="">
                </div>
            </div>
            <div class="col-2"> 
                <span class="footer__title">О компании</span>
                <div class="internal-blocks__about-company">
                    <a href="#">О нас</a>
                    <a href="#">Контакты</a>
                    <a href="#">Сертификаты</a>
                    <a href="#">Преимущества</a>
                    <a href="#">Наши скидки</a>
                </div>
            </div>
            <div class="col-3">
                <span class="footer__title">Обслуживание клиентов</span>
                <div class="internal-blocks__client-service">
                    <a href="#">Как делать заказ</a>
                    <a href="#">Возврат товара</a>
                    <a href="#">Публичная оферта</a>
                    <a href="#">Доставка</a>
                    <a href="#">Способы оплаты</a>
                    <a href="#">Возврат денежных средств</a>
                    <a href="#">Вопросы и ответы</a>
                    <a href="#">Подарочные карты</a>
                    <a href="#">Сотрудничество оптом</a>
                </div>
            </div>
            <div class="col-4">
                <span class="footer__title">прочее</span>
                <div class="internal-blocks__etc-block">
                    <a href="#">Фабрика</a>
                    <a href="#">Обратная связь</a> 
                </div>
            </div>
        </div>
    </div>
        <div class="footer__bottom-block">
            <div class="footer__bottom-inner">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                <span>© 1999-2018 Mondigo.ru</span>
            </div>
        </div>
</div>

















<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
                                    <!-- vendor -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/slick.js"></script>

                                    <!-- jS -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
</body>
</html>