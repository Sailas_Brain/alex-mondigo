<div class="footer">
    <div class="footer__inner">
        <div class="footer__internal internal-blocks"> 
            <div class="col-1">
                <div class="internal-blocks__information">
                <span class="footer__title">контаткы</span>
                    <div class="internal-blocks__address">
                    <i class="fas fa-map-marker-alt"></i>
                        <span> г. Москва, ул. Нижняя Красносельская, д. 40/12, корп. 6, офис 8 </span>
                    </div>
                    <div class="internal-blocks__massege">
                    <i class="fas fa-envelope"></i>
                        <span>info@mondigo.ru</span>
                    </div>
                    <div class="internal-blocks__number-phone">
                        <i class="fas fa-phone"></i>
                        <span>8 800 775 88 38 (звонок бесплатный) 8 495 775 88 38</span>
                    </div>
                </div>
                <div class="internal-blocks__means-of-payment"> 
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/visa.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mastercard.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mir-logo-h14px.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/qiwi.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/maestro.svg" alt="">
                </div>
            </div>
            <div class="col-2"> 
                <span class="footer__title">О компании</span>
                <div class="internal-blocks__about-company">
                    <a href="#">О нас</a>
                    <a href="#">Контакты</a>
                    <a href="#">Сертификаты</a>
                    <a href="#">Преимущества</a>
                    <a href="#">Наши скидки</a>
                </div>
            </div>
            <div class="col-3">
                <span class="footer__title">Обслуживание клиентов</span>
                <div class="internal-blocks__client-service">
                    <a href="#">Как делать заказ</a>
                    <a href="#">Возврат товара</a>
                    <a href="#">Публичная оферта</a>
                    <a href="#">Доставка</a>
                    <a href="#">Способы оплаты</a>
                    <a href="#">Возврат денежных средств</a>
                    <a href="#">Вопросы и ответы</a>
                    <a href="#">Подарочные карты</a>
                    <a href="#">Сотрудничество оптом</a>
                </div>
            </div>
            <div class="col-4">
                <span class="footer__title">прочее</span>
                <div class="internal-blocks__etc-block">
                    <a href="#">Фабрика</a>
                    <a href="#">Обратная связь</a> 
                </div>
            </div>
        </div>
    </div>
        <div class="footer__bottom-block">
            <div class="footer__bottom-inner">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                <span>© 1999-2018 Mondigo.ru</span>
            </div>
        </div>
</div>

















<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
                                    <!-- vendor -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/slick.js"></script>

                                    <!-- jS -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
</body>
</html>