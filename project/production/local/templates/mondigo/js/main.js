var initSlick = function(){
    $('.page-catalog__big-preview').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: true,
        verticalSwiping: false,
        horizontalSwiping: true,
        prevArrow: '<button type="button" class="btn page-catalog__btn-prev"></button>',
        nextArrow: '<button type="button" class="btn page-catalog__btn-next"></button>'
    });
}
$(document).ready(function () {
    $(".internal-filter__sort").dropdown(); 
    $(".internal-filter__block-filter").dropdown();
    initSlick(); 
    $(document).on("click", ".view-two", function() {
        $(".view-four").removeClass("active");
        $(this).toggleClass("active");
        $(".page-catalog__slider-new-collection").toggleClass("active");
        $('.page-catalog__big-preview').slick('unslick');
        initSlick();
    });
    $(document).on("click", ".view-four", function(){
        $(".view-two").removeClass("active");
        $(this).toggleClass("active");
        $(".page-catalog__slider-new-collection").removeClass("active");
        $('.page-catalog__big-preview').slick('unslick');
        initSlick();
    });
    $(document).on("click", ".buttom-close", function(){
        $(".internal-filter__wrapper-sort").css("display", "none");
        $(".sort-text").removeClass("active");
    });
    var catalogFilterPriceSlider = $(".internal-price__wrapper-slider").slider({
        range: true,
        min: 700,
        max: 27000,
        values: [700, 27000],
        slide: function ( event, ui ) {
            $(".internal-price__min-value").val(ui.values[0]);
            $(".internal-price__max-value").val(ui.values[1]);
        }
    });
    $(document).on("change", ".internal-price__val", function(){
        var catalogFilterPriceSlider = Array();
        $(".internal-price__val").each(function () {
            catalogFilterPriceSlider.push($(this).val());
        });
        catalogFilterPriceSlider.slider("option", "values", catalogFilterPriceSlider);
        delete catalogFilterPriceSlider;
    });
    $(document).on("click", ".page-catalog__icon-heart", function() {
        $(this).toggleClass("active"); 
    });
    $(document).on( "click", ".in-cart", function(){
        $(this).css("display", "none");
        $(this).next(".choice-of-size").css("display", "block");
        $(document).on( 'click', function(e){
            if (!$(".in-cart").is(e.target) && $(".in-cart").has(e.target).length === 0) {
                $(".in-cart").css("display", "block");
                $(".in-cart").next(".choice-of-size").css("display", "none");
            }
        });
    });
    if( screen.width < 768){
        $(".col-1").accordion(true);
        $(".col-4").accordion(true);        
        $(".col-floor").accordion(true);
        $(".col-size").accordion(true);
        $(".col-price-in-filter").accordion(true);
        $(".col-season").accordion(true);
        $(".col-composition").accordion(true);
    }
    $(".page-catalog__big-preview").hover( function(){
        $(this).parents(".page-catalog__slider-new-collection").find(".page-catalog__wrapper-for-hover").addClass("active");
        $(this).find(".btn.page-catalog__btn-prev").addClass("active");
        $(this).find(".btn.page-catalog__btn-next").addClass("active");
    });
    $(".page-catalog__big-preview").mouseleave( function(){
        $(this).parents(".page-catalog__slider-new-collection").find(".page-catalog__wrapper-for-hover").removeClass("active");
        $(this).find(".btn.page-catalog__btn-prev").removeClass("active");
        $(this).find(".btn.page-catalog__btn-next").removeClass("active");
    });

});


$(document).ready( function () {
    $(document).on("click", ".button-choose", function(){
        $(".modal-application-on-hceck__dropdown-fomr-wrapper").toggleClass("active");
    });
    $(".button-create-request").modal(".modal-application-on-hceck");
    $(".button-familiarize").modal(".internal-modal");
    $(document).on("click", ".btn-modal-internal-close", function(){
        $(".internal-modal").css("display", "none");
    });
    if(screen.width <= 320){
        $(".page-checking-goods__return-on-receipt").accordion(true);
        $(".page-checking-goods__return-after-payment").accordion(true);        
    }else if(screen.width <= 425){
        $(".page-checking-goods__return-on-receipt").accordion(true);
        $(".page-checking-goods__return-after-payment").accordion(true);  
    }else{
        $('.page-checking-goods .checking-goods').tabs(1);
        $(".page-checking-goods__requirements").accordion(true);
    }
});
$(document).ready( function(){
    $('.page-deferred-goods .wrapper-for-lest').checkAllInputs();
}); 
$(document).ready( function () {
    $(".button-percent-ransom").modal(".modal-percent-ransom");
    $(".button-delivery").modal(".modal-total-number-goods");
    $(".modal-percent-ransom__calculation-of-interest").dropdown();
});
$(document).ready(function() {
    
    $('.internal-blocks__lock').dropdown();  

    
    if (screen.width <= 320) {
        $('.internal-blocks-col-2').dropdown();
        $('.internal-blocks__novelties').dropdown();
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else if(screen.width <= 425) {
        $('.internal-blocks-col-2').dropdown();    
        $('.internal-blocks__novelties').dropdown();    
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else if(screen.width <= 768) {
        $('.internal-blocks-col-2').dropdown();    
        $('.internal-blocks__novelties').dropdown();    
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else{

    }

    // $(document).on("click", ".internal-blocks__burger-menu", function(){
    //     $(this).toggleClass("active");
    //     $(".internal-blocks__background-burger-menu").toggleClass("active-menu");
    // });
    $(document).on("click", ".button-prev-menu", function(){
        $(".internal-blocks__block-menu").css("display", "none");
    });
    $(".internal-blocks__block-images.serch").on("click", function () {
        $(".internal-blocks__input-search").css("display", "block");
        $(document).on( 'click', function(e){
            if (!$(".internal-blocks__block-images.serch").is(e.target) && $(".internal-blocks__block-images.serch").has(e.target).length === 0 && !$('input').is(e.target)) {
                $(".internal-blocks__input-search").css("display", "none");
            }
        });
    });
    // $(".nember-basket").on("click", function (){
    //     $(".internal-blocks__basket-dropdawn").toggleClass("active");
    //     $(document).on("click", function (e) {
            
    //     });
    // });

    var hideOpen = function(e) {
        // if ($('.internal-blocks__basket-dropdawn').is(e.target)) {

        // }
        if ($('.nember-basket').is(e.target)) {
            $(".internal-blocks__basket-dropdawn").toggleClass("active");
        }
        else if (!$('.internal-blocks__basket-dropdawn').is(e.target) && $(".internal-blocks__basket-dropdawn").has(e.target).length === 0 ) {
            $(".internal-blocks__basket-dropdawn").removeClass("active");
        }

    }
    document.addEventListener("click", hideOpen);
    $('.internal-basket-product-basket').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: false,
        vertical: true,
        verticalSwiping: true,
        horizontalSwiping: false,
        prevArrow: '<button type="button" class="btn internal-basket__btn-prev"></button',
        nextArrow: '<button type="button" class="btn internal-basket__btn-next"></button'
    });
});


    // if(window.screen.width < 320){
    //     console.log("hello world");
    // }
$(document).ready(function(){
    $('.page-main__big-preview').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: true,
        verticalSwiping: false,
        horizontalSwiping: true,
        prevArrow: '<button type="button" class="btn page-main__btn-prev"></button',
        nextArrow: '<button type="button" class="btn page-main__btn-next"></button'
    });
    $(".page-main__icon-heart").on("click", function() {
       $(this).toggleClass("active"); 
    });
    // $(".in-cart").on( "click", function(){
    //     $(this).css("display", "none");
    //     $(".page-main__choice-of-size").css("display", "block");
    //     $(document).on( 'click', function(e){
    //         if (!$(".in-cart").is(e.target) && $(".in-cart").has(e.target).length === 0) {
    //             $(".in-cart").css("display", "block");
    //             $(".page-main__choice-of-size").css("display", "none");
    //         }
    //     });
    // });
    $(".slick-slider").hover(function() {
        $(this).find('.slick-current .image-slider-hover img').toggleClass("active");
    });
    $(".bth-download-more").on("click", function () {
        $(".page-main__wrapper-for-hidden-block").css("display", "flex");
    });



    $(".page-main__big-preview").hover( function(){
        $(this).parents(".page-main__slider-new-collection").find(".page-main__wrapper-for-hover").addClass("active");
        $(this).find(".btn.page-main__btn-prev").addClass("active");
        $(this).find(".btn.page-main__btn-next").addClass("active");
    });
    $(".page-main__big-preview").mouseleave( function(){
        $(this).parents(".page-main__slider-new-collection").find(".page-main__wrapper-for-hover").removeClass("active");
        $(this).find(".btn.page-main__btn-prev").removeClass("active");
        $(this).find(".btn.page-main__btn-next").removeClass("active");
    });

});

$(document).ready( function () {
    $(".page-making-refund-initial__block-quality").dropdown();
    $(".page-making-refund-initial__block-choose").dropdown();

        $(document).on("click", ".internal-block span", function(){
            $(".quality").text($(this).text());
        });
        $(document).on("click", ".internal-choose-block span", function(){
            $(".choose").text($(this).text());
        });
});
$(document).ready( function () {
    
});

$(document).ready( function(){
    $('.page-my-appeals .page-my-appeals__top-list').checkAllInputs();
    $('.page-my-appeals .page-my-appeals__top-list-heading').checkAllInputs();

    
    $(".button-gift-vouchers").modal(".modal-gift-vouchers");


    if (screen.width <= 320) {
        $(".page-my-appeals__wrapper-for-accordion").accordion(true);
        // $(".page-my-appeals__wrapper-for-accordion").eq(1).accordion(true);
    }else if(screen.width <= 425){
        $(".page-my-appeals__wrapper-for-accordion").accordion(true);        
    }else{
        $('.page-my-appeals .my-appeals').tabs(1);
    }
});
$(document).ready( function(){
    $(".page-my-balance__button-for-open").dropdown();
    $(".modal-gift-vouchers .gift-vouchers").tabs(1);
});
$(document).ready( function() {
    //$(Кнопра).modal(Модальное окно);
    $(".button-personal-data").modal(".modal-personal-data");
    $(".button-change-number").modal(".modal-change-number-phone");
    $(".change-email").modal(".modal-change-email");
    $(".change-password").modal(".modal-change-password");
    $(".button-setting").modal(".modal-addition-protection");
    $(".button-chenge-params").modal(".modal-change-parametrs");
    if(screen.width <= 425){
        $(document).on("click", ".internal-block-list", function () {
            $(this).toggleClass("active");
        });
    }else{

    }
});
$(document).ready( function(){
    if(screen.width <= 425){
        $(".page-my-dispatch__mobile-version").accordion(true);
    }else{
        $('.page-my-dispatch .my-dispatch').tabs(1);
    }
});


$(document).ready( function(){
    $(".page-my-orber__wrapper-for-internal-element").accordion(true);
    $(".page-my-orber__wrapper-for-elment-list-delivery").accordion(true);
    $(".button-courier-service").modal(".modal-courier-service");


    var datepickerToOrber = $('.datepicker-to').datepicker(datepickerSettings).on( "change", function() {
        datepickerFromOrber.datepicker( "option", "maxDate", datepickerGetDate( this ) );
    //     $('#dataGetTo p span').text( 
    //         datepickerTo.datepicker({ dateFormat: 'dd.mm.yy' }).val()
    //  );
    });
    var datepickerFromOrber = $('.datepicker-from').datepicker(datepickerSettings).on( "change", function() {
        datepickerToOrber.datepicker( "option", "minDate", datepickerGetDate( this ) );
        // $('#dataGetFrom p span').text( 
        //     datepickerFrom.datepicker({ dateFormat: 'dd.mm.yy' }).val()
        // );
    });

    for (let i = 0; i < $('.page-my-orber .page-my-orber__wrapper-delivery-list .wrapper-for-product').length; i++) {
        $('.page-my-orber .wrapper-for-product-' + i).checkAllInputs();
    }
    if(screen.width <= 425){
        $(".page-my-orber__orbers-tab").accordion(true);
    }else{
        $('.page-my-orber .my-orbers').tabs(1);
    }



}); 
$(document).ready(function(){
    $('.internal-blocks__wrapper-for-accordion').accordion(true);
    $(document).on("click", ".close-element-list", function () {
        $(".internal-blocks__list-return").removeClass("active");
        $(".internal-blocks__internal-block-list").css("display", "none");
    });
});
$(document).ready( function(){
    $('.page-my-wardrobe .wrapper-for-list-wardrobe').checkAllInputs();
});
$(document).ready( function () {
    $('.page-order-registration .order-registration').tabs(1);
    $('.page-order-registration__city').dropdown();
    $('.page-order-registration__block-city').dropdown();
    $(document).on("click", ".internal-block-city span", function(){
        $(".text-city").text($(this).text());
    });
    $(document).on("click", ".internal-block span", function(){
        $(".block-text-city").text($(this).text());
    });
    $(document).on("click", ".icons button", function(){
        $(this).toggleClass("active");
    });

});
$(document).ready( function(){
    $(".button-calculation-table").modal(".modal-total-number-goods")
});
$(document).ready( function(){
    var datepickerTo = $('#datepicker').datepicker(datepickerSettings);
});
$(document).ready(function() {


    
    $('.page-product-card__top-images img').on("click", function(){
        var productCardItemPreview = $(this).parents('.wrapper-for-slider');
        $(this).removeClass('active');
        $(this).addClass('active');
        $(productCardItemPreview).find('.page-product-card__big-images img').attr('src', $(this).attr("src"));
    });
    $(".page-product-card__choose-color").eq(0).dropdown();
    $(".page-product-card__choose-color").eq(1).dropdown();
    $(".page-product-card__choose-color").eq(2).dropdown();
    $(".page-product-card__choose-color").eq(3).dropdown();
    $(".page-product-card__choose-color").eq(4).dropdown();
    $(".page-product-card__choose-color").eq(5).dropdown();
    $(".page-product-card__choose-color").eq(6).dropdown();
    $(".page-product-card__choose-color").eq(7).dropdown();
    $(".page-product-card__choose-color").eq(8).dropdown();

    $(document).on("click", ".page-product-card__wrapper-specify-question textarea", function () {
        $(this).toggleClass("active");
        $(document).on( 'click', function(e){
            if (!$("textarea").is(e.target) && $("textarea").has(e.target).length === 0) {
                $("textarea").removeClass("active");
            }
        });
    })
    $(document).on("click", ".like-heart, .modal-like-heart, .like-heart-block-slider",  function(){
        $(this).toggleClass("active");
    });
    $('.page-product-card .product-card').tabs(1);
    $(document).on("click", ".element-table", function(e) {
        $('.modal-define-size__wrapper-for-table span').removeClass('active');
        if ($('span').is(e.target)) {

            var nthSpan = $(this).find(e.target).index() + 1;
            $(this).parents('.modal-define-size__wrapper-element-table').find('span:nth-of-type(' + nthSpan + ')').addClass('active');

            var nthDiv =  $(this).index() + 1;
            $('.modal-define-size__wrapper-element-table .element-table:nth-of-type(' + nthDiv + ') span').toggleClass('active');

            for (var i = 0; i <= $(this).find('span').length; i++) {
                if (i > nthSpan) {
                    $(this).find('span:nth-of-type(' + i +')').removeClass('active');
                }
            }

            var nthRow = $(this).parents('.modal-define-size__wrapper-element-table').index() + 1;
            for (var i = 0; i <= $('.modal-define-size__wrapper-element-table').length + 1; i++) {
                if (i > nthRow) {
                    $('.modal-define-size__wrapper-element-table:nth-child(' + i + ') span').removeClass('active');
                }
            }
        }

    });
    $('.modal-define-size__wrapper-element-table').on("click", "span", function() {
        $(".modal-define-size__wrapper-for-table span").removeClass("selected");
        $(this).addClass('selected');
    });

    $(".button-define-size").modal(".modal-define-size");
    $(".button-define-size").modal(".modal-women-define-size");
    $(".button-buy-the-way").modal(".modal-buy-way");

    $(document).on("click", ".element-table-women", function(e) {
        $('.modal-women-define-size__wrapper-for-table span').removeClass('active');
        if ($('span').is(e.target)) {

            var nthSpan = $(this).find(e.target).index() + 1;
            $(this).parents('.modal-women-define-size__wrapper-element-table').find('span:nth-of-type(' + nthSpan + ')').addClass('active');

            var nthDiv =  $(this).index() + 1;
            $('.modal-women-define-size__wrapper-element-table .element-table-women:nth-of-type(' + nthDiv + ') span').toggleClass('active');

            for (var i = 0; i <= $(this).find('span').length; i++) {
                if (i > nthSpan) {
                    $(this).find('span:nth-of-type(' + i +')').removeClass('active');
                }
            }
            var nthCol = $(this).index() +1;
            for (var j = 0; j < $(".element-table-women").length +1; j++) {
                if (j > nthCol) {
                    $('.element-table-women:nth-child(' + j + ') span').removeClass('active');        
                }
            }
        }

    });
    $('.modal-women-define-size__wrapper-element-table').on("click", "span", function() {
        $(".modal-women-define-size__wrapper-for-table span").removeClass("selected");
        $(this).addClass('selected');
    });

    if( screen.width <= 320){
        $('.page-product-card__top-images').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            prevArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-prev"></button',
            nextArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-next"></button',
            dots: true,
            horizontal: true,
            horizontalSwiping: true,
        });
    }else{
        $('.page-product-card__top-images').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-prev"></button',
            nextArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-next"></button',
            dots: false,
            horizontal: true,
            horizontalSwiping: true,
        });
    }

});











$(document).ready( function () {
    $(".page-wholesalers__wrapper-for-accordion").accordion(true, true);
});
$(document).ready( function(){
    $(".page-write-appeals__select-block").eq(0).dropdown();
    $(".page-write-appeals__select-block").eq(1).dropdown();
    $(".page-write-appeals__select-block").eq(2).dropdown();
    $(".page-write-appeals__select-block").eq(3).dropdown();
    if(screen.width <= 425){
        $(".page-write-appeals__write-appeals").accordion(true);
    }else{
        $('.page-write-appeals .write-appeals').tabs(1);
    }
});
$.fn.dropdown = function (dropdownHeading = null, dropdownBlock = null){
    var dropdown = $(this);
    if(dropdownHeading == null) {
        dropdownHeading = dropdown.find("> div:nth-child(1)");
    }
    else {
        dropdownHeading = dropdown.find(dropdownHeading);
    }
    if(dropdownBlock == null) {
        dropdownBlock = dropdown.find("> div:nth-child(2)");
    }
    else {
        dropdownBlock = dropdown.find(dropdownBlock);
    }

    dropdownHeading.on('click', function(){
        dropdownBlock.toggle();
        $(this).toggleClass('active');
    });
    $(document).mouseup(function (e){
        if (!dropdown.is(e.target) && dropdown.has(e.target).length === 0) {
            dropdownBlock.hide();
            dropdownHeading.removeClass('active');
        }
    });
}
$.fn.accordion = function(autoClose, hasWrapBlocks = false){
    var accordion = $(this);
    if(hasWrapBlocks == true){
        var accordionHeadings = accordion.find("> div > div:nth-of-type(1)");
    }
    else {
        var accordionHeadings = accordion.find("> div").filter(':even');
    }
    if(autoClose) {
        accordionHeadings.removeClass('active');
        accordionHeadings.next().css('display', 'none');
    }
    else {
        accordionHeadings.addClass('active');
        accordionHeadings.next().css('display', 'block');
    }

    accordionHeadings.each(function(){
        $(this).on('click', function(){
            if(autoClose) {
                accordionHeadings.not(this).removeClass('active');
                accordionHeadings.not(this).next().slideUp();
            }
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    })
}
$.fn.modal = function(modal) {
    var modalShow = $(this);
    $(modalShow).on('click', function(e){
        $(modal).fadeIn();
        $(modal).css('display', 'flex');
        // $('body').css('overflow', 'hidden');
    })

    $(document).mouseup(function (e){
        if ($(modal).is(e.target)) {
            $(modal).fadeOut();
            // $('body').css('overflow', 'auto')
        }
        //костыль на datepicker
        // if (!$(modal).is(e.target) && $(modal).has(e.target).length === 0 && !$('.ui-datepicker a').is(e.target) && !$('.ui-datepicker span').is(e.target) && !$('.ui-datepicker div').is(e.target) && !$('.ui-datepicker th').is(e.target) && !$('.ui-datepicker').is(e.target)) {
        //     $(modal).fadeOut();
        // }
    });
}
$.fn.tabs = function(tab = 1) {
    var tabs = $(this);
    var tabsBtns = tabs.find('.tabs-btn');
    var tabsBlocks = tabs.find('.tabs-block');

    $(tabsBtns).removeClass('active');
    $(tabsBlocks).hide();

    $(tabsBtns[tab - 1]).addClass('active');
    $(tabsBlocks[tab - 1]).show();

    $(tabsBtns).each(function (index) {
        $(this).on('click', function(){
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $(tabsBlocks[index]).show();
            $(tabsBlocks[index]).siblings().hide();
        })
    });
}
$.fn.checkAllInputs = function() {
    var checkAllInputs = $(this);
    var checkBtn = checkAllInputs.find('.js-btn-autocheck');
    var inputs = checkAllInputs.find('.js-input-autocheck');
    var labels = checkAllInputs.find('.js-label-autocheck');

    $(checkBtn).on('click', function(e) {
        // alert("hahaha");
        if (!$('button').is(e.target) && !$('a').is(e.target)) {
            $(inputs).prop('checked',true);
            if ((checkBtn).hasClass('active')) {
                $(inputs).prop('checked',false);
                $(checkBtn).removeClass('active');
            } else {
                $(checkBtn).addClass('active');
            }
        }
    });

    $('input[type="reset"]').on('click', function() {
        $(inputs).prop('checked',false);
        $(checkBtn).removeClass('active');
    });
    
    $(labels).each(function (index) {
        $(this).on('click', function(e) {
            if (!$('button').is(e.target) && !$('a').is(e.target)) {
                var counter = inputs.length;
                $(inputs).each(function() {
                    if($(this).prop('checked')==false) {
                        counter--;
                    }
                });
                if (counter == (inputs.length)) {
                    $(checkBtn).removeClass('active');
                }
                if($(inputs[index]).prop('checked')==false && counter == (inputs.length - 1)) {
                    $(checkBtn).addClass('active');
                }
            }
        });
    });
}
function datepickerGetDate( element ) {
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }
    return date;
}
var dateFormat = "dd.mm.yy";
var datepickerSettings = {
    closeText: "Закрыть",
    prevText: "&#x3C;Пред",
    nextText: "След&#x3E;",
    currentText: "Сегодня",
    monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
    "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
    monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
    "Июл","Авг","Сен","Окт","Ноя","Дек" ],
    dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
    dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
    dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
    weekHeader: "Нед",
    dateFormat: dateFormat,
    firstDay: 1,
    showOtherMonths: true,
    selectOtherMonths: true,
    isRTL: false,
}
$(document).ready(function() {
    $(document).on('click', '.btn-modal-close', function () {
        $(this).parents(".modal").fadeOut();
        // $('body').css('overflow', 'auto');
    });
    $(document).on('click', '.btn-cancellation-modal', function () {
        $(this).parents(".modal").fadeOut();
        // $('body').css('overflow', 'auto');
    });
});
// cupy URL
document.getElementById('copy-link-url').onclick = function(){
    var dummy = document.createElement('input'),
        text = window.location.href;

    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
}
// cupy URL

