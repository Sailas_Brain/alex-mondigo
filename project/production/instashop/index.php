<?include('../local/templates/mondigo/header.php');?>
<div class="page page-instashop">
    <div class="page__inner page-instashop__inner">
        <div class="page-how-to-order__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="col">
            <span class="page-instashop__title">INSTASHOP</span>
            <span class="page-instashop__text-under-title">Анонсируем новинки от брендов, предлагаем готовые образы от стилистов,
            рассказываем про тренды сезона. Купить понравившиеся товары с фото можно здесь. Кликайте по фотографии и выбирайте вещи,
            которые хотите добавить в корзину.</span>
            <span class="page-instashop__mondigo-insta"><i class="fab fa-instagram"></i>mondigo_ru</span>
            <div class="page-instashop__wrapper-element-instashop">
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>