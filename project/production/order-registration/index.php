<?include('../local/templates/mondigo/header.php');?>
<div class="page page-order-registration">
    <div class="page__inner page-order-registration__inner">
        <div class="page-order-registration__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="col">
            <span class="page-order-registration__title">Оформление заказа</span>
            <div class="order-registration tabs">
                <div class="order-registration__header-list">
                    <div class="registration-method__method-name tabs-btn active">
                        <span class="desktop-version">Вход</span>
                        <span class="backgroung">1</span>
                    </div>
                    <div class="registration-method__method-name tabs-btn">
                        <span class="desktop-version">Личные данные</span>
                        <span class="backgroung">2</span>
                    </div>
                    <div class="registration-method__method-name tabs-btn">
                        <span class="desktop-version">Доставка и оплата</span>
                        <span class="backgroung">3</span>
                    </div>
                    <div class="registration-method__method-name tabs-btn">
                        <span class="desktop-version">Подтверждение</span>
                        <span class="backgroung">4</span>
                    </div>
                </div>
                <div>
                    <div class="input tabs-block">
                        <div class="col-1">
                            <div class="page-order-registration__wrapper-left-col">
                                <span class="title">Вход</span>
                                <input type="text" placeholder="Ваше e-mail">
                                <input type="text" placeholder="Ваш пароль">
                                <div class="input">
                                    <div>
                                        <a href="#">Забыли пароль?</a>
                                        <a href="#">Зарегистрироваться</a>
                                    </div>
                                    <button>войти</button>
                                </div>
                                <span class="text-center">или</span>
                                <div class="social-network">
                                    <a href="#">Facebook</a>
                                    <a href="#">Twitter</a>
                                    <a href="#">Vkontakne</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="page-order-registration__wrapper-right-col">
                                <span class="title">Регистрация</span>
                                <input type="text" placeholder="ФИО*">
                                <input type="text" placeholder="Ваш e-mail*">
                                <input type="text" placeholder="Введите пароль*">
                                <input type="text" placeholder="Повторите пароль*">
                                <input type="text" placeholder="Ваш номер телефона (+7)*">
                                <div class="block-label">
                                    <label for="">
                                        <input type="checkbox">
                                        <span>Женский</span>
                                    </label>
                                    <label for="">
                                        <input type="checkbox">
                                        <span>Мужской</span>
                                    </label>
                                </div>
                                <input type="text" placeholder="Дата рождения (чч.мм.гг.)"> 
                                <div class="content-to-provide-data">
                                    <label for="">
                                        <input type="checkbox">
                                        <span>Согласие на предоставление данных</span>
                                    </label>
                                    <label for="">
                                        <input type="checkbox">
                                        <span>Хочу получать уведомления об акциях и скидках</span>
                                    </label>
                                </div>
                                <button>Регистрация</button>
                                <span class="text-center">или</span>
                                <div class="social-network">
                                    <a href="#">Facebook</a>
                                    <a href="#">Twitter</a>
                                    <a href="#">Vkontakne</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="personal-data tabs-block">      
                        <div class="page-order-registration__wrapper-for-personal-data">
                            <span class="page-order-registration__tabs-title">Личные данные</span>
                            <input type="text" placeholder="ФИО*">
                            <div class="page-order-registration__city">
                                <div class="text-city"><span>Город*</span></div>
                                <div class="internal-block-city">
                                    <span>Адыгея</span>
                                    <span>Крым</span>
                                    <span>Краснодар</span>
                                    <span>Ростов</span>
                                </div>
                            </div>
                            <input type="text" placeholder="Улица*">
                            <input type="text" placeholder="Дом*">
                            <input type="text" placeholder="Квартира">
                            <div class="page-order-registration__block-city">
                                <div class="block-text-city"><span>Область/республика*</span></div>
                                <div class="internal-block">
                                    <span>Оренбургская область</span>
                                    <span>Свердловская область</span>
                                    <span>Владимирская область</span>
                                    <span>Иркутская область</span>
                                </div>
                            </div>     
                            <input type="text" placeholder="Ваш номер телефона (+7)*">     
                            <div class="button">
                                <button>обновить</button>
                                <button class="resume">продолжить</button>
                            </div>                                  
                        </div>
                    </div>
                    <div class="shipping-and-payment tabs-block">
                        <div class="page-order-registration__wrapper-for-tabs-number-three">
                            <div class="page-order-registration__wrapper-list">
                                <ul>
                                    <li>Товар</li>
                                    <li>Цена</li>
                                    <li>Кол-во</li>
                                    <li>Скидка</li>
                                    <li>Сумма</li>
                                </ul>
                                <div class="page-order-registration__list-goods">
                                    <div class="mobile-version">
                                        <span>Товар</span>
                                    </div>
                                    <div class="goods">
                                        <div class="left">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                        </div>
                                        <div class="right">
                                            <a href="#" class="title">Джинсы эластичные синие</a>
                                            <span class="text">Размер: 50</span>
                                            <span class="text">Цвет: синий</span>
                                            <span>Купон:  USVM-2791</span>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <span class="mobile-version">Цена</span>
                                        <span>4.500</span>
                                    </div>
                                    <div class="quantity">
                                        <span class="mobile-version">Кол-во</span>
                                        <span>01</span>
                                    </div>
                                    <div class="discount">
                                        <span class="mobile-version">Скидка</span>
                                        <span>- 500 по купону</span>
                                    </div>
                                    <div class="sum">
                                        <span class="mobile-version">Сумма</span>
                                        <span>8.500</span>
                                    </div>
                                    <div class="like-and-close">
                                        <div class="icons">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/making-refund-second/close.svg" alt="">
                                            <button></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="page-order-registration__list-goods">
                                    <div class="mobile-version">
                                        <span>Товар</span>
                                    </div>
                                    <div class="goods">
                                        <div class="left">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                        </div>
                                        <div class="right">
                                            <a href="#" class="title">Джинсы эластичные синие</a>
                                            <span class="text">Размер: 50</span>
                                            <span class="text">Цвет: синий</span>
                                            <span>Купон:  USVM-2791</span>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <span class="mobile-version">Цена</span>
                                        <span>4.500</span>
                                    </div>
                                    <div class="quantity">
                                        <span class="mobile-version">Кол-во</span>
                                        <span>01</span>
                                    </div>
                                    <div class="discount">
                                        <span class="mobile-version">Скидка</span>
                                        <span> - </span>
                                    </div>
                                    <div class="sum">
                                        <span class="mobile-version">Сумма</span>
                                        <span>8.500</span>
                                    </div>
                                    <div class="like-and-close">
                                        <div class="icons">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/making-refund-second/close.svg" alt="">
                                            <button></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="page-order-registration__wrapper-promo-code">
                                <div class="enter-promo-code">
                                    <input type="text" placeholder="Ввести промокод">
                                    <button>применить</button>
                                </div>
                                <div class="block-sum">
                                    <div>
                                        <span class="summ">Сумма:</span>
                                        <span class="color">Вы  экономите:</span>
                                    </div>
                                    <div>
                                        <span class="underline-sum">руб 6.500</span>
                                        <span class="color">500 по купону</span>    
                                    </div>
                                </div>
                            </div>
                            <div class="page-order-registration__wrapper-delivery-and-orber">
                                <div class="col-1">
                                    <span class="title">Доставка</span>
                                    <div class="block-delivery">
                                        <label>
                                            <input type="checkbox">
                                            <span>Самовывоз. г. Москва, ул. Ленинская Слобода, д. 19, шоу-рум  1092, БЦ «Омега Плаза»</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Курьерская доставка по Москве в пределах МКАД<br>
                                            При заказе выше 2000руб. доставка бесплатно. При полном отказе,<br> покупатель оплачивает доставку 250руб.</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Курьерская доставка по России (СДЭК)<br>
                                            При заказе выше 3000руб. доставка бесплатно. При полном отказе,<br> покупатель оплачивает доставку 450руб.</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Почта России<br>
                                            При заказе выше 3000руб. доставка бесплатно. При полном отказе,<br> покупатель оплачивает доставку 350руб.</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Доставка транспортными компаниями<br>
                                            Цена договорная</span>
                                        </label>
                                    </div>
                                    <div class="buttom">
                                        <button>Добавить пункт самовывоза</button>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <span class="title">Способ оплаты:</span>
                                    <div class="page-order-registration__orber">
                                        <label>
                                            <input type="checkbox">
                                            <span>Наличными<br>
                                            Оплата наличными при получении заказа</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Оплата картой онлайн</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Qiwi-кошелек</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Требуется уточнение</span>
                                        </label>
                                    </div> 
                                </div>
                            </div>
                            <div class="page-order-registration__costomer-data">
                                <div class="data-costomer">
                                    <span class="title">ДАННЫЕ КЛИЕНТА</span>
                                    <div>
                                        <span>Крылова, 365</span>
                                        <span>675859 Зеленые сады</span>
                                        <span>Московская область</span>
                                        <span>Ирина Седакова</span>
                                        <span>E-mail:   irinasedak@mail.ru</span>
                                        <span>Моб. телефон:   +7 8655832102</span>
                                        <button>редактировать данные</button>
                                    </div>
                                </div>
                                <div class="address-delivery">
                                    <span class="title">АДРЕС ДОСТАВКИ</span>
                                    <div>
                                        <span>Крылова, 365</span>
                                        <span>675859 Зеленые сады</span>
                                        <span>Московская область</span>
                                        <span>Ирина Седакова</span>
                                        <span>E-mail:   irinasedak@mail.ru</span>
                                        <span>Моб. телефон:   +7 8655832102</span>
                                        <button>редактировать данные</button>
                                    </div>
                                </div>
                            </div>
                            <div class="page-order-registration__wrapper-general-payment">
                                <div class="wrappper-bottom-block">
                                    <div class="wrapper-sum">
                                        <div class="left">
                                            <span class="sum">Сумма:</span>
                                            <span class="color">Вы экономите:</span>
                                            <span>Доставка:</span>
                                        </div>
                                        <div class="right">
                                            <span class="sum-uppercase">руб 6.500</span>
                                            <span class="color">500 по купону</span>
                                            <span>300 рублей</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <span>Общая сумма:</span>
                                        <span class="demi">руб 6.800</span>
                                    </div>
                                    <div class="i-gree">
                                        <label>
                                            <input type="checkbox">
                                            <span>Я согласен с условиями <a href="#">Публичной оферты.</a></span>
                                        </label>
                                    </div>
                                    <div class="button">
                                        <button class="confirm">подтвердить</button>
                                        <button>продолжить покупки</button>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="confirmation tabs-block">
                        <div class="page-order-registration__wrapper-confirmation">
                            <div class="col-1">
                                <span class="title">Кристина, спасибо за ваш заказ!</span>
                                <div class="text-under-title">
                                    <span class="size">Номер вашего заказа 12336</span>
                                    <span>В качестве подтверждение заказа мы отправили вам электронное письмо. </span>
                                    <span class="color">Статус вашего заказа вы можете отслеживать в <a href="#">личном кабинете.</a></span>
                                </div>
                                <div class="button">
                                    <a href="#">вернуться в каталог</a>
                                </div>                                
                            </div>
                            <div class="col-2">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/order-registration/images-on-page-order.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>