<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-orber">
    <div class="page__inner page-my-orber__inner">
        <div class="page-my-orber__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои заказы</span>
        </div>
        <div class="page-my-orber__wrapper-for-col">
            <span></span>
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2 ">
                <span class="page-my-orber__title">Мои заказы</span>
                <div class="my-orbers tabs">
                    <div class="my-orbers__header-list">
                        <div class="delivery-method__method-name tabs-btn active"><span>Мои заказы</span></div>
                        <div class="delivery-method__method-name tabs-btn"><span>Доставки</span></div>
                        <div class="delivery-method__method-name tabs-btn"><span>Оценка качества обслуживания</span></div>
                    </div>
                    <div>
                        <div class="page-my-orber__orbers-tab tabs-block">
                            <div class="page-my-orber__mobile-accordion"><span>МОИ ЗАКАЗЫ</span></div>
                            <div class="my-orbers-tab ">
                                <div class="page-my-orber__top-button">
                                    <button>Все заказы</button>
                                    <button>Цифровой контент</button>
                                    <button>Подарочные сертификаты</button>
                                </div>
                                <div class="page-my-orber__top-navigation">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="page-my-orber__wrapper-for-list">
                                    <div class="page-my-orber__titles-list">
                                        <ul>
                                            <li>Заказ</li>
                                            <li>Дата и время заказа</li>
                                            <li>Статус</li>
                                            <li>Сумма</li>
                                        </ul>
                                    </div>
                                    <? for ($i=0; $i < 7; $i++) {?>
                                        <div class="page-my-orber__wrapper-for-internal-element">
                                            <div class="page-my-orber__element-list">
                                                <ul>
                                                    <li class="mobile-button"><span class="mobile-version">Заказ</span> <span>2127480288</span></li>
                                                    <li class="mobile-hidden-list"><span class="mobile-version">Дата и время заказа</span> <span class="desktop-version">11.03.2018 02:15</span></li>
                                                    <li class="mobile-hidden-list"><span class="mobile-version">Статус</span> <span>Закрыт</span></li>
                                                    <li class="mobile-hidden-list"><span class="mobile-version">Сумма</span><span>759</span></li>
                                                    <li class="mobile-hidden-list"><span>Способы оплаты</span></li>
                                                    <li class="mobile-hidden-list"><a href="#">Оплатить</a></li>
                                                    <li class="mobile-hidden-list"><a href="#">Отменить</a></li>
                                                </ul>
                                            </div>
                                            <div class="page-my-orber__internal-block-element-list">
                                                <span class="title">Заказ № 16922303</span>
                                                <div class="page-my-orber__internal-list">
                                                    <ul>
                                                        <li>№</li>
                                                        <li>Товар</li>
                                                        <li>Статус</li>
                                                        <li>Кол-во</li>
                                                        <li>Цена</li>
                                                        <li>Сумма</li>
                                                        <li>Способ оплаты</li>
                                                    </ul>
                                                </div>
                                                <div class="page-my-orber__orber internal-blocks">
                                                    <div class="internal-blocks__mobile-version"><span class="mobile-version">№ Товар</span></div>
                                                    <div class="internal-blocks__number">
                                                        <span>1</span>
                                                    </div>
                                                    <div class="internal-blocks__product">
                                                        <div class="images">
                                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-returns/images-for-list.jpg" alt="">
                                                        </div>
                                                        <div class="text">
                                                            <a href="#">Джинсы эластичные синие</a>
                                                        </div>
                                                    </div>  
                                                    <div class="internal-blocks__status">
                                                        <span class="mobile-version">Статус</span>
                                                        <span>Отказ 30.01.2014</span>
                                                    </div>
                                                    <div class="internal-blocks__amount">
                                                        <span class="mobile-version">Кол-во</span>
                                                        <div class="block">
                                                            <button>-</button>
                                                            <input type="number" value="1">
                                                            <button>+</button>
                                                        </div>
                                                    </div>
                                                    <div class="internal-blocks__price">
                                                        <span class="mobile-version">Цена</span>
                                                        <span class="initial-price">4.500</span>
                                                    </div>
                                                    <div class="internal-blocks__summ">
                                                        <span class="mobile-version">Сумма</span>
                                                        <span>9.000</span>
                                                    </div>
                                                    <div class="internal-blocks__payment-method">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>
                                </div>
                                <div class="page-my-orber__top-navigation mobile-hidden">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-orber__orbers-tab tabs-block">
                            <div class="page-my-orber__mobile-accordion"><span>ДОСТАВКИ</span></div>
                            <div class="delivery-tab">
                                <div class="page-my-orber__top-navigation">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="page-my-orber__wrapper-delivery-list">
                                    <div class="page-my-orber__delivery-title-list">
                                        <ul> 
                                            <li>Доставка</li>
                                            <li>Дата и время доставки</li>
                                            <li>Адрес</li>
                                            <li>Статус</li>
                                            <li>Сумма</li>
                                            <li>Сумма доставки</li>
                                        </ul>
                                    </div>
                                    <? for ($i=0; $i < 3; $i++) {?>
                                        <div class="page-my-orber__wrapper-for-elment-list-delivery internal-blocks">
                                            <div class="internal-blocks__elment-list-delivery">
                                                <div class="delivery">
                                                    <span class="mobile-version">Доставка</span>
                                                    <span>2127480288</span>
                                                </div>
                                                <div class="date-and-time">
                                                    <span class="mobile-version">Дата и время доставки</span>
                                                    <div>
                                                        <p class="initial-top">Хранение по<p>
                                                        <p>11.03.2018 02:15<p>
                                                        <p class="initial">Включительно</p>
                                                        <span class="initial-term"><i class="fas fa-check"></i>Срок хранения продлён</span>
                                                        <span class="initial">Часы работы 09:00-21:00 </span>
                                                        <button>Изменить</button>
                                                    </div>
                                                </div>
                                                <div class="address">
                                                    <span class="mobile-version">Адрес</span>
                                                    <div>
                                                        <span>Самовывоз. Москва, ул.<br>
                                                        Горького 34/3 кв. 20</span>
                                                    </div>
                                                </div>
                                                <div class="status">
                                                    <span class="mobile-version">Статус</span>
                                                    <div>
                                                        <span class="initial">Готов к
                                                        получению</span>
                                                        <span class="hidden">Резерв</span>
                                                    </div>
                                                </div>
                                                <div class="amount">
                                                    <span class="mobile-version">Сумма</span>
                                                    <div>
                                                        <span>5 689</span>
                                                        <button>Накладная</button>
                                                    </div>
                                                </div>
                                                <div class="amount-delivery">
                                                    <span class="mobile-version">Сумма доставки</span>
                                                    <span>400</span>
                                                </div>
                                            </div>
                                            <div class="internal-blocks__internal-blocks-element-list">
                                                <span class="internal-blocks__title">Доставка № 16922303</span>
                                                <div class="internal-blocks__wrapper">
                                                    <div class="internal-blocks__you-can-change-date">
                                                        <span>Вы можете изменить дату и время доставки</span>
                                                    </div>
                                                    <button class="delete-of-delivery">удалить из доставки</button>
                                                    <div class="internal-blocks__convenient-way">
                                                        <span>Выберите удобный для вас способ </span>
                                                        <label class="datepicker__label">
                                                            <input class="datepicker-from" type="text" name="birthday-orber" placeholder="12.02.1028">
                                                        </label> 
                                                        <label class="datepicker__label">
                                                            <input class="datepicker-to" type="text" name="birthday-orber" placeholder="12.02.1028">
                                                        </label>
                                                    </div>
                                                    <div class="internal-blocks__button-in-bottom">
                                                        <button>Отменить</button>
                                                        <button class="icons-save">сохранить</button>
                                                    </div>
                                                </div>
                                                <div class="wrapper-for-product wrapper-for-product-<?echo($i)?>">
                                                    <ul class="wrapper-for-product__list">
                                                        <li class="ul-list js-btn-autocheck"><i class="indicator"></i><span class="mobile-version">Выделить все</span><span class="desktop-version">Товар</span></li>
                                                        <li>Цена</li>
                                                        <li>Номер заказа</li>
                                                    </ul>
                                                    <? for ($j=0; $j < 2; $j++) { ?>
                                                    <ul class="wrapper-for-product__item-list">
                                                        <li>
                                                        <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                        <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                            <div class="item">
                                                                <span class="item__mobile-version">Товар</span>
                                                                <div class="item__checkbox-container column">
                                                                    <i></i>
                                                                </div>
                                                                <div class="item__item-info column">
                                                                    <div class="item__image-container">
                                                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                                                    </div>
                                                                    <div class="item__info-container">
                                                                        <a href="" class="color">Джинсы эластичные синие</a>
                                                                        <p>Размер: <span>38</span></p>
                                                                        <p>Цвет: <span>белый</span></p>
                                                                        <span>Купон:  USVM-2791</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item__price column">
                                                                    <span class="mobile-version">Цена</span>
                                                                    <span>4.500</span>
                                                                </div>
                                                                <div class="column">
                                                                    <span class="mobile-version">Номер заказа</span>
                                                                    <span>9585733834653</span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        </li>
                                                    <? } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>
                                    <!-- <div class="page-my-orber__wrapper-for-elment-list-delivery internal-blocks">
                                            <div class="internal-blocks__elment-list-delivery">
                                                <div class="delivery">
                                                    <span class="mobile-version">Доставка</span>
                                                    <span>2127480288</span>
                                                </div>
                                                <div class="date-and-time">
                                                    <span class="mobile-version">Дата и время доставки</span>
                                                    <div>
                                                        <p class="initial-top">Хранение по<p>
                                                        <p>11.03.2018 02:15<p>
                                                        <p class="initial">Включительно</p>
                                                        <span class="initial-term"><i class="fas fa-check"></i>Срок хранения продлён</span>
                                                        <span class="initial">Часы работы 09:00-21:00 </span>
                                                        <button>Изменить</button>
                                                    </div>
                                                </div>
                                                <div class="address">
                                                    <span class="mobile-version">Адрес</span>
                                                    <div>
                                                        <span>Самовывоз. Москва, ул.<br>
                                                        Горького 34/3 кв. 20</span>
                                                    </div>
                                                </div>
                                                <div class="status">
                                                    <span class="mobile-version">Статус</span>
                                                    <div>
                                                        <span class="initial">Готов к
                                                        получению</span>
                                                        <span class="hidden">Резерв</span>
                                                    </div>
                                                </div>
                                                <div class="amount">
                                                    <span class="mobile-version">Сумма</span>
                                                    <div>
                                                        <span>5 689</span>
                                                        <button>Накладная</button>
                                                    </div>
                                                </div>
                                                <div class="amount-delivery">
                                                    <span class="mobile-version">Сумма доставки</span>
                                                    <span>400</span>
                                                </div>
                                            </div>
                                            <div class="internal-blocks__internal-blocks-element-list">
                                                <span class="internal-blocks__title">Доставка № 16922303</span>
                                                <div class="internal-blocks__you-can-change-date">
                                                    <span>Вы можете изменить дату и время</span>
                                                    <button>удалить из доставки</button>
                                                </div>
                                                <div class="internal-blocks__convenient-way">
                                                    <span>Выберите удобный для вас способ </span>
                                                    <label class="datepicker__label">
                                                        <input class="datepicker hasDatepicker" type="text" id="datepicker-from" name="birthday" placeholder="12.02.1028">
                                                        <i class="datepicker__icon"></i>
                                                    </label> 
                                                    <label class="datepicker__label">
                                                        <input class="datepicker hasDatepicker" type="text" id="datepicker-to" name="birthday" placeholder="12.02.1028">
                                                        <i class="datepicker__icon"></i>
                                                    </label>
                                                    <button>Отменить</button>
                                                    <button class="icons-save">сохранить</button>
                                                </div>
                                                <div class="wrapper-for-product wrapper-for-product-<?echo($i)?>">
                                                    <ul class="wrapper-for-product__list">
                                                        <li class="ul-list js-btn-autocheck"><i class="indicator"></i>Товар</li>
                                                        <li>Цена</li>
                                                        <li>Номер заказа</li>
                                                    </ul>
                                                    <? for ($j=0; $j < 2; $j++) { ?>
                                                    <ul class="wrapper-for-product__item-list">
                                                        <li>
                                                        <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                        <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>-<?echo($j)?>">
                                                            <div class="item">
                                                                <div class="item__checkbox-container column">
                                                                    <i></i>
                                                                </div>
                                                                <div class="item__item-info column">
                                                                    <div class="item__image-container">
                                                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                                                    </div>
                                                                    <div class="item__info-container">
                                                                        <span class="color">Джинсы эластичные синие</span>
                                                                        <p>Размер: <span>38</span></p>
                                                                        <p>Цвет: <span>белый</span></p>
                                                                        <span>Купон:  USVM-2791</span>
                                                                    </div>
                                                                </div>
                                                                <div class="item__price column">
                                                                    <span>4.500</span>
                                                                </div>
                                                                <div class="column"><span>9585733834653</span></div>
                                                            </div>
                                                        </label>
                                                        </li>
                                                    <? } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> -->
                                </div>
                                <div class="page-my-orber__top-navigation mobile-hidden">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-orber__orbers-tab tabs-block">
                            <div class="page-my-orber__mobile-accordion"><span>ОЦЕНКА КАЧЕСТВА ОБСЛУЖИВАНИЯ</span></div>
                            <div class="rate-service-tab">
                                <div class="page-my-orber__top-navigation">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="page-my-orber__wrapper-block-rate-service">
                                    <div class="page-my-orber__rate-service-list">
                                        <ul>
                                            <li>Доставка</li>
                                            <li>Дата и время<br> доставки</li>
                                            <li>Адрес</li>
                                            <li>Имя курьера</li>
                                            <li>Оценить обслуживание</li>
                                        </ul>
                                    </div>
                                    <? for ($i=0; $i < 4; $i++) { ?>
                                    <div class="page-my-orber__internal-rate-service-list">
                                        <ul>
                                            <li><span class="mobile-version">Доставка</span> 2127480288</li>
                                            <li><span class="mobile-version">Дата и время заказа</span> 11.03.2018 02:15</li>
                                            <li><span class="mobile-version">Адрес</span> Москва, ул. Горького<br> 34/3 кв. 20</li>
                                            <li><span class="mobile-version">Имя курьера</span> Александр</li>
                                            <li><span class="mobile-version">Оценить обслуживание</span> <button class="button-courier-service">Оценить обслуживание</button></li>
                                        </ul>
                                    </div>
                                    <? } ?>
                                </div>
                                <div class="page-my-orber__top-navigation mobile-hidden">
                                    <ul>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"> <a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
<div class="modal modal-courier-service">
    <div class="modal__inner modal-courier-service__inner">
        <div class="col">
            <span class="modal-courier-service__title">Обслуживание курьером</span>
            <div class="modal-courier-service__interanal-wrapper">
                <span>Для улучшения кач-ва сервиса и работы интернет-магазина, пожалуйста оцените обслуживание.</span>
                <div class="modal-courier-service__assessnent-work-courier">
                    <span>1. Оцените кач-во работы курьера</span>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/assessment.png" alt="">
                </div>
                <div class="modal-courier-service__your-comment">
                    <p class="title">2. Ваши замечания</p>
                    <label>
                        <input type="checkbox">
                        <span>Не вежливый курьер</span>
                    </label>
                    <label>
                        <input type="checkbox">
                        <span>Курьер приехал не в указанное время</span>
                    </label>
                    <label>
                        <input type="checkbox">
                        <span>Курьер не предупредил заранее о доставке заказа</span>
                    </label>
                    <label>
                        <input type="checkbox">
                        <span>Курьер не предоставил положенное время на примерку (20 мин)</span>
                    </label>
                    <label>
                        <input type="checkbox">
                        <span>Отсутствие кассового аппарата</span>
                    </label>
                    <label>
                        <input type="checkbox">
                        <span>Отсутствие терминала безналичной оплаты</span>
                    </label>
                </div>
                <div class="modal-courier-service__your-comment-and-wishes">
                    <span>3. Ваши комментарии и пожелания по работе курьера интернет-магазина</span>
                    <textarea></textarea>
                </div>
                <div class="modal-courier-service__button">
                    <button>отменить</button>
                    <button class="send">отправить</button>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>  
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>