<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-dispatch">
    <div class="page__inner page-my-dispatch__inner">
        <div class="page-my-data__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-dispatch__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div> 
            <div class="col-2">
                <span class="page-my-dispatch__title">Мои рассылки</span>
                <p class="page-my-dispatch__text-under-title">Вы можете подписаться на следующие виды рассылок нашего магазина для получения новостей про акции и <span>промокоды:</span> </p>
                <div class="page-my-dispatch__internal-wrapper">
                    <div class="my-dispatch tabs">
                        <div class="my-dispatch__header-dispatch">
                            <div class="dispatch-method__method-name tabs-btn active"><span>E-mail рассылки</span></div> 
                            <div class="dispatch-method__method-name tabs-btn"><span>СМС-рассылки</span></div>
                        </div>
                        <div>
                            <div class="page-my-dispatch__mobile-version tabs-block">
                                <div class="page-my-dispatch__button-mobile-version"><button>E-MAIL РАССЫЛКИ</button></div>
                                <div class="block-email-dispatch">
                                    <div class="page-my-dispatch__checkbox-dispatch">
                                        <label>
                                            <input type="checkbox">
                                            <span>Акции, персональные промокоды и секретные распродажи</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" checked>
                                            <span>Лист ожидания</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Личные рекомендации товаров</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="page-my-dispatch__mobile-version tabs-block">
                                <div class="page-my-dispatch__button-mobile-version"><button>СМС-РАССЫЛКИ</button></div>
                                <div class="block-SMS-dispatch">
                                    <div class="page-my-dispatch__checkbox-dispatch">
                                        <label>
                                            <input type="checkbox">
                                            <span>Акции, персональные промокоды и секретные распродажи</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Лист ожидания</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" checked>
                                            <span>Личные рекомендации товаров</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-dispatch__button">
                            <button>сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>