<?include('../local/templates/mondigo/header.php');?>
<div class="page page-style-pages">
    <div class="page__inner page-style-pages__inner">
        <div class="page-style-pages__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои обращения</span>
        </div>
        <span class="page-style-pages__title">Выбери свой стиль</span>
        <div class="col">
            <div class="page-style-pages__wrapper-background-images">
                <a href="#"><div class="page-style-pages__backgroung-images">
                    <div class="inrernal-text">
                        <span>classic</span>
                        <button>купить</button>
                    </div>
                </div></a>
                <a href="#"><div class="page-style-pages__wrapper-background-second-images">
                    <div class="internal-block">
                        <div class="element">
                            <span>Business</span>
                            <button>купить</button>
                        </div>
                    </div>
                    <div class="internal-block two">
                        <div class="element">
                            <span>marine</span>
                            <button>купить</button>
                        </div>
                    </div>
                </div></a>
                <a href="#"><div class="page-style-pages__backgroung-images initial">
                    <div class="inrernal-text">
                        <span>Active Wear</span>
                        <button>купить</button>
                    </div>
                </div></a>
                <a href="#"><div class="page-style-pages__backgroung-images second">
                    <div class="inrernal-text">
                        <span>Casual</span>
                        <button>купить</button>
                    </div>
                </div></a>
            </div>
        </div> 
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>