<?include('/local/templates/mondigo/header.php');?>
<div class="page page-main">
    <div class="page__inner page-main__inner">
        <div class="page-main__background-images-top">
            <div class="page-main__background-internal internal-blocks">
                <h1>весна-лето 2018</h1>
                <span>Discover the making of the new object of desire. </span>
                <span>Now available to purchase</span>
                <a href="#">каталог</a>
            </div>
        </div>
        <div class="page-main__wrapper-our-model">
            <div class="blocks-one">
                <div class="blocks-one__title">
                    <h2>Наши Модели</h2>
                    <span>There are many variations of passages of Lorem Ipsum available, but the majority 
                    have suffered alteration in some form, by injected humour, or randomised words which
                    don't look even slightly believable.</span>
                </div>
                <div class="blocks-one__background-image">
                    <div class="blocks-one__internal-element">
                        <span>Платья</span>
                        <a href="#">купить</a>
                    </div>
                </div>
            </div>
            <div class="blocks-two">
                <div class="blocks-two__background-images">
                    <div class="blocks-two__link">
                        <a href="#">купить</a>
                    </div>
                </div>
            </div>
            <div class="blocks-three">
                 <div class="blocks-three__background-images">
                    <div class="blocks-three__link">
                        <span>рубашки</span>
                        <a href="#">купить</a>
                    </div>
                </div>
            </div>
            <div class="blocks-four">
                <div class="blocks-four__title">
                    <span>There are many variations of passages of Lorem Ipsum available, 
                    but the majority have suffered alteration in some form, by injected humour,
                    or randomised words which don't look even .</span>
                </div>
                <div class="blocks-four__background-image">
                    <div class="blocks-four__internal-element">
                        <a href="#">купить</a>
                    </div>
                </div>
            </div>
            <div class="page-main__wrapper-for-hidden-block">
                <div class="blocks-one">
                    <div class="blocks-one__title">
                        <h2>Наши Модели</h2>
                        <span>There are many variations of passages of Lorem Ipsum available, but the majority 
                        have suffered alteration in some form, by injected humour, or randomised words which
                        don't look even slightly believable.</span>
                    </div>
                    <div class="blocks-one__background-image">
                        <div class="blocks-one__internal-element">
                            <span>Платья</span>
                            <a href="#">купить</a>
                        </div>
                    </div>
                </div>
                <div class="blocks-two">
                    <div class="blocks-two__background-images">
                        <div class="blocks-two__link">
                            <a href="#">купить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-main__bottom-our-model">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/main/loading.png" alt="">
            <button class="btn bth-download-more">загрузить ещё</button>
        </div>
        <div class="page-main__opportunity-information">
            
            <div class="page-main__delivery delivery-images">
                <span>Бесплатная доставка по<br> Москве. Самовывоз.</span>
            </div>
            <div class="page-main__methond-payment payment-images">
                <span>Разные способы<br> оплаты</span> 
            </div>
            <div class="page-main__fitting-product fitting-images">
                <span>Примерка<br> товара</span>
            </div>
            <div class="page-main__return-of-goods return-images">
                <span>Возврат товара в течении<br> 14 дней</span>
            </div>
        </div>
        <div class="page-main__new-collection">
            <h3>Новая коллекция</h3>
            <span>There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by injected humour,
                or randomised words which don't look even slightly believable.</span>
        </div>
        <div class="page-main__flex-slider">
            <? for ($i=0; $i < 4; $i++) { ?>
                <div class="page-main__slider-new-collection">
                    <div class="page-main-item-slider">
                        <div class="page-main__big-preview">
                            <div class="page-main__slider-images image-slider-hover">
                                <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                            </div>
                            <div class="page-main__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                            </div>
                            <div class="page-main__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                            </div>
                            <div class="page-main__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-main__wrapper-for-hover">
                            <span class="page-main__icon-heart"></span>
                            <div class="page-main__add-to-cart">
                                <div class="in-cart">
                                    <span>В корзину</span>
                                </div>
                                <div class="choice-of-size">
                                    <span>Выбрать размер: </span>
                                    <ul>
                                        <li><a href="#">42</a></li>
                                        <li><a href="#">43</a></li>
                                        <li><a href="#">44</a></li>
                                        <li><a href="#">45</a></li>
                                        <li><a href="#">46</a></li>
                                        <li><a href="#">47</a></li>
                                        <li><a href="#">48</a></li>
                                        <li><a href="#">49</a></li>
                                        <li><a href="#">50</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="page-main__info-bottom-slider">
                            <span>Купальник</span>
                            <span class="line-through">руб 3.400</span>
                            <span class="color-red">РУБ 3.000 по купону</span>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? for ($i=0; $i < 4; $i++) { ?>
                <div class="page-main__slider-new-collection">
                <div class="page-main-item-slider">
                    <div class="page-main__big-preview">
                        <div class="page-main__slider-images image-slider-hover">
                            <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-5.jpg" alt="">
                            <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-6.jpg" alt="">
                        </div>
                        <div class="page-main__slider-images">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                        </div>
                        <div class="page-main__slider-images">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                        </div>
                        <div class="page-main__slider-images">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                        </div>
                    </div>
                    <div class="page-main__wrapper-for-hover">
                            <span class="page-main__icon-heart"></span>
                            <div class="page-main__add-to-cart second-size">
                                <div class="in-cart">
                                    <span>В корзину</span>
                                </div>
                                <div class="choice-of-size">
                                    <span>Выбрать размер: </span>
                                    <ul>
                                        <li><a href="#">42</a></li>
                                        <li><a href="#">43</a></li>
                                        <li><a href="#">44</a></li>
                                        <li><a href="#">45</a></li>
                                        <li><a href="#">46</a></li>
                                        <li><a href="#">47</a></li>
                                        <li><a href="#">48</a></li>
                                        <li><a href="#">49</a></li>
                                        <li><a href="#">50</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <div class="page-main__info-bottom-slider">
                        <span>Купальник</span>
                        <span class="line">руб 3.400</span>
                        <!-- <span class="color-red">РУБ 3.000 по купону</span> -->
                    </div>
                </div>
            </div>
            <? } ?>
        </div>
    </div>
    <div class="page-main__subscribe internal-block">
        <div class="internal-block__inner">
            <div class="internal-block__subscribe-info">
                <span class="internal-block__title">Подпишитесь на нашу рассылку</span>
                <input type="text" name="email" placeholder="Введите ваш e-mail">
                <div class="internal-block__images-social-networks">
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-vk"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-odnoklassniki"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('/local/templates/mondigo/footer.php');?>