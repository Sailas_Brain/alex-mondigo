<?define('SITE_TEMPLATE_PATH', '/local/templates/mondigo')?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- vendor -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick-theme.css" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main.css">
    <title>Document</title>
</head>
<body>
<div class="header">
    <div class="header__inner">
        <div class="header__wrapper-for-col internal-blocks">
            <div class="internal-blocks-col-1">
                <a href="/link/index.php" class="internal-blocks__logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                </a>
            </div>
            <div class="internal-blocks-col-2">
                <div class="internal-blocks__burger-menu"><button class="btn button-burger-menu"></button></div>
                <div class="internal-blocks__background-burger-menu">
                    <div class="internal-blocks__block-manu">
                        <div class="internal-blocks__novelties">
                            <div class="">
                                <span>Новинки</span>
                            </div> 
                            <div class="internal-blocks__block-menu novelties">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="elements">
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-1.jpg" alt="">
                                        <span>Новинки женские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-2.jpg" alt="">
                                        <span>Новинки мужские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-3.jpg" alt="">
                                        <span>Аксессуары</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="internal-blocks__female">
                            <div>
                                <span>Женская</span>
                            </div>
                            <div class="internal-blocks__block-menu female">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="internal-blocks__male">
                            <div>
                                <span>Мужская</span>
                            </div>
                            <div class="internal-blocks__block-menu male">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <a href="#" class="iinternal-blocks__InstaShop">InstaShop</a>
                    </div>
                </div>
            </div>
            <div class="internal-blocks-col-3">
                <div class="internal-blocks__messege">
                    <div class="internal-blocks__block-images messege"></div>
                    <div>

                    </div>
                </div>
                <div class="internal-blocks__phone">
                    <div class="internal-blocks__block-images phone"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__lock">
                    <div class="internal-blocks__block-images lock"></div>
                    <div class="internal-blocks__internal-block-lock">
                        <div class="links">
                            <a class="uppercase" href="#">Товары</a>
                            <a class="active" href="#">Отложенные товары</a>
                            <a class="bottom-text" href="#">Лист Ожидания</a>
                            <a class="uppercase" href="#">Работа с заказом</a>
                            <a href="#">Мои заказы</a>
                            <a href="#">Мои доставки</a>
                            <a href="#">Мой баланс</a>
                            <a href="#">Личные предложения</a>
                            <a href="#">Гардероб</a>
                            <a class="bottom-text" href="#">Оформление возврата</a>
                            <a class="uppercase" href="#">Профиль</a>
                            <a href="#">Мои данные</a>
                            <a href="#">Моя скидка 5%</a>
                            <a href="#">Мои рассылки</a>
                            <a class="bottom-text" href="#">Новости</a>
                            <a class="uppercase" href="#">Обратная связь</a>
                            <a href="#">Мои обращения</a>
                            <a class="bottom-text" href="#">Проверка товара</a>
                            <a href="#">Выйти</a>
                        </div>                   
                    </div>
                </div>
                <div class="internal-blocks__heart">
                    <div class="internal-blocks__block-images heart"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__serch">
                    <div class="internal-blocks__block-images serch"></div>
                    
                </div>
                <div class="internal-blocks__basket">
                    <div class="nember-basket"><span>2</span></div>

                    <div class="internal-blocks__basket-dropdawn internal-basket">
                    
                        <span class="internal-basket-title">Корзина</span>
                        <div class="internal-basket-product-basket">
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                        </div>

                        <div class="internal-basket__price">
                            <span>Итого:</span>
                            <span>руб 6.500</span>
                        </div>



                        <div class="internal-basket__lenk-basket">
                            <a href="#">перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="internal-blocks__input-search">
                <span>Что ищем? |</span>
                <span class="close-in-search"></span>
                <input type="search" placeholder="">
            </div>
        </div>
    </div>
</div>
<div class="page page-my-data">
    <div class="page__inner page-my-data__inner">
        <div class="page-my-data__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-data__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div> 
            <div class="col-2">
                <h1 class="page-my-data__title">Мои данные</h1>
                <div class="page-my-data__internal-blocks-second-col">
                    <div class="col-2-1">
                        <div class="page-my-data__personal-data">
                            <span class="page-my-data__internal-title">Персональные данные</span>
                            <div class="page-my-data__personal-info">
                                <div class="left">
                                     <span>ФИО:</span>
                                     <span class="email">E-mail:</span>
                                     <span>Дата рождения:</span>
                                     <span>Пол:</span>
                                </div>
                                <div class="right">
                                    <span>Щербинина Кристина Сергеевна</span>
                                    <span>kristinaa89@bk.ru</span>
                                    <span>23 мая 1989 г.</span>
                                    <span>Женский</span>
                                </div>
                            </div>
                            <div class="page-my-data__button-chenge">
                                <button class="btn button-personal-data">Изменить персональные данные</button>
                            </div>
                            <div>
                                <button class="page-my-data__button-confirm">подтвердить</button>
                            </div>
                        </div>
                        <div class="page-my-data__parameters-figure">
                            <span class="page-my-data__internal-title">Параметры фигуры</span>
                            <div class="page-my-data__parameters">
                                <div class="left">
                                    <span>Рост (см)</span>
                                    <span>Вес (кг)</span>
                                    <span>Воротник (см)</span>
                                    <span>Плечи (см)</span>
                                    <span>Живот (см)</span>
                                    <span>Бицепс (см)</span>
                                    <span>Длина рукова (см)</span>
                                    <span>Грудь (см)</span>
                                    <span>Бедра (см)</span>
                                    <span>Талия (см)</span>
                                    <span>Обхват запястья (см)</span>
                                    <span>Длина рубашки (см)</span>
                                </div>
                                <div class=right>
                                    <div>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                    </div>
                                    <div>
                                    <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                        <span>-</span>
                                    </div>
                                </div>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button class="btn button-chenge-params">Изменить параметры</button>
                            </div>
                        </div>
                        <div class="page-my-data__distribution">
                            <span class="page-my-data__internal-title">Рассылки</span>
                            <div class="page-my-data__newsletter">
                                <div class="text">
                                    <p>
                                    Вы можете подписаться на следующие виды рассылок нашего магазина для
                                    получения новостей про акции и <span>промокоды</span>:
                                    </p>
                                </div>
                                <div class="special-offer">
                                    <p>Email рассылки</p>
                                    <label>
                                        <input type="checkbox">
                                        <span>Акции, персональные промокоды и секретные <b class="span-text">распродажи</b></span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Лист ожидания</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Личные рекомендации товаров</span>
                                    </label>
                                </div>
                                <div class="botton-block">
                                    <p>SMS рассылки</p>
                                    <label>
                                        <input type="checkbox">
                                        <span>SMS-сообщения</span>
                                    </label>
                                    <button class="page-my-data__button-confirm">подтвердить</button>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-data__delite-personal-account">
                            <span class="page-my-data__internal-title">Удаление личного кабинета</span>
                            <span>Как только Ваш Личный Кабинет будет удален, Вы
                                автоматически выйдете из системы и больше не
                                сможете войти в этот аккаунт.</span>
                            <div class="page-my-data__button-internal-block">
                                <button>Удалить личный кабинет</button>
                            </div>
                        </div>
                        <div class="page-my-data__social-network">
                            <span class="page-my-data__internal-title">Социальные сети</span>
                            <span>Нажмите на соотвествующий значок социальной сети, чтобы связать ее с Вашим личным кабинетом</span>
                            <div class="block-icons">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                                <a href="#"><i class="fab fa-vk"></i></a>
                                <a href="#"><i class="fas fa-at"></i></a>
                                <a href="#"><i class="fab fa-odnoklassniki"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-2-2">
                        <div class="page-my-data__number-phone">
                            <span class="page-my-data__internal-title">Номер мобильного</span>
                            <div class="text">
                                <span>Изменить номер мобильного телефона можно не более 2-х раз в сутки.</span>
                                <span class="tel">+7 (965) 242-29-99</span>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button class="btn button-change-number">Изменить телефон</button>
                            </div>
                        </div>
                        <div class="page-my-data__adres-email">
                            <span class="page-my-data__internal-title">Адрес электронной почты</span>
                            <div class="text">
                                <span>kristinaa89@bk.ru</span>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button class="change-email">Изменить адрес электронной почты</button>
                            </div>
                        </div>
                        <div class="page-my-data__password">
                            <span class="page-my-data__internal-title">Пароль</span>
                            <div class="text">
                                <span>Здесь Вы можете изменить свой пароль для входа в личный кабинет</span>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button class="change-password">Смена пароля</button>
                            </div>
                        </div>
                        <div class="page-my-data__additional-protection">
                            <span class="page-my-data__internal-title">Дополнительная защита</span>
                            <div class="text">
                                <span>Данная функция позволяет настроить вход на сайт с подтверждением по СМС</span>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button class="button-setting">Настройка</button>
                            </div>
                        </div>
                        <div class="page-my-data__delivery-address">
                            <span class="page-my-data__internal-title">Адреса доставки</span>
                            <div class="text">
                                <span>Дымова Лариса Сергеевна, +7 (926) 220-63-86</span>
                                <button class="edditing"></button>
                                <button class="close"></button>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button>Добавить адрес</button>
                            </div>
                        </div>
                        <div class="page-my-data__requisites">
                            <span class="page-my-data__internal-title">Реквизиты</span>
                            <div class="page-my-data__internal-requisites">
                                <div class="images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                                </div>
                                <div class="text">
                                    <span>Уважаемые Клиенты! Обращаем Ваше внимание на то, что в связи с ограничениями стандартов 
                                    безопасности платежных шлюзов PCI DSS, возникают трудности с возвратами денежных средств на банковские счета, 
                                    начинающиеся на 302*******. Ограничения связаны с передачей номера карты для совершения транзакции.</span>
                                </div>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button>Добавить реквизиты</button>
                            </div>
                        </div>
                        <div class="page-my-data__delivery-address">
                            <span class="page-my-data__internal-title">Адреса доставки</span>
                            <div class="text">
                                <span>Дымова Лариса Сергеевна, +7 (926) 220-63-86</span>
                                <button class="edditing"></button>
                                <button class="close"></button>
                            </div>
                            <div class="page-my-data__button-internal-block">
                                <button>Добавить адрес</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-personal-data">
    <div class="modal__inner modal-personal-data__inner">
        <div class="col">
            <span class="modal-personal-data__title">Персональные данные</span>
            <div class="modal-personal-data__internal internal-blocks">
                <div class="text">
                    <span>Для изменения ФИО Вам необходимо написать заявление от руки в свободной форме с просьбой изменить данные. 
                    В заявлении необходимо обязательно указать причину смены данных и номер мобильного телефона, указанный в ЛК. 
                    Заявление можно отправить Заказным письмом на юридический адрес: 142715, 
                    Московская область, Ленинский район, деревня Мильково, владение 1 — или передать его на ближайший пункт самовывоза.</span>
                    <span>Чтобы получать подарки и сюрпризы, пожалуйста, укажите свои данные.</span>
                </div>
                <div class="private-data">
                    <div class="data-of-dirth">
                        <input type="text" placeholder="Дата рождения (чч.мм.гг.)">
                        <button></button>
                    </div>
                    <div class="internal-block-list">
                        <select name="" id="">
                            <option value="Показывать дату рождения">Показывать дату рождения</option>
                            <option value="20.09.2018">Скрыть дату рождения</option>
                            <option value="20.09.2018">Показывать дату рождения</option>
                        </select>
                    </div>
                </div>
                <div class="caution">
                    <div class="left">
                        <img src="/local/templates/mondigo/images/my-data/exclamation.svg" alt="">
                    </div>
                    <div class="right">
                        <span>В случае, если на Личном Счете Клиента есть денежные средства,
                        перед изменением данных представители интернет-магазина оставляют 
                        за собой право потребовать документ, удостоверяющий личность.</span>
                    </div>
                </div>
                <div class="modal-button">
                    <button class="btn btn-cancellation-modal">отменить</button>
                    <button class="button">сохранить</button>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
        
    </div>
</div>
<div class="modal modal-change-parametrs">
    <div class="modal__inner modal-change-parametrs__inner">
        <div class="col">
            <span class="modal-change-parametrs__title">Мои параметры фигуры</span>
            <div class="modal-change-parametrs__internal-wrapper">
                <div class="modal-change-parametrs__parametrs-block">
                    <span>Рост (см)</span>
                    <span>Вес (кг)</span>
                    <span>Воротник (см)</span>
                    <span>Плечи (см)</span>
                    <span>Живот (см)</span>
                    <span>Бицепс (см)</span>
                    <span>Длина рукова (см)</span>
                    <span>Грудь (см)</span>
                    <span>Бедра (см)</span>
                    <span>Талия (см)</span>
                    <span>Обхват запястья (см)</span>
                    <span>Длина рубашки (см)</span>
                </div>
                <div class="modal-change-parametrs__values-block">
                    <input type="text" placeholder="см">
                    <input type="text" placeholder="кг">
                    <? for ($i=0; $i < 10; $i++) {?>
                        <input type="text" placeholder="см">
                    <? } ?>
                </div>
                <div class="modal-change-parametrs__images-block">
                    <div class="image">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/human-body-page-data.jpg" alt="">
                    </div>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-change-number-phone">
    <div class="modal__inner modal-change-number-phone__inner">
        <div class="col">
            <span class="modal-change-parametrs__title">Изменение номера телефона</span>
            <div class="modal-change-number-phone__point-number">
                <span>Укажите новый номер</span>
                <input type="text" placeholder="Ваш номер телефона (+7)*">
            </div>
            <div class="modal-change-number-phone__text">
                <span>Изменить номер мобильного телефона можно 
                не более 2-х раз в сутки. В случае неоднократного подтверждения
                одного и того же номера в разных Личных кабинетах, номер заблокируется.
                При наличии денежных средств на Личном счете Клиента,
                интернет-магазин оставляет за собой право потребовать документ, удостоверяющий личность.</span>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-change-email">
    <div class="modal__inner modal-change-email__inner">
        <div class="col">
            <span class="modal-change-email__title">Изменение электронной почты</span>
            <div class="modal-change-email__enter-your-mail">
                <input type="text" placeholder="Введите ваш e-mail">
            </div>
            <div class="modal-change-email__text">
                <span>На указанный Вами электронный адрес будет выслано письмо с ссылкой, 
                перейдите по ней для подтверждения адреса электронной почты.</span>
                <span>Подписки на рассылки, выбранные Вами ранее, сохраняются 
                при изменении электронного адреса.</span>
            </div>
            <div class="modal-change-email__modal-button">
                <button class="btn btn-cancellation-modal">отменить</button>
                <button class="button">сохранить</button>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-change-password">
    <div class="modal__inner modal-change-password__inner">
        <div class="col">
            <span class="modal-change-password__title">Изменение пароля</span>
            <div class="modal-change-password__change-password">
                <input type="text" placeholder="Текущий пароль*">
                <input type="text" placeholder="Новый пароль*">
                <input type="text" placeholder="Подтверждение пароля*">
            </div>
            <div class="modal-change-password__button">
                <button class="btn btn-cancellation-modal">отменить</button>
                <button class="button">сохранить</button>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-addition-protection">
    <div class="modal__inner modal-addition-protection__inner">
        <div class="col">
            <span class="modal-addition-protection__title">Дополнительная защита для входа</span>
            <div class="modal-addition-protection__text">   
                <span>При включенной функции, для входа на сайт, помимо логина и пароля,
                потребуется дополнительно ввести код, который будет приходить Вам по СМС.</span>            
            </div>
            <div class="modal-addition-protection__protection-your-account">
                <span class="title">Это значительно увеличит безопасность Вашего аккаунта</span>
                <div class="internal-block">
                    <label for="enadled">
                            <input type="radio" value="Включено" id="enadled" name="button" checked="">
                            <span></span>Включено
                    </label>
                    <label for="tarned-off">
                            <input type="radio" value="Выключено" id="tarned-off" name="button">    
                            <span></span>Выключено
                    </label>
                </div>
            </div>
            <div class="modal-addition-protection__button">
                <button class="btn btn-cancellation-modal">отменить</button>
                <button class="button">применить</button>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>

<div class="footer">
    <div class="footer__inner">
        <div class="footer__internal internal-blocks"> 
            <div class="col-1">
                <div class="internal-blocks__information">
                <span class="footer__title">контаткы</span>
                    <div class="internal-blocks__address">
                    <i class="fas fa-map-marker-alt"></i>
                        <span> г. Москва, ул. Нижняя Красносельская, д. 40/12, корп. 6, офис 8 </span>
                    </div>
                    <div class="internal-blocks__massege">
                    <i class="fas fa-envelope"></i>
                        <span>info@mondigo.ru</span>
                    </div>
                    <div class="internal-blocks__number-phone">
                        <i class="fas fa-phone"></i>
                        <span>8 800 775 88 38 (звонок бесплатный) 8 495 775 88 38</span>
                    </div>
                </div>
                <div class="internal-blocks__means-of-payment"> 
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/visa.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mastercard.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mir-logo-h14px.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/qiwi.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/maestro.svg" alt="">
                </div>
            </div>
            <div class="col-2"> 
                <span class="footer__title">О компании</span>
                <div class="internal-blocks__about-company">
                    <a href="#">О нас</a>
                    <a href="#">Контакты</a>
                    <a href="#">Сертификаты</a>
                    <a href="#">Преимущества</a>
                    <a href="#">Наши скидки</a>
                </div>
            </div>
            <div class="col-3">
                <span class="footer__title">Обслуживание клиентов</span>
                <div class="internal-blocks__client-service">
                    <a href="#">Как делать заказ</a>
                    <a href="#">Возврат товара</a>
                    <a href="#">Публичная оферта</a>
                    <a href="#">Доставка</a>
                    <a href="#">Способы оплаты</a>
                    <a href="#">Возврат денежных средств</a>
                    <a href="#">Вопросы и ответы</a>
                    <a href="#">Подарочные карты</a>
                    <a href="#">Сотрудничество оптом</a>
                </div>
            </div>
            <div class="col-4">
                <span class="footer__title">прочее</span>
                <div class="internal-blocks__etc-block">
                    <a href="#">Фабрика</a>
                    <a href="#">Обратная связь</a> 
                </div>
            </div>
        </div>
    </div>
        <div class="footer__bottom-block">
            <div class="footer__bottom-inner">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                <span>© 1999-2018 Mondigo.ru</span>
            </div>
        </div>
</div>

















<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
                                    <!-- vendor -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/slick.js"></script>

                                    <!-- jS -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
</body>
</html>