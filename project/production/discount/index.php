<?include('../local/templates/mondigo/header.php');?>
<div class="page page-discount">
    <div class="page__inner page-discount__inner">
        <div class="page-discount__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="page-discount__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <div class="page-discount__my-discount">
                    <div class="left">
                        <span>МОЯ СКИДКА</span>
                        <span class="color">0 <span class="percent">%</span></span>
                    </div>
                    <div class="right-text">
                        <span>Скидка рассчитывается на основе покупок, сделанных через Личный кабинет,
                        и зависит от суммы выкупа и процента выкупа заказанных товаров за последние полгода (183 дня).
                        Если Клиент не совершает покупок и возвратов в течение полугода, процент выкупа рассчитывается
                        за весь период сотрудничества.</span>
                        <button class="button-delivery">Как рассчитать?</button>
                    </div>
                </div>
                <div class="page-discount__actions-limits">
                    <span>Ограничения действия скидки постоянного покупателя</span>
                </div>
                <div class="page-discount__wrapper-for-bottom-block">
                    <div class="page-discount__elenent-bottom-block">
                        <span class="uppercase-border">СУММА ВЫКУПА</span>
                        <span class="uppercase">29 553 руб.</span>
                        <!-- <span></span> -->
                    </div>
                    <div class="page-discount__elenent-bottom-block color-dark-blue">
                        <span class="uppercase-border">ПРОЦЕНТ ВЫКУПА</span>
                        <span class="uppercase">22,61%</span>
                        <button class="button-percent-ransom">Как рассчитать?</button>
                    </div>
                    <div class="page-discount__elenent-bottom-block color-grey">
                        <span class="uppercase-color-border">ДОСТАВКА БЕЗ ПРЕДОПЛАТЫ</span>
                        <span class="uppercase-color">до 30 шт.</span>
                        <button class="button-delivery">Как рассчитать?</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-percent-ransom">
    <div class="modal__inner modal-percent-ransom__inner">
        <div class="col">
            <span class="modal-percent-ransom__title">Таблица расчета процента выкупа</span>
            <div class="modal-percent-ransom__wrapper-for-calculation-table">
                <div class="block-1">
                    <span>Оплачено</span>
                    <span>(Возвращено + Оплачено - Брак)</span>
                </div>
                <div class="block-2">
                <span>x</span>
                <span>100%</span>
                <span>=></span>
                </div>
                <div class="block-3">
                <span>10 754</span>
                <span>(37 574 + 10 754 - 0)</span>
                </div>
                <div class="block-4">
                <span>x</span>
                <span>100%</span>
                <span>=</span>
                <span>22,25%</span>
                </div>
            </div>
            <div class="modal-percent-ransom__blocks-calculation">
                <div class="block-left">
                    <span class="text">Расчет за период c </span>
                    <span class="color">20.09.2017 по 22.03.2018</span>
                    <span>(обновляется каждый день)</span>
                </div>
                <div class="block-right">
                    <span class="text">Процент выкупа на завтра составит</span>
                    <span class="color">22,25%</span>
                    <div class="modal-percent-ransom__calculation-of-interest">
                        <div class=""><button>Калькулятор расчета процента</button></div>
                        <div class="internal-calculator">
                            <span>Введите сумму предполагаемой покупки, возврата и узнайте, как изменится Ваш процент выкупа</span>
                            <div class="internal-calculator__block-sum-return">
                                <label>
                                    <span>Сумма покупки</span>
                                    <input type="text" placeholder="1500">
                                </label>
                                <label>
                                    <span>Сумма возврата</span>
                                    <input type="text" placeholder="700">
                                </label>
                            </div>
                            <div class="internal-calculator__calculate">
                                <button>рассчитать</button>
                                <span>22,25%</span>
                            </div>
                            <span class="botton-text">Расчет предварительный, произведен на текущий момент на основании<br> введенных данных</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-percent-ransom__wrapper-product-list">
                <ul>
                    <li>№ заказа</li> 
                    <li>Оплачено</li> 
                    <li>Возвращено</li> 
                    <li>Учет в проценте выкупа</li> 
                </ul>
                <? for ($i=0; $i < 3; $i++) {?>
                    <div class="modal-percent-ransom__element-product-list">
                        <div class="number-order">
                            <span>24977654</span>
                        </div>
                        <div class="goods">
                            <span class="title">Джинсы эластичные синие</span>
                            <span class="text">Размер: 50</span>
                            <span class="text">Цвет: синий</span>
                            <span>Купон:  USVM-2791</span>
                        </div>
                        <div class="payment">
                            <span class="mobile-version">Оплачено</span>
                            <span>1 148 руб</span>
                            <span>11.11.2017</span>
                        </div>
                        <div class="returned">
                            <span class="mobile-version">Возвращено</span>
                            <span>1 148 руб</span>
                            <span>11.11.2017</span>
                        </div>
                        <div class="percent-ransom">
                            <span class="mobile-version">Учёт в проценте выкупа</span>
                        </div>
                    </div>
                <? } ?>
                <div class="modal-percent-ransom__result">
                    <div class="result">
                        <span>Итого:</span>
                        <span>Итого (кол-во):</span>
                    </div>
                    <div class="modal-percent-ransom__wrapper-for-sum">
                        <div class="payment">
                            <span class="mobile-version">Оплачено</span>
                            <span>10 754</span>
                            <span class="mobile-version">Возвращено</span>
                            <span>14</span>
                        </div>
                        <div class="returned">
                            <span class="mobile-version">Оплачено</span>
                            <span>37 574</span>
                            <span class="mobile-version">Возвращено</span>
                            <span>22</span>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-total-number-goods">
    <div class="modal__inner modal-total-number-goods__inner">
        <div class="col">
            <span class="modal-total-number-goods__title">Как рассчитать общее кол-во товаров, которое можно оформить
            на доставку без предварительной оплаты</span>
            <div class="modal-total-number-goods__given-amount">
                <div class="left">
                    <p>Данное количество товаров зависит от вашего процента выкупа и суммы выкупа.</p>
                    <p><span>Процент выкупа</span> — отношение стоимости выкупленного товара к стоимости заказанного товара в процентах, рассчитанное за последние полгода сотрудничества с нами.</p>
                    <p><span>Сумма выкупа</span> — сумма, на которую вы совершили покупки в нашем магазине с момента первого заказа.</p>
                </div>
                <div class="right">
                    <div>
                        <span class="size">29 553 руб.</span>
                        <span class="bottom">Ваша сумма выкупа</span>
                    </div>
                    <div>
                        <span class="size">22,25%</span>
                        <span>Ваш процент выкупа</span>
                    </div>
                </div>
            </div>
            <div class="modal-total-number-goods__restriction">
                <span class="color-red">Ограничение на количество товаров в доставке действует только при наличном расчете.</span>
                <span>При безналичном расчете нет ограничения на количество товаров, которое можно оформить на доставку.</span>
            </div>
            <div class="modal-total-number-goods__calculation-table">
                <div class="internal-wrapper">
                    <ul>
                        <li>Суммы выкупа<br> (руб.)</li>
                        <li>Бронза<br> от 0</li>
                        <li>Серебро<br> от 3 000</li>
                        <li>Золото<br> от 20 000</li>
                        <li>VIP<br> от 100 000</li>
                    </ul>
                    <div class="text">
                        <span>% выкупа</span>
                        <span>Количество товаров, которое можно оформить на доставку без предварительной оплаты, шт.</span>
                    </div>
                    <div class="modal-total-number-goods__table">
                        <div class="element-table">
                            <span>50—100</span>
                            <span>40—50</span>
                            <span>30—40</span>
                            <span>17—30</span>
                            <span>10—17</span>
                            <span>0—10</span>
                        </div>
                        <div class="element-table">
                            <span>до 10</span>
                            <span>до 10</span>
                            <span>до 10</span>
                            <span>до 10</span>
                            <span>до 10</span>
                            <span>до 0</span>
                        </div>
                        <div class="element-table">
                            <span>до 30</span>
                            <span>до 25</span>
                            <span>до 20</span>
                            <span>до 10</span>
                            <span>до 10</span>
                            <span>до 0</span>
                        </div>
                        <div class="element-table">
                            <span>до 150</span>
                            <span>до 100</span>
                            <span>до 50</span>
                            <span>до 30</span>
                            <span>до 10</span>
                            <span>до 0</span>
                        </div>
                        <div class="element-table">
                            <span>до 500</span>
                            <span>до 250</span>
                            <span>до 150</span>
                            <span>до 50</span>
                            <span>до 10</span>
                            <span>до 0</span>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>