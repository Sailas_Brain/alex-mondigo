<?define('SITE_TEMPLATE_PATH', '/local/templates/mondigo')?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- vendor -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick-theme.css" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main.css">
    <title>Document</title>
</head>
<body>
<div class="header">
    <div class="header__inner">
        <div class="header__wrapper-for-col internal-blocks">
            <div class="internal-blocks-col-1">
                <a href="/link/index.php" class="internal-blocks__logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                </a>
            </div>
            <div class="internal-blocks-col-2">
                <div class="internal-blocks__burger-menu"><button class="btn button-burger-menu"></button></div>
                <div class="internal-blocks__background-burger-menu">
                    <div class="internal-blocks__block-manu">
                        <div class="internal-blocks__novelties">
                            <div class="">
                                <span>Новинки</span>
                            </div> 
                            <div class="internal-blocks__block-menu novelties">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="elements">
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-1.jpg" alt="">
                                        <span>Новинки женские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-2.jpg" alt="">
                                        <span>Новинки мужские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-3.jpg" alt="">
                                        <span>Аксессуары</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="internal-blocks__female">
                            <div>
                                <span>Женская</span>
                            </div>
                            <div class="internal-blocks__block-menu female">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="internal-blocks__male">
                            <div>
                                <span>Мужская</span>
                            </div>
                            <div class="internal-blocks__block-menu male">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <a href="#" class="iinternal-blocks__InstaShop">InstaShop</a>
                    </div>
                </div>
            </div>
            <div class="internal-blocks-col-3">
                <div class="internal-blocks__messege">
                    <div class="internal-blocks__block-images messege"></div>
                    <div>

                    </div>
                </div>
                <div class="internal-blocks__phone">
                    <div class="internal-blocks__block-images phone"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__lock">
                    <div class="internal-blocks__block-images lock"></div>
                    <div class="internal-blocks__internal-block-lock">
                        <div class="links">
                            <a class="uppercase" href="#">Товары</a>
                            <a class="active" href="#">Отложенные товары</a>
                            <a class="bottom-text" href="#">Лист Ожидания</a>
                            <a class="uppercase" href="#">Работа с заказом</a>
                            <a href="#">Мои заказы</a>
                            <a href="#">Мои доставки</a>
                            <a href="#">Мой баланс</a>
                            <a href="#">Личные предложения</a>
                            <a href="#">Гардероб</a>
                            <a class="bottom-text" href="#">Оформление возврата</a>
                            <a class="uppercase" href="#">Профиль</a>
                            <a href="#">Мои данные</a>
                            <a href="#">Моя скидка 5%</a>
                            <a href="#">Мои рассылки</a>
                            <a class="bottom-text" href="#">Новости</a>
                            <a class="uppercase" href="#">Обратная связь</a>
                            <a href="#">Мои обращения</a>
                            <a class="bottom-text" href="#">Проверка товара</a>
                            <a href="#">Выйти</a>
                        </div>                   
                    </div>
                </div>
                <div class="internal-blocks__heart">
                    <div class="internal-blocks__block-images heart"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__serch">
                    <div class="internal-blocks__block-images serch"></div>
                    
                </div>
                <div class="internal-blocks__basket">
                    <div class="nember-basket"><span>2</span></div>

                    <div class="internal-blocks__basket-dropdawn internal-basket">
                    
                        <span class="internal-basket-title">Корзина</span>
                        <div class="internal-basket-product-basket">
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                        </div>

                        <div class="internal-basket__price">
                            <span>Итого:</span>
                            <span>руб 6.500</span>
                        </div>



                        <div class="internal-basket__lenk-basket">
                            <a href="#">перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="internal-blocks__input-search">
                <span>Что ищем? |</span>
                <span class="close-in-search"></span>
                <input type="search" placeholder="">
            </div>
        </div>
    </div>
</div>
<div class="page page-catalog">
    <div class="page__inner page-catalog__inner">
        <div class="page-catalog__chain-navigation">
            <a href="#">Главная</a>
            <span>Новинки</span>
        </div>
        <div class="page-catalog__filter internal-filter">
            <div class="internal-filter__wrapper-for-filter">
                <div class="internal-filter__found">
                    <span>Найдено 1500 товаров</span>
                </div>
                <div class="internal-filter__block-filter">
                    <div class="block-filter-text">
                        <button class="btn btn-filter">фильтр</button>
                    </div>
                    <div class="internal-filter__wrapper-block-filter">
                        <div class="internal-filter__indernal-wrapper-filter">
                            <div class="col-1">
                                <div class="title-block"><span class="title">Категории</span></div>
                                <div class="category-text">
                                    <a href="">Базовые модели</a>
                                    <a href="">Блузки</a>
                                    <a href="" class="active">Болеро</a>
                                    <a href="">Брюки</a>
                                    <a href="">Верхняя одежда</a>
                                    <a href="">Вечерний коктейль</a>
                                    <a href="">Водолазки</a>
                                    <a href="">Джемпера</a>
                                    <a href="">Джинсы</a>
                                    <a href="">Жакеты</a>
                                    <a href="">Жилеты</a>
                                    <a href="">Капри</a>
                                    <a href="">Кардиганы</a>
                                    <a href="">Комбинезоны</a>
                                    <a href="">Сарафаны</a>
                                    <a href="">Свитшоты</a>
                                    <a href="">Топы</a>
                                    <a href="">Трикотаж</a>
                                    <a href="">Туники</a>
                                    <a href="">Футболки</a>
                                    <a href="">Шорты</a>
                                    <a href="">Юбки</a>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="col-floor">
                                    <div class="title-block"><span class="title">Пол</span></div>
                                    <div class="floor">
                                        <label>
                                            <input type="checkbox">
                                            <span>Женский</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Мужской</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-size">
                                    <div class="title-block"><span class="title">Размер</span></div>
                                    <div class="size">
                                        
                                        <ul>
                                            <? for ($i = 42; $i <= 50; $i++) { ?>
                                                <li>
                                                <label>
                                                    <input class="item-size-input" type="checkbox" name="item-size" value="<?echo($i)?>" form="card-item-form">
                                                    <span><?echo($i)?></span>
                                                </label>                                        
                                                </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-price-in-filter">
                                    <div  class="title-block"><span class="title">Цена</span></div>
                                    <div class="internal-filter__price-in-filter internal-price">
                                        <label>
                                            <input class="internal-price__val internal-price__min-value" type="text" value="700">
                                            <span>руб</span>
                                        </label>
                                        <label>
                                            <input class="internal-price__val internal-price__max-value" type="text" value="27000">
                                            <span>руб</span>
                                        </label>
                                        <div class="internal-price__wrapper-slider"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-3">
                                <div class="col-season">
                                    <div class="title-block"><span class="title">сезон</span></div>
                                    <div class="season">
                                        <label>
                                            <input type="checkbox">
                                            <span>Весна - Лето</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Осень - Зима</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-composition">
                                    <div  class="title-block"><span class="title">Состав</span></div>
                                    <div class="composition">
                                        <label>
                                            <input type="checkbox">
                                            <span>Хлопок</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Синтетика</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Шелк</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Шерсть</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Лён</span>
                                        </label>
                                    </div>   
                                </div>     
                            </div>
                            <div class="col-4">
                                <div class="title-block"><span class="title">цвет</span></div>
                                <div class="color">
                                    <label>
                                        <input type="checkbox">
                                        <span>Серебряный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Бежевый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Жёлтый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Розовый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Золотой</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-синий</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Черный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Синий</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Зеленый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Серый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Белый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Красный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Коричневый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Бургундия</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Розовый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Голубой</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-красный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-желтый</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="internal-filter__button-in-mobile">
                            <button class="apply">применить</button>
                            <button>очистить</button>
                        </div>
                    </div>
                </div>
                <div class="internal-filter__sort">
                    <div class="sort-text">
                        <button class="btn btn-sort">сортировка</button>
                    </div>
                    <div class="internal-filter__wrapper-sort">
                        <div class="internal-filter__dropdown-view-sort">
                            <div class="internal-block">
                                <a href="#">Популярности </a>
                                <a href="#" class="active">Рейтингу</a>
                                <a href="#">Цене</a>
                                <a href="#">Скидке</a>
                                <a href="#">Обновлению</a>
                            </div>
                            <div class="buttom-close"></div>
                        </div>
                    </div>
                </div>
                <div class="internal-filter__sort-by-number">
                    <a href="">40</a>
                    <a href="" class="active">100</a>
                    <a href="">200</a>
                </div>
                <div class="internal-filter__view-catalog">
                    <div class="view-four active"></div>
                    <div class="view-two"></div>                    
                </div>
            </div>
        </div>
        <div class="page-catalog__product-wrapper">
            <? for ($i=0; $i < 8; $i++) { ?>
                <div class="page-catalog__slider-new-collection">
                    <div class="page-main-item-slider">
                        <div class="page-catalog__big-preview">
                            <div class="page-catalog__slider-images image-slider-hover">
                                <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-catalog__wrapper-for-hover">
                                    <span class="page-catalog__icon-heart"></span>
                                    <div class="page-catalog__add-to-cart">
                                        <div class="in-cart">
                                            <span>В корзину</span>
                                        </div>
                                        <div class="choice-of-size">
                                            <span>Выбрать размер: </span>
                                            <ul>
                                                <li><a href="#">42</a></li>
                                                <li><a href="#">43</a></li>
                                                <li><a href="#">44</a></li>
                                                <li><a href="#">45</a></li>
                                                <li><a href="#">46</a></li>
                                                <li><a href="#">47</a></li>
                                                <li><a href="#">48</a></li>
                                                <li><a href="#">49</a></li>
                                                <li><a href="#">50</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        <div class="page-catalog__info-bottom-slider">
                            <span class="title">Купальник</span>
                            <span class="line-through">руб 3.400</span>
                            <span class="color-red">РУБ 3.000 по купону</span>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? for ($i=0; $i < 8; $i++) { ?>
                <div class="page-catalog__slider-new-collection">
                    <div class="page-main-item-slider">
                        <div class="page-catalog__big-preview">
                            <div class="page-catalog__slider-images image-slider-hover">
                                <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-catalog__wrapper-for-hover">
                            <span class="page-catalog__icon-heart"></span>
                            <div class="page-catalog__add-to-cart">
                                <div class="in-cart">
                                    <span>В корзину</span>
                                </div>
                                <div class="choice-of-size">
                                    <span>Выбрать размер: </span>
                                    <ul>
                                        <li><a href="#">42</a></li>
                                        <li><a href="#">43</a></li>
                                        <li><a href="#">44</a></li>
                                        <li><a href="#">45</a></li>
                                        <li><a href="#">46</a></li>
                                        <li><a href="#">47</a></li>
                                        <li><a href="#">48</a></li>
                                        <li><a href="#">49</a></li>
                                        <li><a href="#">50</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="page-catalog__info-bottom-slider">
                            <span class="title">Купальник</span>
                            <span class="line-through">руб 3.400</span>
                            <span class="color-red">РУБ 3.000 по купону</span>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="page-catalog__button-chain-navigation">
            <ul>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active"> <a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/catalog/button-chain-navigation.svg" alt=""></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="footer">
    <div class="footer__inner">
        <div class="footer__internal internal-blocks"> 
            <div class="col-1">
                <div class="internal-blocks__information">
                <span class="footer__title">контаткы</span>
                    <div class="internal-blocks__address">
                    <i class="fas fa-map-marker-alt"></i>
                        <span> г. Москва, ул. Нижняя Красносельская, д. 40/12, корп. 6, офис 8 </span>
                    </div>
                    <div class="internal-blocks__massege">
                    <i class="fas fa-envelope"></i>
                        <span>info@mondigo.ru</span>
                    </div>
                    <div class="internal-blocks__number-phone">
                        <i class="fas fa-phone"></i>
                        <span>8 800 775 88 38 (звонок бесплатный) 8 495 775 88 38</span>
                    </div>
                </div>
                <div class="internal-blocks__means-of-payment"> 
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/visa.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mastercard.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/mir-logo-h14px.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/qiwi.svg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/footer/maestro.svg" alt="">
                </div>
            </div>
            <div class="col-2"> 
                <span class="footer__title">О компании</span>
                <div class="internal-blocks__about-company">
                    <a href="#">О нас</a>
                    <a href="#">Контакты</a>
                    <a href="#">Сертификаты</a>
                    <a href="#">Преимущества</a>
                    <a href="#">Наши скидки</a>
                </div>
            </div>
            <div class="col-3">
                <span class="footer__title">Обслуживание клиентов</span>
                <div class="internal-blocks__client-service">
                    <a href="#">Как делать заказ</a>
                    <a href="#">Возврат товара</a>
                    <a href="#">Публичная оферта</a>
                    <a href="#">Доставка</a>
                    <a href="#">Способы оплаты</a>
                    <a href="#">Возврат денежных средств</a>
                    <a href="#">Вопросы и ответы</a>
                    <a href="#">Подарочные карты</a>
                    <a href="#">Сотрудничество оптом</a>
                </div>
            </div>
            <div class="col-4">
                <span class="footer__title">прочее</span>
                <div class="internal-blocks__etc-block">
                    <a href="#">Фабрика</a>
                    <a href="#">Обратная связь</a> 
                </div>
            </div>
        </div>
    </div>
        <div class="footer__bottom-block">
            <div class="footer__bottom-inner">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                <span>© 1999-2018 Mondigo.ru</span>
            </div>
        </div>
</div>

















<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
                                    <!-- vendor -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/jquery-ui.js"></script>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/slick.js"></script>

                                    <!-- jS -->
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
</body>
</html>