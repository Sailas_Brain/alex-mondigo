<?include('../local/templates/mondigo/header.php');?>
<div class="page page-wholesalers">
    <div class="page__inner page-wholesalers__inner">
        <div class="page-wholesalers__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оптовикам</span>
        </div>
        <div class="col">
            <span class="page-wholesalers__title">Оптовикам</span>
            <div class="page-wholesalers__header-mondigo">
                <span><span class="logo">MONDIGO</span> — производитель и дистрибьютор модной женской одежды. Мы рады сотрудничеству с новыми партнерами и предлагаем им</span>
                <ul>
                    <li>Три ценовых категории для оптовиков с разным объемом закупки. Скидка достигает 5% от стартовой оптовой цены;</li>
                    <li>Две договорные схемы оптовых покупок. Выбирайте удобную схему, подходящую вашему бизнесу;</li>
                    <li>Индивидуальные условия сотрудничества для постоянных партнеров;</li>
                    <li>Сезонные и событийные акции и скидки. Мы работаем над тем, чтобы ваши закупки стали ещё выгоднее;</li>
                    <li>Комфортные условия сотрудничества: удобный и функциональный личный кабинет, квалифицированная помощь
                    менеджеров оптового отдела, полная информационная поддержка продаж;</li>
                    <li>Эксклюзивная дизайнерская марка MONDIGO, которая становится всё более популярной на российском рынке.</li>
                </ul>
            </div>
            <div class="page-wholesalers__wrapper-for-accordion">
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>3 ценовых категории</span></div>
                    <div class="page-wholesalers__conditions">
                        <ul>
                            <li class="font">Ценовая категория</li>
                            <li class="font">Условия</li>
                            <li class="font">Договорная схема</li>
                        </ul>
                        <ul>
                            <li><span class="mobile-version">Ценовая<br> категория</span> <span>«ОПТ 1»</span></li>
                            <li><span class="mobile-version">Условия</span> <span class="big-text">Для покупателей с объемом закупки
                            от 30 000 до 60 000 руб</span></li>
                            <li><span class="mobile-version">Договорная<br> схема</span> <span>Оптовый покупатель</span></li>
                        </ul>
                        <ul>
                            <li><span class="mobile-version">Ценовая<br> категория</span> <span>«ОПТ 2»</span></li>
                            <li><span class="mobile-version">Условия</span> <span class="big-text">Скидка 2,7% от «ОПТ 1» Для покупателей
                            с объемом закупки от 60 000 до 90 000 руб.</span></li>
                            <li><span class="mobile-version">Договорная<br> схема</span> <span>Оптовый покупатель</span></li>
                        </ul>
                        <ul>
                            <li><span class="mobile-version">Ценовая<br> категория</span> <span>«ОПТ 3»</span></li>
                            <li><span class="mobile-version">Условия</span> <span class="big-text">Скидка 5,5% от «ОПТ 1»<br>
                            Для покупателей с объемом закупки от 90 000 руб.</span></li>
                            <li><span class="mobile-version">Договорная<br> схема</span> <span>Оптовый покупатель</span></li>
                        </ul>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>2 договорных схемы оптовых покупок</span></div>
                    <div class="page-wholesalers__contact">
                        <span class="title">1. Договорная схема «Оптовый покупатель»</span>
                        <span>Предоплатная договорная схема, которая подходит для розничных магазинов с выстроенной системой продаж.</span>
                        <div class="terms-of-cooperation">
                            <span class="uppercase">УСЛОВИЯ СОТРУДНИЧЕСТВА:</span>
                            <ul>
                                <li class="font"> Тип<br> клиента</li>
                                <li class="font">Предоплата</li>
                                <li class="font">Доставка</li>
                                <li class="font">Право первой закупки<br> по оптовой цене</li>
                                <li class="font">Минимальный<br> объем закупки</li>
                            </ul>
                            <ul>
                                <li><span class="mobile-version">Тип<br> клиента</span> <span>Оптовый клиент</span></li>
                                <li><span class="mobile-version">Предоплата</span> <span>100 %</span></li>
                                <li><span class="mobile-version">Доставка</span> <span class="big-text">Самовывоз. При закупке от 45 000 рублей доставка
                                до транспортной компании за счет Поставщика.</span></li>
                                <li class="margin"><span class="mobile-version">Право первой закупки по оптовой цене</span> <span>20.000 руб.</span></li>
                                <li class="margin"> <span class="mobile-version">Минимальный<br> объем закупки</span> <span>30.000 руб.</span></li>
                            </ul>
                        </div>
                        <div class="prising-terms">
                            <span class="uppercase">УСЛОВИЯ ЦЕНООБРАЗОВАНИЯ:</span>
                            <ul>
                                <li class="font">Ценовая категория</li>
                                <li class="font">Единоразовая закупка</li>
                                <li class="font">Размер скидки от цены ОПТ 1</li>
                            </ul>
                            <ul>
                                <li><span class="mobile-version">Ценовая категория</span> <span>«ОПТ 1»</span></li>
                                <li><span class="mobile-version">Единоразовая закупка</span> <span class="big-text">от 30 000 до 60 000 руб</span></li>
                                <li><span class="mobile-version">Размер скидки от цены ОПТ 1</span> <span>0</span></li>
                            </ul>
                            <ul>
                                <li><span class="mobile-version">Ценовая категория</span> <span>«ОПТ 2»</span></li>
                                <li><span class="mobile-version">Единоразовая закупка</span> <span class="big-text">от 60 000 до 90 000 руб.</span></li>
                                <li><span class="mobile-version">Размер скидки от цены ОПТ 1</span> <span>2,7%</span></li>
                            </ul>
                            <ul>
                                <li><span class="mobile-version">Ценовая категория</span> <span>«ОПТ 3»</span></li>
                                <li><span class="mobile-version">Единоразовая закупка</span> <span class="big-text">от 90 000 руб.</span></li>
                                <li><span class="mobile-version">Размер скидки от цены ОПТ 1</span> <span>5,5%</span></li>
                            </ul>
                            <div class="mobile-version-block">
                                <span>*Цены на товар указаны без НДС, в связи с применением Поставщиком упрощенной системы налогообложения.</span>
                                <a href="#">Скачать договор поставки</a>
                            </div>
                        </div>
                        <div class="contactual-scheme">
                            <span class="title">2. Договорная схема “Комиссионный покупатель”</span>
                            <span>Договорная схема с отгрузкой под реализацию,
                            хорошо подходит для тех клиентов, чья система продаж построена по комиссионному принципу. 
                            Вы платите только за проданный товар, при этом получая 45-ти дневную отсрочку платежа. </span>
                        </div>
                        <div class="importantly">
                            <span>Важно: Уважаемые покупатели! Отгрузка товара на комиссию осуществляется только крупным сетевым ритейлерам.</span>
                        </div>
                        <div class="cooperation">
                            <span class="uppercase">УСЛОВИЯ СОТРУДНИЧЕСТВА:</span>
                            <ul>
                                <li>Закупка товара под реализацию;</li>
                                <li>Ценовая категория – “ОПТ 1”;</li>
                                <li>Оплата осуществляется ежемесячно по отчету за реализованный товар. Минимальный объем реализации в месяц — 100 000 рублей;</li>
                                <li>Срок реализации партии Товара составляет 30 (тридцать) календарных дней с момента отгрузки Товара со склада Комитента. Получение Комиссионером новой партии
                                Товара осуществляется только после предоставления Отчета за поставленную партию Товара в предыдущий месяц и оплаты суммы реализованного Товара по данному Отчёту;</li>
                                <li>Оплата за реализованный Комиссионером товар производится в течение 3 банковских дней после подписания Сторонами ежемесячного Отчета.</li>
                            </ul>
                            <div class="mobile-version-block">
                                <a href="#">Скачать договор комиссии</a>
                                <a href="#" class="size">Скачать Приложения к договору комиссии</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Как заключить договор?</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <ul>
                            <li>Зарегистрируйтесь на нашем сайте в качестве оптового покупателя или направьте заявку на заключение договора по электронной почте info@mondigo.ru;</li>
                            <li>В течение одного дня с вами свяжется менеджер по оптовым клиентам для согласования Ваших потребностей и определения оптимальных условий сотрудничества;</li>
                            <li>После предоставления Вами карточки предприятия, Вам будет открыт доступ к разделу с оптовыми ценами на сайте;</li>
                            <li>Для заключения договора необходимо предоставить полный комплект уставных документов;</li>
                            <li>После подписания договора Вы сможете приступить к процедуре заказа.</li>
                        </ul>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Какие документы необходимы для заключения договора</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <span class="title">Для индивидуальных предпринимателей:</span>
                        <ul>
                            <li>Свидетельство о государственной регистрации физического лица в качестве предпринимателя (ЕГРИП);</li>
                            <li>Свидетельство о постановке на учет в налоговом органе физического лица (ИНН);</li>
                            <li>Ксерокопия паспорта гражданина РФ с листом прописки;</li>
                            <li>Карточка Клиента с указанием: юр. адреса организации, фактического адреса доставки груза, ФИО контактного лица и телефона.</li>
                        </ul>
                        <span class="title">Для юридических лиц:</span>
                        <ul>
                            <li>Свидетельства о государственной регистрации юридического лица (ОГРН);</li>
                            <li>Свидетельства о постановке на учет в налоговом органе юридического лица (ИНН);</li>
                            <li>Протокол о назначении руководителя;</li>
                            <li>Устав (первая, вторая и последняя страницы);</li>
                            <li>Карточка Клиента с указанием: юр. адреса организации, фактического адреса доставки груза, ФИО контактного лица и телефона;</li>
                            <li>Для представителя юр.лица: доверенность на заключение договора на официальном бланке с синей печатью и подписью руководителя.</li>
                        </ul>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Где можно познакомиться с нашим товаром?</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <span class="color">Вся продукция, представленная в продаже, доступна в нашем шоу-рум по адресу: г. Москва, м. "Автозаводская", ул. Ленинская , д. 19, офис .1092</span>
                        <span>Перед визитом в шоу-рум свяжитесь с менеджером для согласования даты и времени встречи.</span>
                        <span class="color-number">Телефон для связи: 8 (499) 707-69-49</span>
                        <span class="text">Перед выпуском новой коллекции мы также устраиваем предпоказы для постоянных партнеров.
                        Вы сможете забронировать ключевые модели коллекции до начала продаж.</span>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Как увидеть цены на сайте?</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <span>Оптовые цены видят только авторизованные оптовые покупатели. Если вы зарегистрированы на сайте, пройдите авторизацию.
                        Если нет — зарегистрируйтесь сейчас. Заполнение формы регистрации займет несколько минут и даст вам множество преимуществ:</span>
                        <ul>
                            <li>Доступ к оптовым ценам;</li>
                            <li>Доступ к многофункциональному личному кабинету;</li>
                        </ul>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Как сделать заказ?</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <ul>
                            <li>Зайдите на сайт под своим логином и паролем;</li>
                            <li>Поместите в корзину товары, которые планируете заказать и оформите заказ;</li>
                            <li>Либо сделайте заявку по телефону: <span class="color">8 (499) 707-69-49</span>;</li>
                            <li>На основании вашей заявки, мы выставим вам счет, который должен быть оплачен в течение 3-х банковских дней (по их истечению заявка на товар аннулируется);</li>
                            <li>После поступления денег на наш расчетный счет, мы доставим товар до транспортной компании, указанной вами или рекомендованной нами. Доставку до своего региона
                            оплачивает Заказчик. Оригиналы документов (договор, декларацию соответствия, товарно-транспортную накладную ТОРГ-12 и счет) вы получите вместе с товаром.</li>
                        </ul>
                        <span>Актуальный статус заказа вы всегда сможете увидеть в вашем личном кабинете. Весь товар, представленный на сайте, имеется в наличии.
                        Товар подлежит обмену только в случае обнаружения заводского брака. Перед отправкой покупателю весь товар проходит проверку.</span>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Как получить товар (доставка и оплата)?</span></div>
                    <div class="page-wholesalers__receive-goods">
                        <ul>
                            <li> <span class="color">Самовывоз:</span>
                                <span>Вы можете получить товар на складе по адресу: г. Москва, м. "Автозаводская", ул. Ленинская , д. 19, офис .1092</span>
                            </li>
                            <li><span class="color">Доставка по Москве:</span>
                                <span>При заказе на сумму свыше 45 000 рублей мы доставим товар бесплатно до вашей торговой точки. При заказе на сумму менее 45 000 руб. стоимость доставки – 600 руб. </span>
                            </li>
                            <li><span class="color">Доставка по Московской области (в пределах 30 км от МКАД):</span>
                                <span>Бесплатная доставка, при заказе на сумму свыше 55 000руб.<br>
                                При заказе на сумму менее 55 000 руб. стоимость доставки - 800 руб. + 20 руб/км. </span>
                            </li>
                            <li><span class="color">Доставка в регионы:</span>
                                <span>Бесплатная доставка до транспортной компании по Москве, при заказе на сумму свыше 45 000 руб.
                                При заказе на сумму менее 45 000 руб., стоимость доставки до транспортной компании по Москве – 600 руб.
                                по Московской области - 800 руб. + 20 руб/км. </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="page-wholesalers__wrapper-for-blocks-wholesalers">
                    <div class="page-wholesalers__information-dropdown"><span>Способы оплаты</span></div>
                    <div class="page-wholesalers__canclude-contract">
                        <ul>
                            <li>Наличный расчет;</li>
                            <li>Безналичный расчет;</li>
                            <li>Банковской картой (Visa, Visa Electron, Mastercard, Maestro)– в офисе компании, по адресу: г. Москва, м. "Автозаводская", ул. Ленинская , д. 19, офис .1092</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>