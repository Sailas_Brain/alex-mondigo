<?include('../local/templates/mondigo/header.php');?>
<div class="page page-our-discount">
    <div class="page__inner page-our-discount__inner">
        <div class="page-our-discount__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-our-discount__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div> 
            <div class="col-2">
                <span class="page-our-discount__title">Наши скидки</span>
                <div class="page-our-discount__customer-discount">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/our-discount/tag.svg" alt="">
                    <span class="title">Скидка постоянного покупателя</span>
                    <span>Постоянные покупатели нашего сайта пользуются специальной скидкой и экономят еще больше.</span>
                    <span>На странице «<a href="#">Моя скидка</a>» в вашем Личном кабинете вы можете посмотреть таблицу расчета и узнать, как увеличить свою скидку.</span>
                    <span>Если вы не зарегистрированы на сайте, <a href="#">сделайте это</a>! Регистрация займет всего пару минут.</span>
                    <span class="internal-title">Как рассчитать скидку?</span>
                    <span>Скидка рассчитывается на основе покупок, сделанных через Личный кабинет, и зависит от суммы выкупа и процента выкупа заказанных товаров.</span>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/our-discount/not-know.svg" alt="">
                    <button class="bth button-calculation-table">Таблица расчета скидки</button>
                </div>
                <div class="page-our-discount__promo-code">
                <img class="size" src="<?=SITE_TEMPLATE_PATH?>/images/our-discount/promo-code.svg" alt="">
                    <span class="title">Промокоды</span>
                    <span>Мы регулярно проводим акции и даем скидки на различные группы товаров. Чтобы получать такие скидки, нужно использовать промокоды.</span>
                    <span>В случае, когда объявляется скидка в размере определенного процента, этот процент вычитается из цены товара, указанной на сайте.</span>
                    <span>В случае, когда объявляется скидка на определенную сумму, эта сумма пропорционально распределяется по всем товарам в заказе.
                    При отказе от выкупа одного или нескольких товаров сумма скидки пропорционально уменьшается.</span>
                    <span>При использовании скидки по промокоду также действуют<br>
                    и другие скидки: существующие на сайте и постоянного покупателя.</span>
                    <span>Промокоды действуют в течение определенного времени. Пожалуйста, обращайте на это внимание.</span>
                    <span class="internal-title">Что такое промокод?</span>
                    <span>Промокод — это комбинация букв и цифр, либо кодовое слово, которые мы сообщаем в рассылках или на страницах сайта, посвященным акциям.</span>
                    <span class="internal-title">Как использовать промокод?</span>
                    <span>Для активации скидки нужно ввести промокод на странице «<a href="#">Моя корзина</a>» перед подтверждением заказа.</span>
                </div>
            </div>
        </div>        
    </div>
</div>
<div class="modal modal-total-number-goods">
    <div class="modal__inner modal-total-number-goods__inner">
        <div class="col">
            <span class="modal-total-number-goods__title">Как рассчитать общее кол-во товаров , которое можно оформить
            на доставку без предварительной оплаты</span>
            <div class="modal-total-number-goods__given-amount">
                <div class="left">
                    <p>Данное количество товаров зависит от вашего процента выкупа и суммы выкупа.</p>
                    <p><span>Процент выкупа</span> — отношение стоимости выкупленного товара к стоимости заказанного товара в процентах, рассчитанное за последние полгода сотрудничества с нами.</p>
                    <p><span>Сумма выкупа</span> — сумма, на которую вы совершили покупки в нашем магазине с момента первого заказа.</p>
                </div>
                <div class="right">
                    <span class="size">29 553 руб.</span>
                    <span class="bottom">Ваша сумма выкупа</span>
                    <span class="size">22,25%</span>
                    <span>Ваш процент выкупа</span>
                </div>
            </div>
            <div class="modal-total-number-goods__restriction">
                <span class="color-red">Ограничение на количество товаров в доставке действует только при наличном расчете.</span>
                <span>При безналичном расчете нет ограничения на количество товаров, которое можно оформить на доставку.</span>
            </div>
            <div class="modal-total-number-goods__calculation-table">
                <ul>
                    <li>Суммы выкупа<br> (руб.)</li>
                    <li>Бронза<br> от 0</li>
                    <li>Серебро<br> от 3 000</li>
                    <li>Золото<br> от 20 000</li>
                    <li>VIP<br> от 100 000</li>
                </ul>
                <div class="text">
                    <span>% выкупа</span>
                    <span>Количество товаров, которое можно оформить на доставку без предварительной оплаты, шт.</span>
                </div>
                <div class="modal-total-number-goods__table">
                    <div class="element-table">
                        <span>50—100</span>
                        <span>40—50</span>
                        <span>30—40</span>
                        <span>17—30</span>
                        <span>10—17</span>
                        <span>0—10</span>
                    </div>
                    <div class="element-table">
                        <span>до 10</span>
                        <span>до 10</span>
                        <span>до 10</span>
                        <span>до 10</span>
                        <span>до 10</span>
                        <span>до 0</span>
                    </div>
                    <div class="element-table">
                        <span>до 30</span>
                        <span>до 25</span>
                        <span>до 20</span>
                        <span>до 10</span>
                        <span>до 10</span>
                        <span>до 0</span>
                    </div>
                    <div class="element-table">
                        <span>до 150</span>
                        <span>до 100</span>
                        <span>до 50</span>
                        <span>до 30</span>
                        <span>до 10</span>
                        <span>до 0</span>
                    </div>
                    <div class="element-table">
                        <span>до 500</span>
                        <span>до 250</span>
                        <span>до 150</span>
                        <span>до 50</span>
                        <span>до 10</span>
                        <span>до 0</span>
                    </div>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>