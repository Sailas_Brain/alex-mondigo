<?include('../local/templates/mondigo/header.php');?>
<div class="page page-checking-goods">
    <div class="page__inner page-checking-goods__inner">
        <div class="page-checking-goods__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="page-checking-goods__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <span class="page-checking-goods__title">Проверка товара</span>
                <div class="checking-goods tabs">
                    <div class="checking-goods__header-list">
                        <div class="checking-method__method-name tabs-btn active"><span>Возврат при получении</span> </div>
                        <div class="checking-method__method-name tabs-btn"><span>Возврат после оплаты по браку</span> </div>
                    </div>    
                    <div>
                        <div class="page-checking-goods__return-on-receipt tabs-block">
                            <div class="block-for-accordion"><button>Возврат при получении</button></div>
                            <div class="return-on-receipt ">
                                <div class="page-checking-goods__block-create-request">
                                    <span>Чтобы возврат товара не учитывался в проценте выкупа, подайте заявку на его проверку.</span>
                                    <label>
                                        <input type="search" placeholder="Найти">
                                        <button></button>
                                    </label>
                                </div>
                                <div class="page-checking-goods__button-create">
                                    <button class="button-create-request">создать заявку</button>
                                </div>
                                <div class="page-checking-goods__term-of-consideration">
                                    <span>Срок рассмотрения заявки до 3 рабочих дней</span>
                                </div>
                                <div class="page-checking-goods__text-information">
                                    <span>При наличии основательных причин для отказа от покупки товара возврат не будет учитываться в Вашем проценте выкупа.</span>
                                    <span>Для этого Вам необходимо создать заявку на проверку товара: выбрать товар, добавить сделанные Вами фотографии товара и написать комментарии.</span>
                                    <span>Подать заявку можно сразу же после отказа от товара.</span>
                                    <span>Обратите внимание: после перехода позиции в статус «Возврат» останется 24 часа на оформление проверки по ней.</span>
                                    <span>Заявка оформляется на брак, истекший срок годности, ненадлежащее качество товара, несоответствие описанию, неправильное или неполное вложение в заказ.</span>
                                    <span>В случае подтверждения заявленного несоответствия или дефекта, позиция не будет учитываться в расчёте процента Вашего выкупа.</span>
                                    <div class="bottom-block-information">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                                        <span class="color">Некорректно заполненная заявка будет отклонена.</span>
                                    </div>
                                </div>
                                <div class="page-checking-goods__requirements">
                                    <div class=""><span class="title">Требования к содержанию фотографий</span></div>
                                    <div>
                                        <span>Размер каждого файла не должен превышать 2 МБ</span>
                                        <span>Поддерживаемые форматы: JPG, JPEG, PNG</span>
                                        <span>Обзорная фотография товара целиком</span>
                                        <span>Крупный план маркировки товара (навесная бирка, этикетка или ярлык должен содержать общую информацию, указанную производителем)</span>
                                        <span>Фото товара в индивидуальной упаковке с нашей маркировкой</span>
                                        <span>Для товаров из раздела «Красота» — дату изготовления, срок хранения или номер партии товара</span>
                                        <span>Предполагаемый дефект товара</span>
                                        <span>Предмет должен быть в фокусе, не размытым, его маркировка — четкой, хорошо читаемой</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-checking-goods__return-after-payment tabs-block">
                            <div class="block-for-accordion"><button>Возврат после оплаты по браку</button></div>
                            <div class="return-after-payment ">
                                <div class="page-checking-goods__block-create-request">
                                    <span>Чтобы вернуть товар по браку, оформите заявку на его проверку.</span>
                                    <label>
                                        <input type="search" placeholder="Найти">
                                        <button></button>
                                    </label>
                                </div>
                                <div class="page-checking-goods__button-create">
                                    <button class="button-create-request">создать заявку</button>
                                </div>
                                <div class="page-checking-goods__term-of-consideration">
                                    <span>Срок рассмотрения заявки до 5 рабочих дней</span>
                                </div>
                                <div class="page-checking-goods__text-information">
                                    <span><span class="font">Вернуть товар</span> ненадлежащего качества можно быстрее, оформив онлайн-заявку на его проверку:</span>
                                    <span class="text">Приложите фотографии товара (ниже приведены требования к фото)</span>
                                    <span class="text">опишите в комментарии выявленный дефект</span>
                                    <span><span class="font">При одобрении заявки</span> товар можно вернуть в пункте самовывоза, а также через курьера</span>
                                    <span><span class="font">Денежные средства</span> будут зачислены на Ваш баланс сразу после того, как статус товара изменится на «Возврат»</span>
                                    <span>Если брак не подтвердится, Вы получите акт осмотра</span>
                                    <div class="bottom-block-information">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                                        <span class="color">Некорректно заполненная заявка будет отклонена.</span>
                                    </div>
                                </div>
                                <div class="page-checking-goods__requirements">
                                    <div class=""><span class="title">Требования к содержанию фотографий</span></div>
                                    <div>
                                        <span>Размер каждого файла не должен превышать 2 МБ</span>
                                        <span>Поддерживаемые форматы: JPG, JPEG, PNG</span>
                                        <span>Обзорная фотография товара целиком</span>
                                        <span>Крупный план маркировки товара (навесная бирка, этикетка или ярлык должен содержать общую информацию, указанную производителем)</span>
                                        <span>Фото товара в индивидуальной упаковке с нашей маркировкой</span>
                                        <span>Для товаров из раздела «Красота» — дату изготовления, срок хранения или номер партии товара</span>
                                        <span>Предполагаемый дефект товара</span>
                                        <span>Предмет должен быть в фокусе, не размытым, его маркировка — четкой, хорошо читаемой</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-application-on-hceck">
    <div class="modal__inner modal-application-on-hceck__inner">
        <div class="col">
            <span class="modal-application-on-hceck__title">Заявка на проверку товара</span>
            <div class="modal-application-on-hceck__returned-goods">
                <span>Возвращенные товары, доступные для подачи заявки на проверку</span>
                <label>
                    <input type="serch" placeholder="Поиск товара">
                    <button></button>
                </label>
            </div>
            <div class="modal-application-on-hceck__list-goods">
                <ul>
                    <li>Товар</li>
                    <li>Цена</li>
                    <li>Заказ</li>
                </ul>
                <div class="modal-application-on-hceck__wrapper-element-list">
                    <div class="modal-application-on-hceck__wrapper-for-element-list">
                        <div class="product-information">
                            <div class="left">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                            </div>
                            <div class="right">
                                <span class="title">Джинсы эластичные синие</span>
                                <span>Размер: 50</span>
                                <span>Цвет: синий</span>
                                <span>Артикул:  USVM-2791</span>
                            </div>
                        </div>
                        <div class="price">
                            <span>4.500</span>
                        </div>
                        <div class="orber">
                            <span>3808317984</span>
                        </div>
                        <div class="button">
                            <button class="button-choose">Выбрать</button>
                        </div>
                    </div>
                    <div class="modal-application-on-hceck__dropdown-fomr-wrapper">
                        <form>
                            <div class="modal-application-on-hceck__wrapper-upload-photo">
                                <div class="left">
                                    <label for="file-input-label">
                                        <input type="file" id="file-input-label">
                                    </label>
                                    <span>Загрузить фото</span>
                                </div>
                                <div class="right">
                                    <span>Вы можете загрузить 5 изображений. Размер каждого файла не должен превышать 2 МБ.</span>
                                    <span>Поддерживаемые форматы: JPG, JPEG, PNG</span>
                                    <span class="font">Что должно быть на фото:</span>
                                    <button type="button" class="button-familiarize">Ознакомиться</button>
                                </div>
                            </div>
                            <div class="modal-application-on-hceck__your-comment">
                                <textarea placeholder="Ваш комментарий"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-application-on-hceck__bottom-button">
                <div class="left">
                    <span>Смотреть всё</span>
                </div>
                <div class="right">
                    <button class="btn btn-cancellation-modal">отмена</button>
                    <button class="send">отправить</button>
                </div>
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
        <div class="internal-modal">
            <div class="internal-modal__inner">
                <div class="col-1">
                    <p class="internal-modal__title">Требования к содержанию фотографий</p>
                    <div>
                        <span>Размер каждого файла не должен превышать 2 МБ</span>
                        <span>Поддерживаемые форматы: JPG, JPEG, PNG</span>
                        <span>Обзорная фотография товара целиком</span>
                        <span>Крупный план вшивной бирки</span>
                        <span>При наличии пакета со штрих-кодом, фото штрих-кода</span>
                        <span>Предполагаемый дефект товара</span>
                        <span>Предмет должен быть в фокусе, не размытым, его маркировка — четкой, хорошо читаемой</span>
                    </div>
                    <button class="btn btn-modal-internal-close">Закрыть окно</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>