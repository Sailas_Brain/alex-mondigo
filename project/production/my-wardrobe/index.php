<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-wardrobe">
    <div class="page__inner page-my-wardrobe__inner">
        <div class="page-my-wardrobe__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-wardrobe__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <span class="page-my-wardrobe__title">Мой гардероб</span>
                <div class="page-my-wardrobe__top-text">
                    <span>Для того, чтобы оформить возврат с помощью курьера, наведите на нужный товар и нажмите на кнопку «Вернуть»,
                    затем «Оформление возврата».</span>
                    <span>Так же, вы можете самостоятельно вернуть не подошедший вам товар в любом фирменном пункте самовывоза Wildberries 
                    или совершить возврат Почтой России.</span>
                </div>
                <div class="page-my-wardrobe__top-block-serch">
                    <label>
                        <input type="serch" placeholder="Найти">
                        <button></button>
                    </label>
                    <button>продолжить оформление возврата</button>   
                </div>
                <div class="page-my-wardrobe__number-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                </div>
                <div class="wrapper-for-list-wardrobe">
                    <div class="page-my-wardrobe__top-list-heading">
                        <ul class="page-my-wardrobe__element-list">
                            <li class="ul-list js-btn-autocheck"><i class="indicator"></i><span class="mobile-version">Выделить все</span> <span class="desktop-version">Товар</span></li>
                            <li>Номер заказа</li>
                            <li>Цена</li>
                            <li>Действие</li>
                        </ul>
                        <ul class="page-my-wardrobe__item-list">
                            <li>
                            <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-1">
                                <label class="js-label-autocheck" for="appeal-id-1">
                                    <div class="item">
                                        <div class="item__mobile-version">
                                            <span>Товар</span>
                                        </div>
                                        <div class="item__checkbox-container column">
                                            <i></i>
                                        </div>
                                        <div class="item__item-info column">
                                            <div class="item__image-container">
                                                <img src="/local/templates/mondigo/images/my-orber/images-insude-list.jpg" alt="">
                                            </div>
                                            <div class="item__info-container">
                                                <a href="#" class="color">Джинсы эластичные синие</a>
                                                <p>Размер: <span>38</span></p>
                                                <p>Цвет: <span>белый</span></p>
                                                <span>Купон:  USVM-2791</span>
                                            </div>
                                        </div>
                                        <div class="item__number column"> <span class="mobile-version">Номер заказа</span> <span>058565638</span></div>
                                        <div class="item__price column"><span class="mobile-version">Цена</span> <span class="price">4.500</span></div>
                                        <div class="item__block-actiont">
                                            <span class="mobile-version">Действие</span>
                                            <div class="item__wrapper">
                                                <div class="item__text-and-button">
                                                    <span>Не подлежит возврату</span>
                                                    <span class="color">?</span>
                                                    <!-- <button>вернуть</button> -->
                                                </div>
                                                <div class="item__action-social">
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/facebook.svg" alt=""></a>
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/odnoklassniki.svg" alt=""></a>
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/vk.svg" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </li>
                        </ul>
                        <ul class="page-my-wardrobe__item-list">
                            <li>
                                <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-2">
                                <label class="js-label-autocheck" for="appeal-id-2">
                                    <div class="item">
                                        <div class="item__checkbox-container column">
                                            <i></i>
                                        </div>
                                        <div class="item__item-info column">
                                            <div class="item__image-container">
                                                <img src="/local/templates/mondigo/images/my-orber/images-insude-list.jpg" alt="">
                                            </div>
                                            <div class="item__info-container">
                                                <a href="#" class="color">Джинсы эластичные синие</a>
                                                <p>Размер: <span>38</span></p>
                                                <p>Цвет: <span>белый</span></p> 
                                                <span>Купон:  USVM-2791</span>
                                            </div>
                                        </div>
                                        <div class="item__number column"><span class="mobile-version">Номер заказа</span> <span>058565638</span></div>
                                        <div class="item__price column"><span class="mobile-version">Цена</span> <span class="initial-price">4.500</span></div>
                                        <div class="item__block-actiont">
                                            <span class="mobile-version">Действие</span>
                                            <div class="item__wrapper">
                                                <div class="item__text-and-button">
                                                    <!-- <span>Не подлежит возврату</span>
                                                    <span class="color">?</span> -->
                                                    <button>вернуть</button>
                                                </div>
                                                <div class="item__action-social">
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/facebook.svg" alt=""></a>
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/odnoklassniki.svg" alt=""></a>
                                                    <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/vk.svg" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="page-my-wardrobe__bottom-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                    <button>продолжить оформление возврата</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>