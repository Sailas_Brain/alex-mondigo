<?include('../local/templates/mondigo/header.php');?>
<div class="page page-making-refund-second">
    <div class="page__inner page-making-refund-second__inner">
        <div class="page-making-refund-second__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="page-making-refund-second__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <div class="col-2-1">
                    <span class="page-making-refund-second__title">Оформление возврата</span>
                    <div class="page-making-refund-second__wrapper-initial-col">
                        <span>Выберите адрес и удобное время приезда курьера. Курьер приедит  на выбранный адрес.</span>
                        <? for ($i=0; $i < 3; $i++) { ?>
                            <label for="refund-<?echo $i;?>">
                                <input type="radio" id="refund-<?echo $i;?>" name="making-refund">
                                <span class="initial"></span>
                                Москва, Расковой, д. 26, кв. 31
                                <button class="edit"></button>
                                <button class="delete"></button>
                            </label>
                        <? } ?>
                    </div>
                    <div class="page-making-refund-second__block-button">
                        <button>добавить адрес</button>
                        <span>Можно добавить не более 5 адресов</span>
                    </div>
                    <div class="page-making-refund-second__courier-departure">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                        <span>Стоимость выезда курьера за возвратом 200р</span>
                    </div>
                    <div class="page-making-refund-second__history-operation">
                        <label class="datepicker__label">
                            <input class="datepicker-from" type="text" id="datepicker-from" name="birthday" placeholder="14.02.2018">
                        </label>
                        <label class="datepicker__label">
                            <input class="datepicker-to" type="text" id="datepicker-to" name="birthday" placeholder="14.03.2018">
                        </label>
                    </div>
                </div>
                <div class="col-2-2">
                    <div class="page-making-refund-second__wrapper-initial-col">
                        <span>Выберите пункт самовывоза для получения наличных денежных </span>
                        <? for ($j=0; $j < 3; $j++) { ?>
                            <label for="making-refund-<?echo $j;?>">
                                <input type="radio" id="making-refund-<?echo $j;?>" name="making-refund-second">
                                <span class="initial"></span>
                                Москва, Садовая, д. 237
                                <button class="edit"></button>
                                <button class="delete"></button>
                            </label>
                        <? } ?>
                    </div>
                    <div class="page-making-refund-second__right-block-button">
                        <button>добавить пункт самовывоза</button>
                    </div>
                    <div class="page-making-refund-second__courier-departure">
                        <div>
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                        </div>
                        <span><span class="uppercase">Внимание</span> При возврате товара надлежащего качества - получение наличных денежных средств возможно на следующий день после передачи товара курьеру.</span>
                    </div>
                    <div class="page-making-refund-second__return-policy">
                        <label>
                            <input type="checkbox">
                            <span>С условиями возврата согласен</span>
                        </label>
                        <p>Необходимо ознакомиться с условиями возврата товаров</p>
                    </div>
                    <div class="page-making-refund-second__return-goods">
                        <button>вернуть товар</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>