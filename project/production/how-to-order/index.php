<?include('../local/templates/mondigo/header.php');?>
<div class="page page-how-to-order">
    <div class="page__inner page-how-to-order__inner">
        <div class="page-how-to-order__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="col">
            <span class="page-how-to-order__title">Как сделать заказ</span>
            <div class="page-how-to-order__order-method">
                <span class="number-title">1. Войти или зарегистрироваться</span>
                <span>Оформить заказ могут только зарегистрированные пользователи. Войдите в Личный кабинет
                под своим логином или зарегистрируйтесь, если Вы у нас впервые. Регистрация займет не более 2 минут.</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-1.jpg" alt="">
            </div>
            <div class="page-how-to-order__order-method">
                <span class="number-title">2. Добавить товар в «Корзину»</span>
                <span>В карточке товара, который Вам понравился, выберите подходящий размер и нажмите кнопку «Добавить в корзину».</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-2.jpg" alt="">
                <span class="text">Чтобы определить свой размер размер, используйте ссылку «Определите свой размер».</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-3.jpg" alt="">
            </div>
            <div class="page-how-to-order__order-method">
                <span class="number-title">3. Перейти в корзину</span>
                <span>Для продолжения оформления заказа Вам необходимо перейти в «Корзину». Это можно сделать 2 способами:</span>
                <span class="align">Нажать кнопку «Перейти в корзину» в карточке товара, после его добавления в «Корзину»</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-4.jpg" alt="">
                <span class="text-right">- Кликнув на кнопку «Моя корзина» в правом верхнем углу сайта. При наведении курсора на этот значок, у Вас</span>
                <span>откроется информация о том, какие товары находятся в «Корзине», а при клике на него, Вы перейдете в саму «Корзину».</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-5.jpg" alt="">
            </div>
            <div class="page-how-to-order__order-method">
                <span class="number-title">4. Проверить заказ</span>
                <span>В «Корзине», на 1 шаге оформления заказа, Вы можете: проверить Ваш заказ, при необходимости изменить количество товара, изменить размер выбранной позиции,
                отложить товар в раздел «Избранные», для последующего оформления в заказ или удалить товар, если передумали его заказывать.</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-6.jpg" alt="">
                <span class="text">Если у Вас есть промокод, который необходимо ввести вручную, то введите его в пустое поле и нажмите кнопку «Применить».</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-7.jpg" alt="">
            </div>      
            <div class="page-how-to-order__order-method">
                <span class="number-title">5. Выбрать способ доставки и способ оплаты</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-8.jpg" alt="">
            </div>  
            <div class="page-how-to-order__order-method">
                <span class="number-title">6. Проверьте свои данные и данные о доставке</span>
                <span>При необходимости отредактируйте их.</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-9.jpg" alt="">
            </div>
            <div class="page-how-to-order__order-method">
                <span class="number-title">7. Завершить оформление заказа</span>
                <span>Нажмите кнопку «Подтвердить», для завершения оформления.</span>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/how-to-order/background-how-to-order-10.jpg" alt="">
            </div>
            <div class="page-how-to-order__order-method">
                <span class="number-title">Поздравляем, Ваш заказ подтвержден!</span>
                <span>Заказ будет автоматически подтвержден на доставку. Вам поступит письмо на e-mail, и заказ отобразится в разделе Личного кабинета «<a href="#">Мои заказы</a>».</span>
                <span class="bottom-text">Если для оформления заказа потребуется дополнительная информация, мы свяжемся с Вами.</span>
                <span class="bottom-text">Отслеживать всю информацию о доставке Вашего заказа Вы сможете в разделе Личного кабинета «<a href="#">Мои доставки</a>».</span>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>