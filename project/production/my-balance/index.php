<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-balance">
    <div class="page__inner page-my-balance__inner">
        <div class="page-my-data__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-balance__wrapper-for-col">
            <div class="col-1">
                <div class="sidebar-menu">
                    <ul class="sidebar-menu__list">
                        <a class="sidebar-menu__title" href="/">Главная</span></a>
                        <li>
                            <a href="">Товары</span></a>
                            <ul>
                                <li><a href="/?p=postponed-items">Отложенные товары</a></li>
                                <li><a href="/?p=waiting-list">Лист ожидания</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Работа с заказом</span></a>
                            <ul>
                                <li><a href="/?p=my-orders">Мои заказы</a></li>
                                <li><a href="/?p=my-orders">Мои доставки</a></li>
                                <li><a href="">Мой баланс</a></li>
                                <li><a href="">Личные предложения</a></li>
                                <li><a href="/?p=my-wardrobe">Гардероб</a></li>
                                <li><a href="/?p=my-wardrobe">Оформление возврата</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/?p=my-account">Профиль</span></a>
                            <ul>
                                <li><a href="">Мои данные</a></li>
                                <li><a href="/?p=discount">Моя скидка <span>5%</span></a></li>
                                <li><a href="">Мои рассылки</a></li>
                                <li><a href="">Новости</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">Обратная связь</span></a>
                            <ul>
                                <li><a href="/?p=my-appeals">Мои обращения</a></li>
                                <li><a href="">Проверка товара</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="">Выйти</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <span class="page-my-balance__title">Мой баланс</span>
                <span class="page-my-balance__text-under-title">Здесь Вы можете хранить денежные средства для дальнейшей оплаты товаров и услуг в нашем </span>
                <div class="page-my-balance__top-block">
                    <span>В истории операций Вы можете увидеть все финансовые операции, проведенные в<br> интернет-магазине.</span>
                    <div class="page-my-balance__button-for-open">
                        <div><button>история операций</button></div>
                        <div class="page-my-balance__internal internal-blocks">
                            <div class="internal-blocks__text">
                                <span>Для того, чтобы посмотреть историю операций, выберите дату и нажмите
                                на кнопку «Запросить». Формирование данной таблицы может занять некоторое время.</span>
                            </div>
                            <div class="internal-blocks__history-operation">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/images-operation.svg" alt="">
                                <span>История операций с</span>
                                <label class="datepicker__label">
                                    <input class="datepicker-from" type="text" id="datepicker-from" name="birthday" placeholder="14.02.2018">
                                </label>
                                    <span class="text">по</span>
                                <label class="datepicker__label">
                                    <input class="datepicker-to" type="text" id="datepicker-to" name="birthday" placeholder="14.03.2018">
                                </label>                                
                                <button>запросить</button>
                            </div>
                            <div class="internal-blocks__text-date-history-operation">
                                <span>История операций от 21.03.2018 с 21.12.2017 по 21.03.2018: </span>
                            </div>  
                            <div class="internal-blocks__header-list">
                                <ul class="top-list">
                                    <li>Дата</li>
                                    <li>Приход, руб.</li>
                                    <li>Расход, руб.</li>
                                    <li>Способ оплаты</li>
                                    <li>Операция</li>
                                </ul>
                            </div>
                            <? for ($i=0; $i < 3; $i++) {?>
                            <div class="internal-blocks__header-list addition">
                                <ul>
                                    <li><span class="mobile-version">Дата</span> <span>21.12.2017</span></li> 
                                    <li><span class="mobile-version">Приход, руб.</span> <span>726.</span></li>
                                    <li><span class="mobile-version">Расход, руб.</span> <span>726</span></li>
                                    <li><span class="mobile-version">Способ оплаты</span> <span>Курьеру</span></li>
                                    <li><span class="mobile-version">Операция</span> <span>Продажа</span></li>
                                </ul>
                            </div>
                            <? } ?>
                            <div class="internal-blocks__export">
                                <button>Экспорт в CSV</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-my-balance__blocks-actions">
                    <div class="page-my-balance__balance">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/purse.svg" alt="">
                        <span class="balance">0 РУБ.</span>
                        <span class="title">БАЛАНС</span>
                        <span class="text">Денежные средства, находящиеся на Вашем балансе, включая заблокированные суммы</span>
                    </div>
                    <div class="page-my-balance__locked">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/lock.svg" alt="">
                        <span class="balance">0 РУБ.</span>
                        <span class="title">ЗАБЛОКИРОВАНО</span>
                        <span class="text">Временно зарезервированные суммы по совершенным операциям до момента подтверждения или отказа от операции</span>
                    </div>
                    <div class="page-my-balance__available-balance">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/money.svg" alt="">
                        <span class="balance">0 РУБ.</span>
                        <span class="title">ДОСТУПНЫЙ ОСТАТОК</span>
                        <span class="text">Доступная сумма денежных средств Вашего баланса за исключением зарезервированной суммы</span>
                    </div>  
                </div>
                <div class="page-my-balance__block-information">
                    <div class="internal-block">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/purse-with-money.svg" alt="">
                        <button>Пополнить баланс</button>
                    </div>
                    <div class="internal-block">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/form.svg" alt="">
                        <button class="btn button-gift-vouchers">Детализация подарочных средств</button>
                    </div>
                    <div class="internal-block">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/mail.svg" alt="">
                        <button>Запросить детализацию</button>
                    </div>
                    <div class="internal-block">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-balance/hend-with-money.svg" alt="">
                        <button>Вернуть деньги</button>
                    </div>
                </div>
                <div class="page-my-balance__get-the-opportunity">
                    <div class="header-text">
                        <span>ПОЛУЧИТЕ ВОЗМОЖНОСТЬ ОПЛАЧИВАТЬ ЗАКАЗЫ В РАССРОЧКУ</span>
                        <button>Условия рассрочки</button>
                    </div>
                    <div class="text">
                        <span>Для получения индивидуального предложения заполните анкету.
                        В случае получения одобрения, Вам будет предоставлен лимит средств,
                        которые могут быть использованы для осуществления покупок.</span>
                        <span class="color-red">Договор по рассрочке заключается только на человека, которому принадлежит личный кабинет.</span>
                    </div>
                    <div class="button">
                        <button>заполнить анкету</button>
                    </div>
                </div>
                <div class="page-my-balance__gift-certificate">
                    <span class="title">Активация подарочного электронного сертификата</span>
                    <span class="text">После активации сертификата, подарочные средства будут зачислены на ваш бонусный счет.
                    Вы будете дополнительно оповещены об активации по СМС.</span>
                    <input type="text" placeholder="Введите номер сертификата">
                    <button>активировать</button>
                    <span class="your-certificate">Ваши сертификаты</span>
                </div>
                <div class="page-my-balance__replenish-balance">
                    <span class="title">Пополнить баланс</span>
                    <span class="text-under-title">В истории операций Вы можете увидеть все финансовые операции, проведенные в интернет-магазине.</span>
                    <div class="sum">
                        <span class="text-uppercase">Сумма</span>
                        <input type="text" placeholder="1500">
                        <span>*Максимальная сумма пополнения устанавливается платежной системой</span>
                    </div>
                    <div class="chosen-method">
                        <p>Выберите способ пополнения:</p>
                        <label>
                            <input type="checkbox">
                            <span>Онлайн оплата картой</span>
                        </label>
                    </div>
                </div>
                <div class="page-my-balance__refund-money">
                    <span class="title">Возврат денежных средств</span>
                    <span class="text-under-title">Вернуть денежные средства с Вашего баланса просто. Для этого Вам нужно оформить заявку.
                    Введите желаемую сумму и укажите банковские реквизиты, на которые необходимо перевести денежные средства.
                    Срок перечисления денежных средств осуществляется в срок до 10 календарных дней. Срок зачисления денежных
                    средств на расчетный счет Клиента зависит от внутреннего регламента банка-получателя.</span>
                    <div class="sum-in-refund">
                        <span class="text-uppercase">Сумма (руб)</span>
                        <input type="text" placeholder="1500">
                        <span>*Сумма возврата не может превышать доступный остаток. </span>
                        <span class="text-mobile-version">Выберите или добавьте реквизиты на которые необходимо сделать возврат денежных средств.</span>
                    </div>
                    <div class="chosen-method-in-refund">
                        <p>Выберите способ пополнения:</p>
                        <label>
                            <input type="checkbox">
                            <span>Онлайн оплата картой</span>
                        </label>
                        <p class="text">Выберите или добавьте реквизиты на которые необходимо сделать возврат денежных средств.</p>
                    </div>
                    <div class="exclamations">
                        <span>Уважаемые Клиенты! Обращаем Ваше внимание на то, что в связи с ограничениями 
                        стандартов безопасности платежных шлюзов PCI DSS, возникают
                        трудности с возвратами денежных средств на банковские счета, начинающиеся на
                        302*******. Ограничения связаны с передачей номера карты для совершения транзакции.</span>
                        <span>В случае введения некорректных реквизитов, денежные средства не будут перечислены в указанные сроки.</span>
                    </div>
                    <div class="button-in-refund">
                        <button>ДОБАВИТЬ РЕКВИЗИТЫ</button>
                        <button class="issue-refund">оформить возврат</button>
                        <span>Реквизиты не добавлены</span>
                    </div>
                </div>
                <div class="page-my-balance__in-this-sectionn-you-can">
                    <div class="col-2-1">
                    <span class="header-title">В этом разделе вы можете:</span>
                        <div class="page-my-balance__payment-for-goods">
                            <span class="title">Оплата товаров и услуг в нашем интернет-магазине
                            в полном объёме или частично доступными средствами Вашего баланса.</span>
                            <span>Вернуть денежные средства с Вашего баланса просто. Для этого Вам нужно оформить заявку.<br>
                            Введите желаемую сумму и укажите банковские реквизиты, на которые необходимо перевести денежные средства.
                            Срок перечисления денежных средств осуществляется в срок до 10 календарных дней.
                            Срок зачисления денежных средств на расчетный счет Клиента зависит от внутреннего регламента банка-получателя.</span>
                        </div>
                        <div class="page-my-balance__tracking-your">
                            <span class="title">Отслеживать Ваши операции.</span>
                            <span><span class="medium">История операций</span> – это перечень совершённых вами операций за интересующий Вас период не ранее чем за последние 183 дня</span>
                            <span><span class="medium">Детализация</span> – это перечень совершенных Вами операций (например, оплаченный заказ средствами
                            Вашего баланса, но еще не полученный Покупателем), суммы по которым временно зарезервированы.</span>
                        </div>
                    </div>
                    <div class="col-2-2">
                        <div class="page-my-balance__payment-for-goods return-partial">
                            <span class="title">Вернуть частично или в полном объёме доступные средства Вашего баланса на указанные Вами реквизиты</span>
                            <span>Вернуть денежные средства с Вашего баланса просто. Для этого Вам нужно всего лишь оформить заявку на возврат денежных средств 
                            (ДС). Для этого нажмите на ссылку «Вернуть ДС» под отображением Ваших доступных денежных
                            средств в окне «Доступный остаток», указав Ваши банковские реквизиты, на которые Вы хотите перевести желаемую 
                            сумму доступных денежных средств Вашего баланса.</span>
                        </div>
                        <div class="page-my-balance__payment-for-goods pay-attention">
                            <span class="title">Обратите внимание!</span>
                            <span class="color-red">В случае возврата товара надлежащего качества после оплаты, баланс будет увеличен на сумму возвращенного товара.
                            В случае возврата товара после оплаты по причине брака, баланс будет увеличен на сумму возвращенного товара после 
                            подтверждения несоответствия.</span>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
<div class="modal modal-gift-vouchers">
    <div class="modal__inner modal-gift-vouchers__inner">
        <div class="col">
            <span class="modal-gift-vouchers__title">Детализация подарочных средств</span>
            <div class="modal-gift-vouchers__internal-wrapper">
                <div class="gift-vouchers tabs">
                    <div class="gift-vouchers__header-list">
                        <div class="vouchers-method__method-name tabs-btn active"><span>АКТИВИРОВАННЫЕ</span></div> 
                        <div class="vouchers-method__method-name tabs-btn"><span>ОЖИДАЮТ АКТИВАЦИИ</span></div>
                        <label>
                            <input type="search" placeholder="Найти">
                        </label>
                    </div>
                    <div>
                    <div class=" tabs-block">
                        <div class="modal-gift-vouchers__block-list">
                            <span>Дата активации</span>
                            <span>Тип подарка</span>
                            <span>Заказ</span>
                            <span>Артикул</span>
                            <span>№ подарка/бонуса</span>
                            <span>Сумма</span>
                            <span>Истечение срока действия</span>
                        </div>
                    </div>
                    <div class=" tabs-block">
                        <div class="modal-gift-vouchers__block-list">
                            <span>Дата активации</span>
                            <span>Тип подарка</span>
                            <span>Заказ</span>
                            <span>Артикул</span>
                            <span>№ подарка/бонуса</span>
                            <span>Сумма</span>
                            <span>Истечение срока действия</span>
                        </div>                               
                    </div>
                </div>
                </div>
                
            </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>  
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>