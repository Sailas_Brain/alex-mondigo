<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-appeals">
    <div class="page__inner page-my-appeals__inner">
        <div class="page-my-data__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои обращения</span>
        </div>
        <div class="page-my-appeals__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div> 
            <div class="col-2">
                <span class="page-my-appeals__title">Мои обращения</span>
                <div class="my-appeals tabs">
                    <div class="my-appeals__header-list">
                        <div class="appeals-method__method-name tabs-btn active"><span>МОИ ОБРАЩЕНИЯ</span></div> 
                        <div class="appeals-method__method-name tabs-btn"><span>АРХИВ ОБРАЩЕНИЙ</span></div>
                    </div>
                    <div>
                        <div class="page-my-appeals__wrapper-for-accordion tabs-block">
                            <div class="page-my-appeals__mobile-version"><span>МОИ ОБРАЩЕНИЯ</span></div>
                            <div class="my-appeals-tab ">
                                <div class="page-my-appeals__images-block">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/my-appeals/question.svg" alt="">
                                    <span>Ответы на часто задаваемые вопросы</span>
                                </div>
                                <div class="page-my-appeals__block-button">
                                    <button>переместить в архив</button>
                                    <button class="send">создать обращение</button>
                                </div>
                                <div class="page-my-appeals__wrapper-intetrnal-block">
                                    <div class="page-my-appeals__top-navigation">
                                        <ul>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li class="active"> <a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                    <div class="page-my-appeals__wrapper-block-list">
                                        <div class="page-my-appeals__top-list">
                                            <ul class="page-my-appeals__header-list">
                                                <li class="select-all-inputs js-btn-autocheck"><i class="indicator"></i><span class="number-order">Номер</span><span class="mobile-version">Выделить все</span></li>
                                                <li>Раздел</li>
                                                <li>Доп. параметры</li>
                                                <li>Дата</li>
                                            </ul>
                                            <? for ($i=0; $i < 4; $i++) {?>
                                            <ul class="page-my-appeals__appeal-list">
                                                <li>
                                                <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>">
                                                <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>">
                                                    <div class="checkbox-container "><i></i><span class="mobile-version">Номер заказа</span> <span>24977875<span></div>
                                                    <div class="column"><span class="mobile-version">Раздел</span> <span>Сложность в оформлении<br> заказа</span></div>
                                                    <div class="column"><span class="mobile-version">Доп.<br>парпметры</span> <span>Промокод: 65675DF4</span></div>
                                                    <div class="column"><span class="mobile-version">Дата</span> <span>21.11.2017</span></div>
                                                </label>
                                                </li>
                                            </ul>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="page-my-appeals__buttom-navigation"> 
                                        <ul>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li class="active"> <a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-appeals__wrapper-for-accordion tabs-block">
                            <div class="page-my-appeals__mobile-version"><span>АРХИВ ОБРАЩЕНИЙ</span></div>
                            <div class="archive-of-requests-tab ">
                                    <div class="page-my-appeals__images-block">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-appeals/question.svg" alt="">
                                        <span>Ответы на часто задаваемые вопросы</span>
                                    </div>
                                    <div class="page-my-appeals__block-button">
                                        <button>переместить в архив</button>
                                        <button class="send">создать обращение</button>
                                    </div>
                                <div class="page-my-appeals__wrapper-intetrnal-block">
                                    <div class="page-my-appeals__top-navigation">
                                        <ul>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li class="active"> <a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                    <div class="page-my-appeals__wrapper-block-list">
                                        <div class="page-my-appeals__top-list-heading">
                                            <ul class="page-my-appeals__header-list">
                                                <li class="select-all-inputs js-btn-autocheck"><i class="indicator-special"></i><span class="number-order">Номер</span><span class="mobile-version">Выделить все</span></li>
                                                <li>Раздел</li>
                                                <li>Доп. параметры</li>
                                                <li>Дата</li>
                                            </ul>
                                            <? for ($i=0; $i < 4; $i++) {?>
                                            <ul class="page-my-appeals__appeal-list">
                                                <li>
                                                <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="my-appeal-id-<?echo($i)?>">
                                                <label class="js-label-autocheck" for="my-appeal-id-<?echo($i)?>">
                                                    <div class="checkbox-container "><i></i><span class="mobile-version">Номер заказа</span> <span>24977875<span></div>
                                                    <div class="column"><span class="mobile-version">Раздел</span> <span>Сложность в оформлении<br> заказа</span></div>
                                                    <div class="column"><span class="mobile-version">Доп.<br>парпметры</span> <span>Промокод: 65675DF4</span></div>
                                                    <div class="column"><span class="mobile-version">Дата</span> <span>21.11.2017</span></div>
                                                </label>
                                                </li>
                                            </ul>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="page-my-appeals__buttom-navigation">
                                        <ul>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li class="active"> <a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>