//= ../../header.php
<div class="page page-my-return">
    <div class="page__inner page-my-return__inner">
        <div class="page-my-return__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-return__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div> 
            <div class="col-2">
                <h1 class="page-my-return__title">Мои возвраты</h1>
                <div class="page-my-return__top-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                </div>
                <div class="page-my-return__archive-list internal-blocks">
                    <div class="internal-blocks__top-list">
                        <ul>
                            <li>Номер</li>
                            <li>Адрес</li>
                            <li>Время возврата</li>
                            <li>Способ возврата</li>
                            <li>Статус</li>
                            <li>Сумма</li>
                        </ul>
                    </div>
                    <? for ($i=0; $i < 3; $i++) { ?>
                        <div class="internal-blocks__wrapper-for-accordion">
                            <div class="internal-blocks__list-return">
                                <ul>
                                    <li><span class="mobile-version">Номер</span> 345572791</li>
                                    <li><span class="mobile-version">Адрес</span> Москва, Садовая 65,<br> кв.37</li>
                                    <li><span class="mobile-version">Время возврата</span> 09.05.2018.<br> 07:00 - 19:00</li>
                                    <li><span class="mobile-version">Способ возврата</span> Наличными</li>
                                    <li><span class="mobile-version">Статус</span> Подготовлен</li>
                                    <li><span class="mobile-version">Сумма</span> 4.500</li>
                                </ul>
                            
                            </div>
                            <div class="internal-blocks__internal-block-list">
                                <div class="list">
                                    <ul>
                                        <li>Описание товара</li>
                                        <li>Цена</li>
                                        <li>Причина возврата</li>
                                    </ul>
                                </div>
                                <div class="internal-blocks__description-product">
                                    <div class="description">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/my-returns/images-for-list.jpg" alt="">
                                        <div class="text">
                                            <span class="color-dark-blue">Джинсы эластичные синие</span>
                                            <span>Размер: 50</span>
                                            <span>Цвет: синий</span>
                                            <span class="text-bottom">Купон:  USVM-2791</span>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <span class="mobile-version">Цена</span>
                                        <span>4.500</span>
                                    </div>
                                    <div class="reaon-for-return">
                                        <span class="mobile-version">Причина<br> возврата</span>
                                        <span>Брак</span>
                                    </div>
                                </div>
                                <div class="internal-blocks__botton-button">
                                    <a href="#">отменить заявку</a>
                                    <span>ИТОГО: 4.500</span>
                                    <button class="close-element-list">Свернуть</button>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                </div>
                <div class="page-my-return__bottom-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
//= ../../footer.php