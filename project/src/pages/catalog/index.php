//= ../../header.php
<div class="page page-catalog">
    <div class="page__inner page-catalog__inner">
        <div class="page-catalog__chain-navigation">
            <a href="#">Главная</a>
            <span>Новинки</span>
        </div>
        <div class="page-catalog__filter internal-filter">
            <div class="internal-filter__wrapper-for-filter">
                <div class="internal-filter__found">
                    <span>Найдено 1500 товаров</span>
                </div>
                <div class="internal-filter__block-filter">
                    <div class="block-filter-text">
                        <button class="btn btn-filter">фильтр</button>
                    </div>
                    <div class="internal-filter__wrapper-block-filter">
                        <div class="internal-filter__indernal-wrapper-filter">
                            <div class="col-1">
                                <div class="title-block"><span class="title">Категории</span></div>
                                <div class="category-text">
                                    <a href="">Базовые модели</a>
                                    <a href="">Блузки</a>
                                    <a href="" class="active">Болеро</a>
                                    <a href="">Брюки</a>
                                    <a href="">Верхняя одежда</a>
                                    <a href="">Вечерний коктейль</a>
                                    <a href="">Водолазки</a>
                                    <a href="">Джемпера</a>
                                    <a href="">Джинсы</a>
                                    <a href="">Жакеты</a>
                                    <a href="">Жилеты</a>
                                    <a href="">Капри</a>
                                    <a href="">Кардиганы</a>
                                    <a href="">Комбинезоны</a>
                                    <a href="">Сарафаны</a>
                                    <a href="">Свитшоты</a>
                                    <a href="">Топы</a>
                                    <a href="">Трикотаж</a>
                                    <a href="">Туники</a>
                                    <a href="">Футболки</a>
                                    <a href="">Шорты</a>
                                    <a href="">Юбки</a>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="col-floor">
                                    <div class="title-block"><span class="title">Пол</span></div>
                                    <div class="floor">
                                        <label>
                                            <input type="checkbox">
                                            <span>Женский</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Мужской</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-size">
                                    <div class="title-block"><span class="title">Размер</span></div>
                                    <div class="size">
                                        
                                        <ul>
                                            <? for ($i = 42; $i <= 50; $i++) { ?>
                                                <li>
                                                <label>
                                                    <input class="item-size-input" type="checkbox" name="item-size" value="<?echo($i)?>" form="card-item-form">
                                                    <span><?echo($i)?></span>
                                                </label>                                        
                                                </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-price-in-filter">
                                    <div  class="title-block"><span class="title">Цена</span></div>
                                    <div class="internal-filter__price-in-filter internal-price">
                                        <label>
                                            <input class="internal-price__val internal-price__min-value" type="text" value="700">
                                            <span>руб</span>
                                        </label>
                                        <label>
                                            <input class="internal-price__val internal-price__max-value" type="text" value="27000">
                                            <span>руб</span>
                                        </label>
                                        <div class="internal-price__wrapper-slider"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-3">
                                <div class="col-season">
                                    <div class="title-block"><span class="title">сезон</span></div>
                                    <div class="season">
                                        <label>
                                            <input type="checkbox">
                                            <span>Весна - Лето</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Осень - Зима</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-composition">
                                    <div  class="title-block"><span class="title">Состав</span></div>
                                    <div class="composition">
                                        <label>
                                            <input type="checkbox">
                                            <span>Хлопок</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Синтетика</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Шелк</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Шерсть</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Лён</span>
                                        </label>
                                    </div>   
                                </div>     
                            </div>
                            <div class="col-4">
                                <div class="title-block"><span class="title">цвет</span></div>
                                <div class="color">
                                    <label>
                                        <input type="checkbox">
                                        <span>Серебряный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Бежевый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Жёлтый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Розовый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Золотой</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-синий</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Черный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Синий</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Зеленый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Серый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Белый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Красный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Коричневый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Бургундия</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Розовый</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Голубой</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-красный</span>
                                    </label>
                                    <label>
                                        <input type="checkbox">
                                        <span>Светло-желтый</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="internal-filter__button-in-mobile">
                            <button class="apply">применить</button>
                            <button>очистить</button>
                        </div>
                    </div>
                </div>
                <div class="internal-filter__sort">
                    <div class="sort-text">
                        <button class="btn btn-sort">сортировка</button>
                    </div>
                    <div class="internal-filter__wrapper-sort">
                        <div class="internal-filter__dropdown-view-sort">
                            <div class="internal-block">
                                <a href="#">Популярности </a>
                                <a href="#" class="active">Рейтингу</a>
                                <a href="#">Цене</a>
                                <a href="#">Скидке</a>
                                <a href="#">Обновлению</a>
                            </div>
                            <div class="buttom-close"></div>
                        </div>
                    </div>
                </div>
                <div class="internal-filter__sort-by-number">
                    <a href="">40</a>
                    <a href="" class="active">100</a>
                    <a href="">200</a>
                </div>
                <div class="internal-filter__view-catalog">
                    <div class="view-four active"></div>
                    <div class="view-two"></div>                    
                </div>
            </div>
        </div>
        <div class="page-catalog__product-wrapper">
            <? for ($i=0; $i < 8; $i++) { ?>
                <div class="page-catalog__slider-new-collection">
                    <div class="page-main-item-slider">
                        <div class="page-catalog__big-preview">
                            <div class="page-catalog__slider-images image-slider-hover">
                                <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-catalog__wrapper-for-hover">
                                    <span class="page-catalog__icon-heart"></span>
                                    <div class="page-catalog__add-to-cart">
                                        <div class="in-cart">
                                            <span>В корзину</span>
                                        </div>
                                        <div class="choice-of-size">
                                            <span>Выбрать размер: </span>
                                            <ul>
                                                <li><a href="#">42</a></li>
                                                <li><a href="#">43</a></li>
                                                <li><a href="#">44</a></li>
                                                <li><a href="#">45</a></li>
                                                <li><a href="#">46</a></li>
                                                <li><a href="#">47</a></li>
                                                <li><a href="#">48</a></li>
                                                <li><a href="#">49</a></li>
                                                <li><a href="#">50</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        <div class="page-catalog__info-bottom-slider">
                            <span class="title">Купальник</span>
                            <span class="line-through">руб 3.400</span>
                            <span class="color-red">РУБ 3.000 по купону</span>
                        </div>
                    </div>
                </div>
            <? } ?>
            <? for ($i=0; $i < 8; $i++) { ?>
                <div class="page-catalog__slider-new-collection">
                    <div class="page-main-item-slider">
                        <div class="page-catalog__big-preview">
                            <div class="page-catalog__slider-images image-slider-hover">
                                <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                            </div>
                            <div class="page-catalog__slider-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-catalog__wrapper-for-hover">
                            <span class="page-catalog__icon-heart"></span>
                            <div class="page-catalog__add-to-cart">
                                <div class="in-cart">
                                    <span>В корзину</span>
                                </div>
                                <div class="choice-of-size">
                                    <span>Выбрать размер: </span>
                                    <ul>
                                        <li><a href="#">42</a></li>
                                        <li><a href="#">43</a></li>
                                        <li><a href="#">44</a></li>
                                        <li><a href="#">45</a></li>
                                        <li><a href="#">46</a></li>
                                        <li><a href="#">47</a></li>
                                        <li><a href="#">48</a></li>
                                        <li><a href="#">49</a></li>
                                        <li><a href="#">50</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="page-catalog__info-bottom-slider">
                            <span class="title">Купальник</span>
                            <span class="line-through">руб 3.400</span>
                            <span class="color-red">РУБ 3.000 по купону</span>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="page-catalog__button-chain-navigation">
            <ul>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active"> <a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/catalog/button-chain-navigation.svg" alt=""></a></li>
            </ul>
        </div>
    </div>
</div>
//= ../../footer.php