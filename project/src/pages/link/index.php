<?include('../local/templates/mondigo/header.php');?>
<div class="page-links">
    <a href="../index.php">Главная</a>
    <a href="/catalog/index.php">каталог</a>
    <a href="/checking-goods/index.php">Проверка товара</a>
    <a href="/deferred-goods/index.php">Отложенные товары</a>
    <a href="/discount/index.php">скидка</a>
    <a href="/how-to-order/index.php">Как сделать заказ</a>
    <a href="/instashop/index.php">INSTASHOP</a>
    <a href="/making-refund-initial/index.php">Оформление возврата 1</a>
    <a href="/making-refund-second/index.php">Оформление возврата 2</a>
    <a href="/making-refund-third/index.php">Оформление возврата 3</a>
    <a href="/my-appeals/index.php">Мои обращения</a>
    <a href="/my-balance/index.php">Мой баланс</a>
    <a href="/my-data/index.php">Мои данные</a>
    <a href="/my-dispatch/index.php">Мои рассылки</a>
    <a href="/my-order/index.php">Мои заказы</a>
    <a href="/my-returns/index.php">Мои возвраты</a>
    <a href="/my-wardrobe/index.php">Мой гардероб</a>
    <a href="/order-registration/index.php">Оформление заказа</a>
    <a href="/our-discount/index.php">Наши скидки</a>
    <a href="/personal-account/index.php">Личный кабинет</a>
    <a href="/personal-offers/index.php">Личные предложения</a>
    <a href="/product-card/index.php">Карточка товара</a>
    <a href="/style-pages/index.php">Выбери свой стиль</a>
    <a href="/wholesalers/index.php">Оптовикам</a>
    <a href="/write-appeals/index.php">Мои обращения</a>
</div>
<?include('../local/templates/mondigo/footer.php');?>