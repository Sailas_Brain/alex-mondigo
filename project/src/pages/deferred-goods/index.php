<?include('../local/templates/mondigo/header.php');?>
<div class="page page-deferred-goods">
    <div class="page__inner page-deferred-goods__inner">
        <div class="page-deferred-goods__chain-navigation">  
            <a href="#">Главная</a>
            <span>Отложенные товары</span>
        </div>
        <div class="page-deferred-goods__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div>
            <div class="col-2">
                <span class="page-deferred-goods__title">Отложенные товары</span>
                <span class="page-deferred-goods__text-under-title">Вы можете отложить не более 1000 товаров. </span>
                <div class="page-deferred-goods__your-promo-code">
                    <div>
                        <input type="text" placeholder="Ваш промокод">
                        <a href="#">проверить</a>
                    </div>
                    <div>
                        <button>Удалить</button>
                        <button class="add-in-basket">добавить в корзину</button>
                    </div>
                </div>
                <div class="page-deferred-goods__top-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                    <label>
                        <input type="serch" placeholder="Найти">
                        <button></button>
                    </label>
                </div>
                <div class="wrapper-for-lest">
                    <ul class="wrapper-for-lest__block-list">
                        <li class="ul-list js-btn-autocheck"><i class="indicator"></i><span class="mobile-select-all">Выделить все</span> <span class="goods">Товар</span></li>
                        <li>Цена</li>
                        <li>Статус</li>
                        <li>Действие</li>
                    </ul>
                    <? for ($i=0; $i < 2; $i++) {?>
                        <ul class="wrapper-for-lest__element-list">
                            <li>
                                <input class="js-input-autocheck" type="checkbox" name="move-to-archive" value="24977875" id="appeal-id-<?echo($i)?>">
                                <label class="js-label-autocheck" for="appeal-id-<?echo($i)?>">
                                    <div class="item">
                                        <div class="item__container-for-initial-element">
                                            <div class="item__checkbox-container column">
                                                <i></i>
                                            </div>
                                            <div class="item__item-info column">
                                                <div class="item__image-container">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                                </div>
                                                <div class="item__info-container">
                                                    <a href="#"  class="color">Джинсы эластичные синие</a>
                                                    <p>Размер: <span>38</span></p>
                                                    <p>Цвет: <span>белый</span></p>
                                                    <span>Купон:  USVM-2791</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item__price column">
                                            <span class="price">Цена:</span>
                                            <span>4.500</span>
                                        </div>
                                        <div class="item__in-stock column">
                                            <span class="quantity">Кол-во:</span>
                                            <span>Есть на складе</span>
                                        </div>
                                        <div class="item__action column">
                                            <span class="text-action">Действие</span>
                                            <div class="images-basket">
                                                <img class="basket" src="<?=SITE_TEMPLATE_PATH?>/images/deferred-goods/basket.svg" alt="">
                                            </div>
                                            <div class="delete">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/images/deferred-goods/delete.svg" alt="">
                                            </div>
                                            <div class="images">
                                                <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/facebook.svg" alt=""></a>
                                                <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/odnoklassniki.svg" alt=""></a>
                                                <a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/images/my-wardrobe/vk.svg" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </li>
                        </ul>
                    <? } ?>
                </div>
                <div class="page-deferred-goods__bottom-navigation">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="active"> <a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><img src="/local/templates/mondigo/images/catalog/button-chain-navigation.svg" alt=""></a></li>
                    </ul>
                        <div>
                            <button>Удалить</button>
                            <button class="add-in-basket">добавить в корзину</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>