<?include('../local/templates/mondigo/header.php');?>
<div class="page page-making-refund-third">
    <div class="page__inner page-making-refund-third__inner">
        <div class="page-making-refund-third__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="page-making-refund-third__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div>
            <div class="col-2">
                <span class="page-making-refund-third__title">Оформление возврата</span>
                <div class="page-making-refund-third__application-accepted">
                    <span class="title">Ваша заявка принята и находится в стадии обработки</span>
                    <span>В ближайшее время Вы получите уведомление на электронную почту<br>
                    с информацией о статусе заявки</span>
                    <button>продолжить покупки</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>