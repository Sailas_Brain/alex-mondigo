//= ../../header.php
<div class="page page-personal-account">
    <div class="page__inner page-personal-account__inner">
        <div class="page-personal-account__chain-navigation">
            <a href="#">Главная</a>
            <span>Личный кабинет</span>
        </div>
        <div class="page-personal-account__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div>
            <div class="col-2">
                <div class="page-personal-account__top-wrapper internal-element">
                    <div class="col-2-1">
                        <div class="internal-element__internal-element-basket">
                            <a href="#">МОЯ КОРЗИНА</a>
                            <span>2 товара</span>
                            <span class="uppercase">4 014 руб.</span>
                            <button>подробнее</button>
                        </div>
                        <div class="internal-element__internal-element-basket background">
                            <a class="color-black-blue" href="#">МОИ ДОСТАВКИ</a>
                            <span class="color-grey">Ближайшая: не ожидается</span>
                            <button class="color-black-blue">подробнее</button>
                        </div>
                    </div>
                    <div class="col-2-2">
                        <div class="internal-element__my-balance">
                            <div class="internal-element__wrapper-for-internal-block">
                                <div class="left">
                                    <span class="uppercase">МОЙ БАЛАНС</span>
                                    <span>Доступный остаток</span>
                                    <span class="uppercase">0 руб.</span>
                                </div>
                                <div class="right">
                                    <a href="#">Подробнее</a>
                                </div>
                            </div>
                            <div class="internal-element__wrapper-for-internal-block">
                                <div class="left">
                                    <span>Баланс</span>
                                    <span class="price-text">0 руб.</span>
                                </div>
                                <div class="right">
                                    <a href="#">Пополнить счёт</a>
                                </div>
                            </div>
                            <div class="internal-element__wrapper-for-internal-block">
                                <div class="left">
                                    <span>Заблокировано</span>
                                    <span class="price-text">0 руб.</span>
                                </div>
                                <div class="right">
                                    <a href="#">Детализация</a>
                                </div>
                            </div>
                            <div class="internal-element__wrapper-for-internal-block">
                                <div class="left">
                                    <span>Акция «Рассрочка»</span>
                                    <button>Заполнить анкету</button>
                                </div>
                                <div class="right">
                                    <a href="#">Подробнее</a>
                                </div>
                            </div>
                            <div class="internal-element__history-operation">
                                <a href="#">История операций</a>
                            </div>
                            <div class="internal-element__history-operation border">
                                <a href="#">Активация подарочного сертификата</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-2-3">
                        <div class="internal-element__left-block">
                            <div class="internal-blocks">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/personal-account/min-box.svg" alt="">
                                <span class="uppercase">Мои заказы</span>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="internal-element__left-block">
                            <div class="internal-blocks">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/personal-account/min-time.svg" alt="">
                                <span class="uppercase">Лист ожидания</span>
                                <span>2 товара<br> Доступно к заказу: 0</span>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="internal-element__left-block">
                            <div class="internal-blocks">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/personal-account/min-shirt.svg" alt="">
                                <span class="uppercase">Отложенные товары</span>
                                <span>3 товара</span>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-personal-account__my-discount internal-blocks">
                    <div class="internal-blocks__left">
                        <div class="left">
                            <span>МОЯ СКИДКА</span>
                            <b>0<span class="percent">%</span></b>
                        </div>
                        <div class="right">
                            <span>Процент выкупа: <span class="color-red">22,25%</span></span>
                            <span>Сумма выкупа: <span class="color-red">29 553 руб.</span></span>
                            <span><b>Кол-во товаров:</b> <span class="color-red">до 30 шт.</span></span>
                            <a href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="internal-blocks__right">
                        <div class="left">
                            <img src="" alt="">
                            <form>
                                <input type="file" id="edit-profile-picture">
                                <label class="button-profile-picture" for="edit-profile-picture"></label>
                            </form>
                        </div>
                        <div class="right">
                                <span class="uppercase">ЩЕРБИНИНА КРИСТИНА</span>
                                <span>E-mail: kristinaa89@bk.ru</span>
                                <span>Дата рождения: 23.05.1989</span>
                                <span>Телефон: +7 (965) 242-29-99</span>
                                <a href="#">Посмотреть все данные</a>
                        </div>
                    </div>
                </div>
                <div class="page-personal-account__bonus">
                    <span class="uppercase">БОНУСЫ</span>
                    <span>Бонусами можно оплатить 100% стоимости заказа. Начисление бонусов происходит через 1 час после покупки товара.
                    Размер бонуса виден в Корзине и в карточке товара. Детализация начисленных бонусов доступна в разделе span  «<a href="#">Мой баланс</a>».</span>
                </div>
            </div>
        </div>
    </div>
</div>
//= ../../footer.php