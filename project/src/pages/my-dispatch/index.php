<?include('../local/templates/mondigo/header.php');?>
<div class="page page-my-dispatch">
    <div class="page__inner page-my-dispatch__inner">
        <div class="page-my-data__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои возвраты</span>
        </div>
        <div class="page-my-dispatch__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div> 
            <div class="col-2">
                <span class="page-my-dispatch__title">Мои рассылки</span>
                <p class="page-my-dispatch__text-under-title">Вы можете подписаться на следующие виды рассылок нашего магазина для получения новостей про акции и <span>промокоды:</span> </p>
                <div class="page-my-dispatch__internal-wrapper">
                    <div class="my-dispatch tabs">
                        <div class="my-dispatch__header-dispatch">
                            <div class="dispatch-method__method-name tabs-btn active"><span>E-mail рассылки</span></div> 
                            <div class="dispatch-method__method-name tabs-btn"><span>СМС-рассылки</span></div>
                        </div>
                        <div>
                            <div class="page-my-dispatch__mobile-version tabs-block">
                                <div class="page-my-dispatch__button-mobile-version"><button>E-MAIL РАССЫЛКИ</button></div>
                                <div class="block-email-dispatch">
                                    <div class="page-my-dispatch__checkbox-dispatch">
                                        <label>
                                            <input type="checkbox">
                                            <span>Акции, персональные промокоды и секретные распродажи</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" checked>
                                            <span>Лист ожидания</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Личные рекомендации товаров</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="page-my-dispatch__mobile-version tabs-block">
                                <div class="page-my-dispatch__button-mobile-version"><button>СМС-РАССЫЛКИ</button></div>
                                <div class="block-SMS-dispatch">
                                    <div class="page-my-dispatch__checkbox-dispatch">
                                        <label>
                                            <input type="checkbox">
                                            <span>Акции, персональные промокоды и секретные распродажи</span>
                                        </label>
                                        <label>
                                            <input type="checkbox">
                                            <span>Лист ожидания</span>
                                        </label>
                                        <label>
                                            <input type="checkbox" checked>
                                            <span>Личные рекомендации товаров</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-my-dispatch__button">
                            <button>сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>