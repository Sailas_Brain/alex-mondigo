<?include('../local/templates/mondigo/header.php');?>
<div class="page page-write-appeals">
    <div class="page__inner page-write-appeals__inner">
        <div class="page-write-appeals__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Мои обращения</span>
        </div>
        <div class="page-write-appeals__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div> 
            <div class="col-2">
                <span class="page-write-appeals__title">Мои обращения</span>
                <div class="write-appeals tabs">
                    <div class="write-appeals__header">
                        <div class="appeals-method__method-name tabs-btn active"><span>E-mail рассылки</span></div> 
                        <div class="appeals-method__method-name tabs-btn"><span>СМС-рассылки</span></div>
                    </div>
                    <div>

                        <div class="page-write-appeals__write-appeals tabs-block">
                            <div class="page-write-appeals__mobile-version"><span>E-mail рассылки</span></div>
                            <div class="write-appeals">
                                <div class="page-write-appeals__internal-blocks-second-col">
                                    <div class="col-2-1">
                                        <div class="page-write-appeals__question">
                                            <img src="/local/templates/mondigo/images/my-appeals/question.svg" alt="">
                                            <span>Ответы на часто задаваемые вопросы</span>
                                        </div>
                                        <div class="page-write-appeals__select-block">
                                            <div class="text"><span>Выберите тему*</span></div>
                                            <div class="internal-block-select">
                                                <span>Выберите тему</span>
                                                <span class="active">Благодарность</span>
                                                <span>Консультация</span>
                                                <span>Работа с заказом</span>
                                                <span>Пожелание/замечание</span>
                                            </div>
                                        </div>
                                        <div class="page-write-appeals__select-block">
                                            <div class="text"><span>Выберите раздел*</span></div>
                                            <div class="internal-block-select additional">
                                                <span>Выберите раздел*</span>
                                                <span class="active">Благодарность</span>
                                                <span>Консультация</span>
                                                <span>Работа с заказом</span>
                                                <span>Пожелание/замечание</span>
                                            </div>
                                        </div>
                                        <div class="page-write-appeals__commints">
                                            <textarea placeholder="Ваше обращение"></textarea>
                                        </div>
                                        <div class="page-write-appeals__required-field">
                                            <span>*Обязательные поля для заполнения</span>
                                        </div>
                                        <div class="page-write-appeals__button">
                                            <button>отправить</button>
                                        </div>
                                    </div>
                                    <div class="col-2-2">
                                        <div class="page-write-appeals__header-text">
                                            <span>В этом разделе вы можете оставить свое обращение, если у вас
                                            возникнут какие-либо проблемы или вопросы по работе интернет-магазина Mondigo.</span>
                                        </div>
                                        <div class="page-write-appeals__text-list">
                                            <p class="title">Обращения могут быть созданы по следующим темам:</p>
                                            <p><span>1.</span>Работа с заказом: перенос доставки, отказ от заказа и возврат товара.</p>
                                            <p><span>2.</span>Консультация о доставке заказа, сроках его поступления и уточнение дополнительных параметров</p>
                                            <p><span>3.</span>Пожелания и замечания по работе сотрудников и ассортименту интернет-магазина.</p>
                                            <p><span>4.</span>Благодарность за работу курьеров, операторов, а также пунктов самовывоза.</p>
                                        </div>
                                        <div class="page-write-appeals__bottom-text">
                                            <span class="title">Обратите внимание!</span>
                                            <span>Ваше обращение будет отклонено в случае несоответствия теме.
                                            Максимальный срок предоставления ответа на обращение составляет 24 часа.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="page-write-appeals__write-appeals tabs-block">
                            <div class="page-write-appeals__mobile-version"><span>СМС-рассылки</span></div>           
                            <div class="archive-of-requests">
                                <div class="page-write-appeals__internal-blocks-second-col">
                                    <div class="col-2-1">
                                        <div class="page-write-appeals__question">
                                            <img src="/local/templates/mondigo/images/my-appeals/question.svg" alt="">
                                            <span>Ответы на часто задаваемые вопросы</span>
                                        </div>
                                        <div class="page-write-appeals__select-block">
                                            <div class="text"><span>Выберите тему*</span></div>
                                            <div class="internal-block-select">
                                                <span>Выберите тему</span>
                                                <span class="active">Благодарность</span>
                                                <span>Консультация</span>
                                                <span>Работа с заказом</span>
                                                <span>Пожелание/замечание</span>
                                            </div>
                                        </div>
                                        <div class="page-write-appeals__select-block">
                                            <div class="text"><span>Выберите раздел*</span></div>
                                            <div class="internal-block-select additional">
                                                <span>Выберите раздел*</span>
                                                <span class="active">Благодарность</span>
                                                <span>Консультация</span>
                                                <span>Работа с заказом</span>
                                                <span>Пожелание/замечание</span>
                                            </div>
                                        </div>
                                        <div class="page-write-appeals__commints">
                                            <textarea placeholder="Ваше обращение"></textarea>
                                        </div>
                                        <div class="page-write-appeals__required-field">
                                            <span>*Обязательные поля для заполнения</span>
                                        </div>
                                        <div class="page-write-appeals__button">
                                            <button>отправить</button>
                                        </div>
                                    </div>
                                    <div class="col-2-2">
                                        <div class="page-write-appeals__header-text">
                                            <span>В этом разделе вы можете оставить свое обращение, если у вас
                                            возникнут какие-либо проблемы или вопросы по работе интернет-магазина Mondigo.</span>
                                        </div>
                                        <div class="page-write-appeals__text-list">
                                            <p class="title">Обращения могут быть созданы по следующим темам:</p>
                                            <p><span>1.</span>Работа с заказом: перенос доставки, отказ от заказа и возврат товара.</p>
                                            <p><span>2.</span>Консультация о доставке заказа, сроках его поступления и уточнение дополнительных параметров</p>
                                            <p><span>3.</span>Пожелания и замечания по работе сотрудников и ассортименту интернет-магазина.</p>
                                            <p><span>4.</span>Благодарность за работу курьеров, операторов, а также пунктов самовывоза.</p>
                                        </div>
                                        <div class="page-write-appeals__bottom-text">
                                            <span class="title">Обратите внимание!</span>
                                            <span>Ваше обращение будет отклонено в случае несоответствия теме.
                                            Максимальный срок предоставления ответа на обращение составляет 24 часа.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>