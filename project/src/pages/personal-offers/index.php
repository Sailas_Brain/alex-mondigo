//= ../../header.php
<div class="page page-personal-offers">
    <div class="page__inner page-personal-offers__inner">
        <div class="page-personal-offers__chain-navigation">
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Новинки</span>
        </div>
        <div class="page-personal-offers__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div>
            <div class="col-2">
                <h1 class="page-personal-offers__title">Личные предложения</h1>
                <div class="page-personal-offers__element-block">
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">500р</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 1 500 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">5%</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-green" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 5000 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">1000р</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-blue" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 1 500 руб.</span>
                        </div>
                    </div>
                    <div class="wrapper-element">
                        <div class="first-element">
                            <span>Mondigo поздравляет Вас с Днем рождения! Порадуйте себя новыми покупками!</span>
                        </div>
                        <div class="second-element">
                            <span>Ваш праздничный купон</span>
                            <span class="uppercase">5%</span>
                        </div>
                        <div class="fourth-element">
                            <span>Ваш код:</span>
                            <a class="color-DeepPink" href="#">4E9DD479</a>
                            <span>При общей сумме заказа от 5000 руб.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
//= ../../footer.php