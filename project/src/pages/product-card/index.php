<?include('../local/templates/mondigo/header.php');?>
<div class="page page-product-card"> 
    <div class="page__inner page-product-card__inner">
        <div class="page-product-card__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="col">
            <div class="page-product-card__wrapper-for-col">
                <div class="col-1-1">   
                    <div class="wrapper-for-slider">
                        <div class="page-product-card__horizontal-scroll">
                            <div class="page-product-card__top-images">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/slider-images-page-card-1.jpg" alt="">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/slider-images-page-card-2.jpg" alt="">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/slider-images-page-card-1.jpg" alt="">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/slider-images-page-card-2.jpg" alt="">
                            </div>
                        </div>
                        <div class="page-product-card__big-images">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/slider-images-page-card-1.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="page-product-card__wrapper-for-left-block">
                        <span class="page-product-card__title-left-block">Купальник с перекрещенными бретельками</span>
                        <div class="page-product-card__price">
                            <span class="discount">999 руб.</span>
                            <span class="price">1999 руб</span>
                            <span class="article">Арт. 095675830</span>
                        </div>
                        <div class="page-product-card__choose-color">
                            <div class="block-click"><span>Выбрать цвет</span></div>
                            <div class="internal-block">
                                <span class="green">Зеленый</span>
                                <span class="blue">Синий</span>
                                <span class="red">Красный</span>
                                <span class="orange active">Оранжевый</span>
                                <span class="white">Белый</span>
                                <span class="black">Черный</span>
                                <span class="green">Зеленый</span>
                                <span class="blue">Синий</span>
                                <span class="red">Красный</span>
                                <span class="orange">Оранжевый</span>
                                <span class="white">Белый</span>
                                <span class="black">Черный</span>
                            </div>
                        </div>
                        <div class="page-product-card__choose-color">
                            <div class="block-click"><span>Выбрать размер</span></div>
                            <div class="internal-block">
                                <span>XS</span>
                                <span>S</span>
                                <span class="active">M</span>
                                <span>L</span>
                                <span>XL</span>
                                <span>XXL</span>
                                <span>XS</span>
                                <span>S</span>
                                <span>M</span>
                                <span>L</span>
                                <span>XL</span>
                                <span>XXL</span>
                            </div>
                        </div>
                        <div class="page-product-card__wrapper-for-add-basket">
                            <button class="btn button-define-size">Определить размер</button>
                            <span class="color-green">Купить в рассрочку</span>
                            <div class="page-product-card__internal-element">
                                <div class="quantity">
                                    <button>-</button>
                                    <span>01</span>
                                    <button>+</button>
                                </div>
                                <div class="button-add">
                                    <button>добавить в корзину</button>
                                </div>
                                <div class="like-heart">
                                    <button></button>
                                </div>
                            </div>
                        </div>
                        <div class="product-card tabs">
                            <div class="product-card__header-list">
                                <div class="card-method__method-name tabs-btn active"><span>ОПИСАНИЕ</span></div> 
                                <div class="card-method__method-name tabs-btn"><span>ДОСТАВКА</span></div>
                                <div class="card-method__method-name tabs-btn"><span>ОПЛАТА</span></div>
                            </div>
                            <div>
                                <div class="description tabs-block">
                                    <div class="page-product-card__wrapper-description">
                                        <span>Гладкий трикотаж. V-образный вырез горловины. Узкие двойные бретельки, перекрещенные сзади.</span>
                                        <span>Подкладка.</span>
                                        <span class="text">Состав: 80% полиамид,20% эластан</span>
                                        <div class="page-product-card__block-property">
                                            <div class="left-block">
                                                <span>Бренд</span>
                                                <span>Артикул</span>
                                                <span>Длина рукава</span>
                                                <span>Покрой</span>
                                                <span>Рисунок</span>
                                                <span>Фактура</span>
                                                <span>Назначение</span>
                                                <span>Уход</span>
                                                <span class="text">Длина изделия по спинке</span>
                                            </div>
                                            <div class="right-block">
                                                <span>MONDIGO</span>
                                                <span>095675830</span>
                                                <span>Без рукавов</span>
                                                <span>Облегающий</span>
                                                <span>Без рисунка</span>
                                                <span>Гладкая</span>
                                                <span>Для купания</span>
                                                <span>Бережная стирка при t не более 40С</span>
                                                <span class="text">60</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="delivery tabs-block">
                                    <div class="page-product-card__wrqapper-for-delivery">
                                        <span><span class="font">Курьером по Москве в пределах МКАД – БЕСПЛАТНО</span> от 2000 руб. При заказе до 2000 рублей стоимость доставки - <span class="font">250 руб.</span></span>
                                        <div class="delivery-product">
                                            <span class="font">Доставка товара за пределы МКАД <span>составляет</span> 250 руб.</span>
                                            <span class="text">- Доставка курьером осуществляется в будни и субботу.</span>
                                            <span class="text">- Срок доставки 1-2 рабочих дня</span>
                                            <span class="text">- При полном отказе покупатель оплачивает доставку 250 руб.</span>
                                            <span class="text">- Примерка вещей при курьере 15 минут.</span>
                                        </div>
                                        <span class="bottom-title">В другие города России.</span>
                                        <span><span class="font">EMS-Почта России</span> – доставка <span class="font">БЕСПЛАТНО от 3000 руб.</span> При заказе до 3000 рублей стоимость доставки - <span class="font">450 руб.</span> Срок доставки - от 2 до 10 дней.</span>
                                        <span class="button-text"><span class="font">Самовывоз</span> из фирменного пункта по адресу г. Москва, улица Ленинская Слобода, 19. БЦ "Омега Плаза", 1 этаж, офис 1092</span>
                                    </div>
                                </div>
                                <div class="payment tabs-block">
                                    <div class="page-product-card__wrapper-for-payment">
                                        <span><span class="font">Курьером</span> - онлайн оплата на сайте, а так же наличными или картой при получении;</span>
                                        <span><span class="font">Почтой</span> - наличными при получении;</span>
                                        <span><span class="font">Самовывоз</span> - онлайн оплата на сайте, а так же наличными или картой при получении;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-product-card__block-icons">
                            <span>Поделиться:</span>
                            <a href="#"><i class="fab fa-whatsapp"></i></a>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-telegram-plane"></i></a>
                            <a href="#"><i class="far fa-envelope"></i></a>
                            <button class="copy" id="copy-link-url"></button>
                        </div>
                        <div class="page-product-card__wrapper-specify-question">
                            <span class="text">ЗАДАТЬ ВОПРОС</span>
                            <form>
                                <input type="text" placeholder="ФИО*">
                                <input type="text" placeholder="Ваш e-mail*">
                                <input type="text" placeholder="Город">
                                <div class="page-product-card__choose-color">
                                    <div class="block-click"><span class="retreat-left">Тема</span></div>
                                    <div class="internal-block">
                                        <span>Тема</span>
                                        <span class="active">Тема Тема</span>
                                        <span>Тема</span>
                                        <span>Тема</span>
                                    </div>
                                </div>
                                <textarea placeholder="Ваш вопрос"></textarea>
                                <button type="button">отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-product-card__wrapper-image-model">
                <span class="page-product-card__title">Образ на модели</span>
                <div class="page-product-card__product-wrapper">
                <? for ($i=0; $i < 3; $i++) { ?>
                    <div class="page-catalog__slider-new-collection">
                        <div class="page-main-item-slider">
                            <div class="page-catalog__big-preview">
                                <div class="page-catalog__slider-images image-slider-hover">
                                    <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                    <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">                     
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="page-catalog__wrapper-for-hover">
                                <span class="page-catalog__icon-heart"></span>
                                <div class="page-catalog__add-to-cart second-size">
                                    <div class="in-cart">
                                        <span>В корзину</span>
                                    </div>
                                    <div class="choice-of-size">
                                        <span>Выбрать размер: </span>
                                        <ul>
                                            <li><a href="#">42</a></li>
                                            <li><a href="#">43</a></li>
                                            <li><a href="#">44</a></li>
                                            <li><a href="#">45</a></li>
                                            <li><a href="#">46</a></li>
                                            <li><a href="#">47</a></li>
                                            <li><a href="#">48</a></li>
                                            <li><a href="#">49</a></li>
                                            <li><a href="#">50</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="page-catalog__info-bottom-slider">
                                <span class="title">Купальник</span>
                                <span class="line-through">руб 3.400</span>
                                <span class="color-red">РУБ 3.000 по купону</span>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
                <div class="page-product-card__buy-the-way">
                    <button class="btn button-buy-the-way">КУПИТЬ ОБРАЗ</button>
                </div>
            </div>
            <span class="page-product-card__title">Выбрано специально для вас</span>
            <div class="page-catalog__product-wrapper">
                <? for ($i=0; $i < 4; $i++) { ?>
                    <div class="page-catalog__slider-new-collection">
                        <div class="page-main-item-slider">
                            <div class="page-catalog__big-preview">
                                <div class="page-catalog__slider-images image-slider-hover">
                                    <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                    <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">     
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="page-catalog__wrapper-for-hover">
                                <span class="page-catalog__icon-heart"></span>
                                <div class="page-catalog__add-to-cart">
                                    <div class="in-cart">
                                        <span>В корзину</span>
                                    </div>
                                    <div class="choice-of-size">
                                        <span>Выбрать размер: </span>
                                        <ul>
                                            <li><a href="#">42</a></li>
                                            <li><a href="#">43</a></li>
                                            <li><a href="#">44</a></li>
                                            <li><a href="#">45</a></li>
                                            <li><a href="#">46</a></li>
                                            <li><a href="#">47</a></li>
                                            <li><a href="#">48</a></li>
                                            <li><a href="#">49</a></li>
                                            <li><a href="#">50</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="page-catalog__info-bottom-slider">
                                <span class="title">Купальник</span>
                                <span class="line-through">руб 3.400</span>
                                <span class="color-red">РУБ 3.000 по купону</span>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
            <span class="page-product-card__title">Последние просмотренные товары</span>
            <div class="page-catalog__product-wrapper">
                <? for ($i=0; $i < 4; $i++) { ?>
                    <div class="page-catalog__slider-new-collection">
                        <div class="page-main-item-slider">
                            <div class="page-catalog__big-preview">
                                <div class="page-catalog__slider-images image-slider-hover">
                                    <img  src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-1.jpg" alt="">
                                    <img class="active" src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-2.jpg" alt="">  
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-3.jpg" alt="">
                                </div>
                                <div class="page-catalog__slider-images">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/main/slide-images-4.jpg" alt="">
                                </div>
                            </div>
                            <div class="page-catalog__wrapper-for-hover">
                                <span class="page-catalog__icon-heart"></span>
                                <div class="page-catalog__add-to-cart">
                                    <div class="in-cart">
                                        <span>В корзину</span>
                                    </div>
                                    <div class="choice-of-size">
                                        <span>Выбрать размер: </span>
                                        <ul>
                                            <li><a href="#">42</a></li>
                                            <li><a href="#">43</a></li>
                                            <li><a href="#">44</a></li>
                                            <li><a href="#">45</a></li>
                                            <li><a href="#">46</a></li>
                                            <li><a href="#">47</a></li>
                                            <li><a href="#">48</a></li>
                                            <li><a href="#">49</a></li>
                                            <li><a href="#">50</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="page-catalog__info-bottom-slider">
                                <span class="title">Купальник</span>
                                <span class="line-through">руб 3.400</span>
                                <span class="color-red">РУБ 3.000 по купону</span>
                            </div>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
    </div>
    <div class="page-product-card__background-subscriptions">
        <div class="page-product-card__subscriptions-inner">
            <span class="page-product-card__title">Подпишись и получи скидку 10%</span>
            <input type="text" placeholder="Введите ваш e-mail">
            <span class="text">Введите правильный e-mail (например, imya@gmail.com)</span>
            <div class="block-icons">
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-vk"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-odnoklassniki"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-define-size">
    <div class="modal__inner modal-define-size__inner">
        <div class="col">
            <span class="modal-define-size__title">Определить размер</span>
                <div class="modal-define-size__wrapper-for-table">
                    <ul>
                        <li>Размер рубашки</li>
                        <li>S</li>
                        <li>M</li>
                        <li>L</li>
                        <li>XL</li>
                        <li>2XL</li>
                        <li>3XL</li>
                        <li>4XL</li>
                        <li>5XL</li>
                    </ul>
                    <div class="modal-define-size__wrapper-element-table">
                         <div class="element-table initial  top no-background"> 
                             <span  class="no-indentation">Обхват шеи</span>
                         </div>
                         <div style="display: none;"></div>
                         <div class="element-table top">
                            <span>37-38</span>
                         </div>
                         <div class="element-table top">
                            <span>39-40</span>
                         </div>
                         <div class="element-table top">
                            <span>41-42</span>
                         </div>
                         <div class="element-table top">
                            <span>43-44</span>
                         </div>
                         <div class="element-table top">
                            <span>45-46</span>
                         </div>
                         <div class="element-table top">
                            <span>47-48</span>
                         </div>
                         <div class="element-table top">
                            <span>49-50</span>
                         </div>
                         <div class="element-table top">
                            <span>51-52</span>
                         </div>
                    </div>
                    <div class="modal-define-size__wrapper-element-table">
                         <div class="element-table no-background"> 
                             <span class="one">Обхват груди</span>
                         </div>
                         <div class="element-table">
                            <span class="first">classic fit</span>
                            <span class="second">regular fit</span>
                            <span class="third">slim fit</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table super-element">
                            <span class="first">520</span>
                            <span class="second">510</span>
                            <span class="third">500</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                    </div>
                    <div class="modal-define-size__wrapper-element-table">
                         <div class="element-table no-background"> 
                             <span class="one">Обхват талии</span>
                         </div>
                         <div class="element-table">
                            <span class="first">classic fit</span>
                            <span class="second">regular fit</span>
                            <span class="third">slim fit</span>
                         </div>
                         <div class="element-table col-s">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-m">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-l">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                    </div>                 
                    <div class="modal-define-size__wrapper-element-table">
                         <div class="element-table no-background"> 
                             <span class="one">Длина по спинке</span>
                         </div>
                         <div class="element-table">
                            <span class="first">classic fit</span>
                            <span class="second">regular fit</span>
                            <span class="third">slim fit</span>
                         </div>
                         <div class="element-table col-s">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-m">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-l">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                    </div>
                    <div class="modal-define-size__wrapper-element-table">
                         <div class="element-table no-background"> 
                             <span class="one">Длина рукава</span>
                         </div>
                         <div class="element-table">
                            <span class="first">classic fit</span>
                            <span class="second">regular fit</span>
                            <span class="third">slim fit</span>
                         </div>
                         <div class="element-table col-s">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-m">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table col-l">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                         <div class="element-table">
                            <span class="first">52</span>
                            <span class="second">51</span>
                            <span class="third">50</span>
                         </div>
                    </div>
                </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>
<div class="modal modal-buy-way">
    <div class="modal__inner modal-buy-way__inner">
        <div class="col">
            <span class="modal-buy-way__title">Купить образ</span>
            <? for ($i=0; $i < 2; $i++) {?>
                <div class="modal-buy-way__wrapper-for-block">
                    <div class="images">
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/product-card/images-in-page-card.jpg" alt="">
                    </div>
                    <div class="modal-buy-way__element-block">
                        <div class="prise">
                            <span>Сандали</span>
                            <span class="font">руб 3.500</span>
                        </div>
                        <div class="page-product-card__choose-color">
                            <div class="block-click alige"><span>Выбрать цвет</span></div>
                            <div class="internal-block alige">
                                <span class="green">Зеленый</span>
                                <span class="blue">Синий</span>
                                <span class="red">Красный</span>
                                <span class="orange active">Оранжевый</span>
                                <span class="white">Белый</span>
                                <span class="black">Черный</span>
                                <span class="green">Зеленый</span>
                                <span class="blue">Синий</span>
                                <span class="red">Красный</span>
                                <span class="orange">Оранжевый</span>
                                <span class="white">Белый</span>
                                <span class="black">Черный</span>
                            </div>
                        </div>
                        <div class="page-product-card__choose-color">
                            <div class="block-click alige"><span>Выбрать размер</span></div>
                            <div class="internal-block alige">
                                <span>XS</span>
                                <span>S</span>
                                <span class="active">M</span>
                                <span>L</span>
                                <span>XL</span>
                                <span>XXL</span>
                                <span>XS</span>
                                <span>S</span>
                                <span>M</span>
                                <span>L</span>
                                <span>XL</span>
                                <span>XXL</span>
                            </div>
                        </div>
                        <button class="modal-buy-way__button-modal-define-size">Определить размер</button>
                        <div class="modal-buy-way__wrapper-add-basket">
                            <div class="quantity">
                                <button>-</button>
                                <span>01</span>
                                <button>+</button>
                            </div>
                            <div class="internal-images">
                                <div class="modal-like-heart">
                                    <button></button>
                                </div>
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/rubbish-bin.svg" alt="">
                            </div>
                            <div class="button">
                                <button>добавить в корзину</button>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
        <div class="modal-buy-way__button-proceed-in-basket">
            <button>Выбрано 2 товара на сумму: <span>руб 6.500</span></button>
            <a href="#">ПЕРЕЙТИ В КОРЗИНУ</a>
        </div>
    </div>  
</div>
<div class="modal modal-women-define-size">
    <div class="modal__inner modal-women-define-size__inner">
        <div class="col">
            <span class="modal-women-define-size__title">Определить размер</span>
                <div class="modal-women-define-size__wrapper-for-table">
                    <ul>
                        <li></li>
                        <li>42</li>
                        <li>44</li>
                        <li>46</li>
                        <li>48</li>
                        <li>50</li>
                        <li>52</li>
                    </ul>
                    <div class="modal-women-define-size__wrapper-element-table">
                         <div class="element-table-women initial  top no-background"> 
                             <span  class="no-indentation">Рост (Р)</span>
                         </div>
                         <div style="display: none;"></div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                    </div>
                    <div class="modal-women-define-size__wrapper-element-table">
                         <div class="element-table-women initial  top no-background"> 
                             <span  class="no-indentation">Обхват груди (ОГ)</span>
                         </div>
                         <div style="display: none;"></div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                    </div>
                    <div class="modal-women-define-size__wrapper-element-table">
                         <div class="element-table-women initial  top no-background"> 
                             <span  class="no-indentation">Обхват талии (ОТ)</span>
                         </div>
                         <div style="display: none;"></div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                    </div>
                    <div class="modal-women-define-size__wrapper-element-table">
                         <div class="element-table-women initial  top no-background"> 
                             <span  class="no-indentation">Обхват бедер (ОБ)</span>
                         </div>
                         <div style="display: none;"></div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                         <div class="element-table-women top">
                            <span>170</span>
                         </div>
                    </div>
                </div>
            <button class="btn btn-modal-close">Закрыть окно</button>
        </div>
    </div>
</div>







<?include('../local/templates/mondigo/footer.php');?>