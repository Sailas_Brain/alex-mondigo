<?include('../local/templates/mondigo/header.php');?>
<div class="page page-making-refund-initial">
    <div class="page__inner page-making-refund-initial__inner">
        <div class="page-deferred-goods__chain-navigation">  
            <a href="#">Главная</a>
            <a href="#">Личный кабинет</a>
            <span>Оформление возврата</span>
        </div>
        <div class="page-deferred-goods__wrapper-for-col">
            <div class="col-1">
                //= ../../blocks/block/sidebar-menu.php
            </div>
            <div class="col-2">
                <span class="page-making-refund-initial__title">Оформление возврата</span>
                <span class="page-making-refund-initial__text-under-title">Следуйте нижеуказанным пунктам, чтобы оформить возврат с помощью курьера. </span>
                <div class="page-making-refund-initial__internal-wrapper">
                    <div class="page-making-refund-initial__header-list">
                        <span>Укажите причину возврата и способ возврата денежных средств</span>
                        <ul>
                            <li>Описание товара</li>
                            <li>Цена</li>
                            <li>Причина возврата</li>
                            <li>Способ возврата ДС</li>
                        </ul>
                    </div>
                    <div class="page-making-refund-initial__element-list">
                        <div class="page-making-refund-initial__block-product">
                            <span class="mobile-version">Товар</span>
                            <div class="wrapper-for-blocks">
                                <div class="left">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/my-orber/images-insude-list.jpg" alt="">
                                </div>
                                <div class="right">
                                    <a href="#" class="title">Джинсы эластичные синие</a>
                                    <span class="text">Размер: 50</span>
                                    <span class="text">Цвет: синий</span>
                                    <span>Купон:  USVM-2791</span>
                                </div>
                            </div>
                        </div>
                        <div class="page-making-refund-initial__block-price">
                            <span class="mobile-version">Цена</span>
                            <span>4.500</span>
                        </div>
                        <div class="page-making-refund-initial__wrapper-block-quality">
                            <span class="mobile-version">Причина возврата</span>
                            <div class="page-making-refund-initial__block-quality">
                                <div class="quality"><span>Качество</span></div>
                                <div class="internal-block">
                                    <span>Качество 1</span>
                                    <span>Качество 2</span>
                                    <span>Качество 3</span>
                                </div>
                            </div>
                        </div>
                        <div class="page-making-refund-initial__wrapper-block-choose">
                            <span class="mobile-version">Способ возврата ДС</span>
                            <div class="page-making-refund-initial__block-choose">
                                <div class="choose"><span>Выберите способ</span></div>
                                <div class="internal-choose-block">
                                    <span>Выберите способ 1</span>
                                    <span>Выберите способ 2</span>
                                    <span>Выберите способ 3</span>
                                </div>
                            <span class="fill">Заполните поле!</span>
                            </div>
                        </div>
                    </div>
                    <div class="page-making-refund-initial__block-button">
                        <button>продолжить</button>
                    </div>
                    <div class="page-making-refund-initial__please-note">
                        <div>
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/my-data/exclamation.svg" alt="">
                        </div>
                        <div>
                            <span class="title">ОБРАЩАЕМ ВАШЕ ВНИМАНИЕ</span>
                            <span>Возврат осуществляется только Почтой России и на абонентский ящик 20. Возвраты, отправленные на юридический адрес, обрабатываться не будут</span>
                            <span>При отправлении посылки в почтовом отделении в обязательном порядке составляется опись отправляемых вещей. Без описи заказа претензии по комплектации не принимаются.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include('../local/templates/mondigo/footer.php');?>