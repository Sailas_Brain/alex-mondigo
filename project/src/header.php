<?define('SITE_TEMPLATE_PATH', '/local/templates/mondigo')?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- vendor -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick.css" type="text/css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/vendor/slick-theme.css" type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/main.css">
    <title>Document</title>
</head>
<body>
<div class="header">
    <div class="header__inner">
        <div class="header__wrapper-for-col internal-blocks">
            <div class="internal-blocks-col-1">
                <a href="/link/index.php" class="internal-blocks__logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/header/mondigo-logo.svg" alt="">
                </a>
            </div>
            <div class="internal-blocks-col-2">
                <div class="internal-blocks__burger-menu"><button class="btn button-burger-menu"></button></div>
                <div class="internal-blocks__background-burger-menu">
                    <div class="internal-blocks__block-manu">
                        <div class="internal-blocks__novelties">
                            <div class="">
                                <span>Новинки</span>
                            </div> 
                            <div class="internal-blocks__block-menu novelties">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="elements">
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-1.jpg" alt="">
                                        <span>Новинки женские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-2.jpg" alt="">
                                        <span>Новинки мужские</span>
                                    </a>
                                    <a href="#">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/novelty-image-3.jpg" alt="">
                                        <span>Аксессуары</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="internal-blocks__female">
                            <div>
                                <span>Женская</span>
                            </div>
                            <div class="internal-blocks__block-menu female">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="internal-blocks__male">
                            <div>
                                <span>Мужская</span>
                            </div>
                            <div class="internal-blocks__block-menu male">
                                <div class="prev-menu"><button class="nth button-prev-menu"></button></div>
                                <div class="menu">
                                    <div>
                                        <span class="color-red">Распродажа %</span>
                                        <a href="#">Новинки</a>
                                        <a href="#">Скидки</a>
                                        <a href="#">Упаковка</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">ОДЕЖДА</span>
                                        <a href="#">Базовые модели</a>
                                        <a href="#">Блузки</a>
                                        <a href="#">Болеро</a>
                                        <a href="#">Брюки</a>
                                        <a href="#">Верхняя одежда</a>
                                        <a href="#">Вечерний коктейль</a>
                                        <a href="#">Водолазки</a>
                                        <a href="#">Вязаный трикотаж</a>
                                        <a href="#">Джемпера</a>
                                        <a href="#">Джинсы</a>
                                    </div>
                                    <div>
                                        <a href="#">Жакеты</a>
                                        <a href="#">Жилеты</a>
                                        <a href="#">Капри</a>
                                        <a href="#">Кардиганы</a>
                                        <a href="#">Комбинезоны</a>
                                        <a href="#" class="active">Костюмы</a>
                                        <a href="#">Кофточки</a>
                                        <a href="#">Лонгсливы</a>
                                        <a href="#">Лосины</a>
                                        <a href="#">Пиджак</a>
                                        <a href="#">Платья</a>
                                        <a href="#">Пончо</a>
                                    </div>
                                    <div>
                                        <a href="#">Сарафаны</a>
                                        <a href="#">Свитшоты</a> 
                                        <a href="#">Топы</a>
                                        <a href="#">Трикотаж</a>
                                        <a href="#">Туники</a>
                                        <a href="#">Футболки</a>
                                        <a href="#">Шорты</a>
                                        <a href="#">Юбки</a>
                                    </div>
                                    <div>
                                        <span class="uppercase">АКСЕССУАРЫ</span>
                                        <a href="#">Бижутерия</a>
                                        <a href="#">Платки</a>
                                        <a href="#">Ремни</a>
                                        <a href="#">Снуды</a>
                                        <a href="#">Сумки</a>
                                        <a href="#">Шарфы</a>
                                    </div>
                                    <div>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/header/picture-dress.jpg" alt="">  
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <a href="#" class="iinternal-blocks__InstaShop">InstaShop</a>
                    </div>
                </div>
            </div>
            <div class="internal-blocks-col-3">
                <div class="internal-blocks__messege">
                    <div class="internal-blocks__block-images messege"></div>
                    <div>

                    </div>
                </div>
                <div class="internal-blocks__phone">
                    <div class="internal-blocks__block-images phone"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__lock">
                    <div class="internal-blocks__block-images lock"></div>
                    <div class="internal-blocks__internal-block-lock">
                        <div class="links">
                            <a class="uppercase" href="#">Товары</a>
                            <a class="active" href="#">Отложенные товары</a>
                            <a class="bottom-text" href="#">Лист Ожидания</a>
                            <a class="uppercase" href="#">Работа с заказом</a>
                            <a href="#">Мои заказы</a>
                            <a href="#">Мои доставки</a>
                            <a href="#">Мой баланс</a>
                            <a href="#">Личные предложения</a>
                            <a href="#">Гардероб</a>
                            <a class="bottom-text" href="#">Оформление возврата</a>
                            <a class="uppercase" href="#">Профиль</a>
                            <a href="#">Мои данные</a>
                            <a href="#">Моя скидка 5%</a>
                            <a href="#">Мои рассылки</a>
                            <a class="bottom-text" href="#">Новости</a>
                            <a class="uppercase" href="#">Обратная связь</a>
                            <a href="#">Мои обращения</a>
                            <a class="bottom-text" href="#">Проверка товара</a>
                            <a href="#">Выйти</a>
                        </div>                   
                    </div>
                </div>
                <div class="internal-blocks__heart">
                    <div class="internal-blocks__block-images heart"></div>
                    <div>
                        
                    </div>
                </div>
                <div class="internal-blocks__serch">
                    <div class="internal-blocks__block-images serch"></div>
                    
                </div>
                <div class="internal-blocks__basket">
                    <div class="nember-basket"><span>2</span></div>

                    <div class="internal-blocks__basket-dropdawn internal-basket">
                    
                        <span class="internal-basket-title">Корзина</span>
                        <div class="internal-basket-product-basket">
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                            <div class="internal-basket__imaage-slider-basked">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/header/images-sneaker.jpg" alt="">
                                <div class="internal-basket__text-slider-span">
                                    <span class="color">Сандали</span>
                                    <span>Цвет: серебристый</span>
                                    <span>Размер: 37</span>
                                    <span>Кол-во: 1</span>
                                </div>
                                <div class="internal-basket__slider-prise">
                                    <span class="line-through">руб 3.500</span>
                                    <span class="color-red">руб 3.000<br> по купону</span>
                                </div>
                            </div>
                        </div>

                        <div class="internal-basket__price">
                            <span>Итого:</span>
                            <span>руб 6.500</span>
                        </div>



                        <div class="internal-basket__lenk-basket">
                            <a href="#">перейти в корзину</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="internal-blocks__input-search">
                <span>Что ищем? |</span>
                <span class="close-in-search"></span>
                <input type="search" placeholder="">
            </div>
        </div>
    </div>
</div>





