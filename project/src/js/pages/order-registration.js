$(document).ready( function () {
    $('.page-order-registration .order-registration').tabs(1);
    $('.page-order-registration__city').dropdown();
    $('.page-order-registration__block-city').dropdown();
    $(document).on("click", ".internal-block-city span", function(){
        $(".text-city").text($(this).text());
    });
    $(document).on("click", ".internal-block span", function(){
        $(".block-text-city").text($(this).text());
    });
    $(document).on("click", ".icons button", function(){
        $(this).toggleClass("active");
    });

});