$(document).ready( function () {
    $(document).on("click", ".button-choose", function(){
        $(".modal-application-on-hceck__dropdown-fomr-wrapper").toggleClass("active");
    });
    $(".button-create-request").modal(".modal-application-on-hceck");
    $(".button-familiarize").modal(".internal-modal");
    $(document).on("click", ".btn-modal-internal-close", function(){
        $(".internal-modal").css("display", "none");
    });
    if(screen.width <= 320){
        $(".page-checking-goods__return-on-receipt").accordion(true);
        $(".page-checking-goods__return-after-payment").accordion(true);        
    }else if(screen.width <= 425){
        $(".page-checking-goods__return-on-receipt").accordion(true);
        $(".page-checking-goods__return-after-payment").accordion(true);  
    }else{
        $('.page-checking-goods .checking-goods').tabs(1);
        $(".page-checking-goods__requirements").accordion(true);
    }
});