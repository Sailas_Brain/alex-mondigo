$(document).ready(function() {


    
    $('.page-product-card__top-images img').on("click", function(){
        var productCardItemPreview = $(this).parents('.wrapper-for-slider');
        $(this).removeClass('active');
        $(this).addClass('active');
        $(productCardItemPreview).find('.page-product-card__big-images img').attr('src', $(this).attr("src"));
    });
    $(".page-product-card__choose-color").eq(0).dropdown();
    $(".page-product-card__choose-color").eq(1).dropdown();
    $(".page-product-card__choose-color").eq(2).dropdown();
    $(".page-product-card__choose-color").eq(3).dropdown();
    $(".page-product-card__choose-color").eq(4).dropdown();
    $(".page-product-card__choose-color").eq(5).dropdown();
    $(".page-product-card__choose-color").eq(6).dropdown();
    $(".page-product-card__choose-color").eq(7).dropdown();
    $(".page-product-card__choose-color").eq(8).dropdown();

    $(document).on("click", ".page-product-card__wrapper-specify-question textarea", function () {
        $(this).toggleClass("active");
        $(document).on( 'click', function(e){
            if (!$("textarea").is(e.target) && $("textarea").has(e.target).length === 0) {
                $("textarea").removeClass("active");
            }
        });
    })
    $(document).on("click", ".like-heart, .modal-like-heart, .like-heart-block-slider",  function(){
        $(this).toggleClass("active");
    });
    $('.page-product-card .product-card').tabs(1);
    $(document).on("click", ".element-table", function(e) {
        $('.modal-define-size__wrapper-for-table span').removeClass('active');
        if ($('span').is(e.target)) {

            var nthSpan = $(this).find(e.target).index() + 1;
            $(this).parents('.modal-define-size__wrapper-element-table').find('span:nth-of-type(' + nthSpan + ')').addClass('active');

            var nthDiv =  $(this).index() + 1;
            $('.modal-define-size__wrapper-element-table .element-table:nth-of-type(' + nthDiv + ') span').toggleClass('active');

            for (var i = 0; i <= $(this).find('span').length; i++) {
                if (i > nthSpan) {
                    $(this).find('span:nth-of-type(' + i +')').removeClass('active');
                }
            }

            var nthRow = $(this).parents('.modal-define-size__wrapper-element-table').index() + 1;
            for (var i = 0; i <= $('.modal-define-size__wrapper-element-table').length + 1; i++) {
                if (i > nthRow) {
                    $('.modal-define-size__wrapper-element-table:nth-child(' + i + ') span').removeClass('active');
                }
            }
        }

    });
    $('.modal-define-size__wrapper-element-table').on("click", "span", function() {
        $(".modal-define-size__wrapper-for-table span").removeClass("selected");
        $(this).addClass('selected');
    });

    $(".button-define-size").modal(".modal-define-size");
    $(".button-define-size").modal(".modal-women-define-size");
    $(".button-buy-the-way").modal(".modal-buy-way");

    $(document).on("click", ".element-table-women", function(e) {
        $('.modal-women-define-size__wrapper-for-table span').removeClass('active');
        if ($('span').is(e.target)) {

            var nthSpan = $(this).find(e.target).index() + 1;
            $(this).parents('.modal-women-define-size__wrapper-element-table').find('span:nth-of-type(' + nthSpan + ')').addClass('active');

            var nthDiv =  $(this).index() + 1;
            $('.modal-women-define-size__wrapper-element-table .element-table-women:nth-of-type(' + nthDiv + ') span').toggleClass('active');

            for (var i = 0; i <= $(this).find('span').length; i++) {
                if (i > nthSpan) {
                    $(this).find('span:nth-of-type(' + i +')').removeClass('active');
                }
            }
            var nthCol = $(this).index() +1;
            for (var j = 0; j < $(".element-table-women").length +1; j++) {
                if (j > nthCol) {
                    $('.element-table-women:nth-child(' + j + ') span').removeClass('active');        
                }
            }
        }

    });
    $('.modal-women-define-size__wrapper-element-table').on("click", "span", function() {
        $(".modal-women-define-size__wrapper-for-table span").removeClass("selected");
        $(this).addClass('selected');
    });

    if( screen.width <= 320){
        $('.page-product-card__top-images').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            prevArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-prev"></button',
            nextArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-next"></button',
            dots: true,
            horizontal: true,
            horizontalSwiping: true,
        });
    }else{
        $('.page-product-card__top-images').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-prev"></button',
            nextArrow: '<button type="button" class="btn page-product-card__btn page-product-card__btn-next"></button',
            dots: false,
            horizontal: true,
            horizontalSwiping: true,
        });
    }

});









