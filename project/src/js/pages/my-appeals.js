$(document).ready( function(){
    $('.page-my-appeals .page-my-appeals__top-list').checkAllInputs();
    $('.page-my-appeals .page-my-appeals__top-list-heading').checkAllInputs();

    
    $(".button-gift-vouchers").modal(".modal-gift-vouchers");


    if (screen.width <= 320) {
        $(".page-my-appeals__wrapper-for-accordion").accordion(true);
        // $(".page-my-appeals__wrapper-for-accordion").eq(1).accordion(true);
    }else if(screen.width <= 425){
        $(".page-my-appeals__wrapper-for-accordion").accordion(true);        
    }else{
        $('.page-my-appeals .my-appeals').tabs(1);
    }
});