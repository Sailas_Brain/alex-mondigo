var initSlick = function(){
    $('.page-catalog__big-preview').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: true,
        verticalSwiping: false,
        horizontalSwiping: true,
        prevArrow: '<button type="button" class="btn page-catalog__btn-prev"></button>',
        nextArrow: '<button type="button" class="btn page-catalog__btn-next"></button>'
    });
}
$(document).ready(function () {
    $(".internal-filter__sort").dropdown(); 
    $(".internal-filter__block-filter").dropdown();
    initSlick(); 
    $(document).on("click", ".view-two", function() {
        $(".view-four").removeClass("active");
        $(this).toggleClass("active");
        $(".page-catalog__slider-new-collection").toggleClass("active");
        $('.page-catalog__big-preview').slick('unslick');
        initSlick();
    });
    $(document).on("click", ".view-four", function(){
        $(".view-two").removeClass("active");
        $(this).toggleClass("active");
        $(".page-catalog__slider-new-collection").removeClass("active");
        $('.page-catalog__big-preview').slick('unslick');
        initSlick();
    });
    $(document).on("click", ".buttom-close", function(){
        $(".internal-filter__wrapper-sort").css("display", "none");
        $(".sort-text").removeClass("active");
    });
    var catalogFilterPriceSlider = $(".internal-price__wrapper-slider").slider({
        range: true,
        min: 700,
        max: 27000,
        values: [700, 27000],
        slide: function ( event, ui ) {
            $(".internal-price__min-value").val(ui.values[0]);
            $(".internal-price__max-value").val(ui.values[1]);
        }
    });
    $(document).on("change", ".internal-price__val", function(){
        var catalogFilterPriceSlider = Array();
        $(".internal-price__val").each(function () {
            catalogFilterPriceSlider.push($(this).val());
        });
        catalogFilterPriceSlider.slider("option", "values", catalogFilterPriceSlider);
        delete catalogFilterPriceSlider;
    });
    $(document).on("click", ".page-catalog__icon-heart", function() {
        $(this).toggleClass("active"); 
    });
    $(document).on( "click", ".in-cart", function(){
        $(this).css("display", "none");
        $(this).next(".choice-of-size").css("display", "block");
        $(document).on( 'click', function(e){
            if (!$(".in-cart").is(e.target) && $(".in-cart").has(e.target).length === 0) {
                $(".in-cart").css("display", "block");
                $(".in-cart").next(".choice-of-size").css("display", "none");
            }
        });
    });
    if( screen.width < 768){
        $(".col-1").accordion(true);
        $(".col-4").accordion(true);        
        $(".col-floor").accordion(true);
        $(".col-size").accordion(true);
        $(".col-price-in-filter").accordion(true);
        $(".col-season").accordion(true);
        $(".col-composition").accordion(true);
    }
    $(".page-catalog__big-preview").hover( function(){
        $(this).parents(".page-catalog__slider-new-collection").find(".page-catalog__wrapper-for-hover").addClass("active");
        $(this).find(".btn.page-catalog__btn-prev").addClass("active");
        $(this).find(".btn.page-catalog__btn-next").addClass("active");
    });
    $(".page-catalog__big-preview").mouseleave( function(){
        $(this).parents(".page-catalog__slider-new-collection").find(".page-catalog__wrapper-for-hover").removeClass("active");
        $(this).find(".btn.page-catalog__btn-prev").removeClass("active");
        $(this).find(".btn.page-catalog__btn-next").removeClass("active");
    });

});

