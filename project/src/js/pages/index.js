$(document).ready(function(){
    $('.page-main__big-preview').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: true,
        verticalSwiping: false,
        horizontalSwiping: true,
        prevArrow: '<button type="button" class="btn page-main__btn-prev"></button',
        nextArrow: '<button type="button" class="btn page-main__btn-next"></button'
    });
    $(".page-main__icon-heart").on("click", function() {
       $(this).toggleClass("active"); 
    });
    // $(".in-cart").on( "click", function(){
    //     $(this).css("display", "none");
    //     $(".page-main__choice-of-size").css("display", "block");
    //     $(document).on( 'click', function(e){
    //         if (!$(".in-cart").is(e.target) && $(".in-cart").has(e.target).length === 0) {
    //             $(".in-cart").css("display", "block");
    //             $(".page-main__choice-of-size").css("display", "none");
    //         }
    //     });
    // });
    $(".slick-slider").hover(function() {
        $(this).find('.slick-current .image-slider-hover img').toggleClass("active");
    });
    $(".bth-download-more").on("click", function () {
        $(".page-main__wrapper-for-hidden-block").css("display", "flex");
    });



    $(".page-main__big-preview").hover( function(){
        $(this).parents(".page-main__slider-new-collection").find(".page-main__wrapper-for-hover").addClass("active");
        $(this).find(".btn.page-main__btn-prev").addClass("active");
        $(this).find(".btn.page-main__btn-next").addClass("active");
    });
    $(".page-main__big-preview").mouseleave( function(){
        $(this).parents(".page-main__slider-new-collection").find(".page-main__wrapper-for-hover").removeClass("active");
        $(this).find(".btn.page-main__btn-prev").removeClass("active");
        $(this).find(".btn.page-main__btn-next").removeClass("active");
    });

});