$(document).ready(function() {
    
    $('.internal-blocks__lock').dropdown();  

    
    if (screen.width <= 320) {
        $('.internal-blocks-col-2').dropdown();
        $('.internal-blocks__novelties').dropdown();
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else if(screen.width <= 425) {
        $('.internal-blocks-col-2').dropdown();    
        $('.internal-blocks__novelties').dropdown();    
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else if(screen.width <= 768) {
        $('.internal-blocks-col-2').dropdown();    
        $('.internal-blocks__novelties').dropdown();    
        $('.internal-blocks__female').dropdown();
        $('.internal-blocks__male').dropdown();
    }else{

    }

    // $(document).on("click", ".internal-blocks__burger-menu", function(){
    //     $(this).toggleClass("active");
    //     $(".internal-blocks__background-burger-menu").toggleClass("active-menu");
    // });
    $(document).on("click", ".button-prev-menu", function(){
        $(".internal-blocks__block-menu").css("display", "none");
    });
    $(".internal-blocks__block-images.serch").on("click", function () {
        $(".internal-blocks__input-search").css("display", "block");
        $(document).on( 'click', function(e){
            if (!$(".internal-blocks__block-images.serch").is(e.target) && $(".internal-blocks__block-images.serch").has(e.target).length === 0 && !$('input').is(e.target)) {
                $(".internal-blocks__input-search").css("display", "none");
            }
        });
    });
    // $(".nember-basket").on("click", function (){
    //     $(".internal-blocks__basket-dropdawn").toggleClass("active");
    //     $(document).on("click", function (e) {
            
    //     });
    // });

    var hideOpen = function(e) {
        // if ($('.internal-blocks__basket-dropdawn').is(e.target)) {

        // }
        if ($('.nember-basket').is(e.target)) {
            $(".internal-blocks__basket-dropdawn").toggleClass("active");
        }
        else if (!$('.internal-blocks__basket-dropdawn').is(e.target) && $(".internal-blocks__basket-dropdawn").has(e.target).length === 0 ) {
            $(".internal-blocks__basket-dropdawn").removeClass("active");
        }

    }
    document.addEventListener("click", hideOpen);
    $('.internal-basket-product-basket').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        horizontal: false,
        vertical: true,
        verticalSwiping: true,
        horizontalSwiping: false,
        prevArrow: '<button type="button" class="btn internal-basket__btn-prev"></button',
        nextArrow: '<button type="button" class="btn internal-basket__btn-next"></button'
    });
});


    // if(window.screen.width < 320){
    //     console.log("hello world");
    // }